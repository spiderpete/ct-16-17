# #  comment
# () grouping
# [] optional
# *  zero or more
# +  one or more
# |  alternative


program    ::= includeREP structdeclREP vardeclREP fundeclREP EOF

includeREP      ::= include includeREP
                  | e

structdeclREP   ::= structdecl structdeclREP
                  | e

vardeclREP      ::= vardecl vardeclREP
                  | e

fundeclREP      ::= fundecl fundeclREP
                  | e

include    ::= "#include" STRING_LITERAL

structdecl ::= structtype "{" vardeclOPT "}" ";"    # structure declaration

vardeclOPT      ::= vardecl vardeclOPT
                  | vardecl

vardecl    ::= type IDENT ";"                       # normal declaration, e.g. int a;
             | type IDENT "[" INT_LITERAL "]" ";"   # array declaration, e.g. int a[2];

fundecl    ::= type IDENT "(" params ")" block      # function declaration

type       ::= ("int" | "char" | "void" | structtype) pointerOPT

pointerOPT      ::= "*"
                  | e

structtype ::= "struct" IDENT

params     ::= paramsOPT

paramsOPT       ::= type IDENT paramsREP
                  | e

paramsREP       ::= "," type IDENT paramsREP
                  | e

stmt       ::= block
             | "while" "(" exp ")" stmt             # while loop
             | "if" "(" exp ")" stmt elseStmtOPT    # if then else
             | "return" expOPT ";"                  # return
             | exp "=" exp ";"                      # assignment
             | exp ";"                              # expression statement, e.g. a function call

elseStmtOPT     ::= "else" stmt
                  | e

expOPT          ::= exp
                  | e

block      ::= "{" vardeclREP stmtREP "}"

stmtREP         ::= stmt stmtREP
                  | e

exp        ::= "(" exp ")" expREP
             | minusOPT (IDENT | INT_LITERAL) expREP
             | CHAR_LITERAL expREP
             | STRING_LITERAL expREP
             | valueat expREP
             | IDENT expREP
             | funcall expREP
             | sizeof expREP
             | typecast expREP

expREP          ::= (">" | "<" | ">=" | "<=" | "!=" | "==" | "+" | "-" | "/" | "*" | "%" | "||" | "&&") exp expREP
                  | "[" exp "]" expREP              # array access
                  | "." IDENT expREP                # structure field member access
                  | e

minusOPT        ::= "-"
                  | e

funcall      ::= IDENT "(" arglistOPT ")"

arglistOPT      ::= exp argREP
                  | e

argREP          ::= "," exp argREP
                  | e

valueat      ::= "*" exp                            # Value at operator (pointer indirection)
sizeof       ::= "sizeof" "(" type ")"              # size of type
typecast     ::= "(" type ")" exp                    # type casting


