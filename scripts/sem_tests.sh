#!/usr/bin/env bash
printf "\033c" # clear the terminal

echo "============= Semantics Testing Initiated ================================"
echo
echo "------------- First Test PASS --------------------------------------------"
java -cp ../bin Main -sem ../tests/sem/firstTest.c ../tests/sem/firstTest_out.c
echo "------------- First Test End ---------------------------------------------"
echo
echo "------------- Fibonacci PASS ---------------------------------------------"
java -cp ../bin Main -sem ../tests/sem/fibonacci.c ../tests/sem/fibonacci_out.c
echo "------------- Fibonacci End ----------------------------------------------"
echo
echo "------------- General Test PASS ------------------------------------------"
java -cp ../bin Main -sem ../tests/sem/generalTest.c ../tests/sem/generalTest_out.c
echo "------------- General Test End -------------------------------------------"
echo
echo "------------- Function Call FAIL -----------------------------------------"
java -cp ../bin Main -sem ../tests/sem/funCall_FAIL.c ../tests/sem/funCall_FAIL_out.c
echo "------------- Function Call End ------------------------------------------"
echo
echo "------------- Variables FAIL ---------------------------------------------"
java -cp ../bin Main -sem ../tests/sem/var_FAIL.c ../tests/sem/var_FAIL_out.c
echo "------------- Variables End ----------------------------------------------"
echo
echo "------------- Assignments FAIL -------------------------------------------"
java -cp ../bin Main -sem ../tests/sem/assignments.c ../tests/sem/assignments_out.c
echo "------------- Assignments End --------------------------------------------"
echo
echo "------------- Return Functions FAIL --------------------------------------"
java -cp ../bin Main -sem ../tests/sem/returnFunctions_FAIL.c ../tests/sem/returnFunctions_FAIL_out.c
echo "------------- Return Functions End ---------------------------------------"
echo
echo "============= Semantics Testing Finished ================================="