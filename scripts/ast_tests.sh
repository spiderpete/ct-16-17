#!/usr/bin/env bash
printf "\033c" # clear the terminal

echo "============= AST Testing Initiated ======================================"
echo
echo "------------- Fibonacci PASS ---------------------------------------------"
java -cp ../bin Main -ast ../tests/ast/fibonacci.c ../tests/ast/fibonacci_out.c
echo
echo "------------- Fibonacci End ----------------------------------------------"
echo
echo "------------- First Test PASS --------------------------------------------"
java -cp ../bin Main -ast ../tests/ast/firstTest.c ../tests/ast/firstTest_out.c
echo
echo "------------- First Test End ---------------------------------------------"
echo
echo "------------- Var_Array PASS ---------------------------------------------"
java -cp ../bin Main -ast ../tests/ast/var_Array.c ../tests/ast/var_Array_out.c
echo
echo "------------- Var_Array End ----------------------------------------------"
echo
echo "============= AST Testing Finished ======================================="