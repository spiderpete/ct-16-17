#!/usr/bin/env bash
printf "\033c" # clear the terminal

echo "============= Code Generation Testing Initiated =========================="
echo
echo "------------- Array Access -----------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/arrayAccess.c ../tests/gen/arrayAccess_out.asm
echo "------------- Array Access End -------------------------------------------"
echo
echo "------------- Bin Op More ------------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/binOpMore.c ../tests/gen/binOpMore_out.asm
echo "------------- Bin Op More End --------------------------------------------"
echo
echo "------------- Bin Op Simple ----------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/binOpSimple.c ../tests/gen/binOpSimple_out.asm
echo "------------- Bin Op Simple End ------------------------------------------"
echo
echo "------------- If Logicals ------------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/ifLogicals.c ../tests/gen/ifLogicals_out.asm
echo "------------- If Logicals End --------------------------------------------"
echo
echo "------------- If Statement -----------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/ifStmt.c ../tests/gen/ifStmt_out.asm
echo "------------- If Statement End -------------------------------------------"
echo
echo "------------- Infinite While ---------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/infiniteWhile.c ../tests/gen/infiniteWhile_out.asm
echo "------------- Infinite While End -----------------------------------------"
echo
echo "------------- Main Alone -------------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/mainAlone.c ../tests/gen/mainAlone_out.asm
echo "------------- Main Alone End ---------------------------------------------"
echo
echo "------------- Var Arrays -------------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/varArrays.c ../tests/gen/varArrays_out.asm
echo "------------- Var Arrays End ---------------------------------------------"
echo
echo "------------- Var Arrays Declarations and Use ----------------------------"
java -cp ../bin Main -gen ../tests/gen/varArraysDeclUse.c ../tests/gen/varArraysDeclUse_out.asm
echo "------------- Var Arrays Declarations and Use End ------------------------"
echo
echo "------------- Var Pointers -----------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/varPointers.c ../tests/gen/varPointers_out.asm
echo "------------- Var Pointers End -------------------------------------------"
echo
echo "------------- Var Pointers Arrays ----------------------------------------"
java -cp ../bin Main -gen ../tests/gen/varPointersArrays.c ../tests/gen/varPointersArrays_out.asm
echo "------------- Var Pointers Arrays End ------------------------------------"
echo
echo "------------- Var Simple -------------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/varSimple.c ../tests/gen/varSimple_out.asm
echo "------------- Var Simple End ---------------------------------------------"
echo
echo "------------- While Logicals ---------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/whileLogicals.c ../tests/gen/whileLogicals_out.asm
echo "------------- While Logicals End -----------------------------------------"
echo
echo "------------- While Statement --------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/whileStmt.c ../tests/gen/whileStmt_out.asm
echo "------------- While Statement End ----------------------------------------"
echo
echo "------------- String Literal ---------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/strLiteral.c ../tests/gen/strLiteral_out.asm
echo "------------- String Literal End -----------------------------------------"
echo
echo "------------- Functions --------------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/functions.c ../tests/gen/functions_out.asm
echo "------------- Functions End ----------------------------------------------"
echo
echo "------------- Array Access 2 ---------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/arrayAccess2.c ../tests/gen/arrayAccess2_out.asm
echo "------------- Array Access 2 End -----------------------------------------"
echo
echo "------------- Or And -----------------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/orAnd.c ../tests/gen/orAnd_out.asm
echo "------------- Or And End -------------------------------------------------"
echo
echo "------------- Read_c Alone -----------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/read_cAlone.c ../tests/gen/read_cAlone_out.asm
echo "------------- Read_c Alone End -------------------------------------------"
echo
echo "------------- Read_i Alone -----------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/read_iAlone.c ../tests/gen/read_iAlone_out.asm
echo "------------- Read_c Alone End -------------------------------------------"
echo
echo "------------- Print_i Alone ----------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/print_iAlone.c ../tests/gen/print_iAlone_out.asm
echo "------------- Print_i Alone End ------------------------------------------"
echo
echo "------------- Print_c Alone ----------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/print_cAlone.c ../tests/gen/print_cAlone_out.asm
echo "------------- Print_c Alone End ------------------------------------------"
echo
echo "------------- Print_s Alone ----------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/print_sAlone.c ../tests/gen/print_sAlone_out.asm
echo "------------- Print_s Alone End ------------------------------------------"
echo
echo "------------- mcmalloc ---------------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/mcmalloc.c ../tests/gen/mcmalloc_out.asm
echo "------------- mcmallo End ------------------------------------------------"
echo
echo "------------- struct -----------------------------------------------------"
java -cp ../bin Main -gen ../tests/gen/struct.c ../tests/gen/struct_out.asm
echo "------------- struct End -------------------------------------------------"
echo
echo "============= Code Generation Testing Finished ==========================="