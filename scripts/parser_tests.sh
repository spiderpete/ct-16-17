#!/usr/bin/env bash
printf "\033c" # clear the terminal

echo "============= Parser Testing Initiated ==================================="
echo
echo "------------- Empty IF FAIL ----------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/emptyIf.c ../tests/parser/emptyIf_out.c
echo "------------- Empty IF End -----------------------------------------------"
echo
echo "------------- Empty Cast FAIL --------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/emptyCast.c ../tests/parser/emptyCast_out.c
echo "------------- Empty Cast End ---------------------------------------------"
echo
echo "------------- Full Program Fibonacci PASS --------------------------------"
java -cp ../bin Main -parser ../tests/parser/fibonacci.c ../tests/parser/fibonacci_out.c
echo "------------- Full Program Fibonacci End ---------------------------------"
echo
echo "------------- Stand Alone Int PASS ---------------------------------------"
java -cp ../bin Main -parser ../tests/parser/int.c ../tests/parser/int_out.c
echo "------------- Stand Alone Int End ----------------------------------------"
echo
echo "------------- Stand Alone Comments PASS ----------------------------------"
java -cp ../bin Main -parser ../tests/parser/comment.c ../tests/parser/comment_out.c
echo "------------- Stand Alone Comments End -----------------------------------"
echo
echo "------------- Stand Alone Multi Comments PASS ----------------------------"
java -cp ../bin Main -parser ../tests/parser/multiComments.c ../tests/parser/multiComments_out.c
echo "------------- Stand Alone Multi Comments End -----------------------------"
echo
echo "------------- Stand Alone Nested Comments FAIL ---------------------------"
java -cp ../bin Main -parser ../tests/parser/nestedComments.c ../tests/parser/nestedComments_out.c
echo "------------- Stand Alone nested Comments End ----------------------------"
echo
echo "------------- Escape Codes PASS ------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/escapeCode.c ../tests/parser/escapeCode_out.c
echo "------------- Escape Codes End -------------------------------------------"
echo
echo "------------- Most Chars PASS --------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/mostChars.c ../tests/parser/mostChars_out.c
echo "------------- Most Chars End ---------------------------------------------"
echo
echo "------------- No Main PASS -----------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/noMain.c ../tests/parser/noMain_out.c
echo "------------- No Main End ------------------------------------------------"
echo
echo "------------- Minimal PASS -----------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/minimal.c ../tests/parser/minimal_out.c
echo "------------- Minimal End ------------------------------------------------"
echo
echo "------------- Just Main FAIL ---------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/justMain.c ../tests/parser/justMain_out.c
echo "------------- Just Main End ----------------------------------------------"
echo
echo "------------- Identifiers PASS -------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/identifiers.c ../tests/parser/identifiers_out.c
echo "------------- Identifiers End --------------------------------------------"
echo
echo "------------- Include PASS -----------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/include.c ../tests/parser/include_out.c
echo "------------- Include End ------------------------------------------------"
echo
echo "------------- Empty PASS -------------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/empty.c ../tests/parser/empty_out.c
echo "------------- Empty End --------------------------------------------------"
echo
echo "------------- While Loop PASS --------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/whileLoop.c ../tests/parser/whileLoop_out.c
echo "------------- While Loop End ---------------------------------------------"
echo
echo "------------- While No Body FAIL -----------------------------------------"
java -cp ../bin Main -parser ../tests/parser/whileNoBody.c ../tests/parser/whileNoBody_out.c
echo "------------- While No Body End ------------------------------------------"
echo
echo "------------- Unterminated Stmt FAIL -------------------------------------"
java -cp ../bin Main -parser ../tests/parser/unterminatedStmt.c ../tests/parser/unterminatedStmt_out.c
echo "------------- Unterminated Stmt End --------------------------------------"
echo
echo "------------- Global Function PASS ---------------------------------------"
java -cp ../bin Main -parser ../tests/parser/fnGlobal.c ../tests/parser/fnGlobal_out.c
echo "------------- Global Function End ----------------------------------------"
echo
echo "------------- Global Function FAIL ---------------------------------------"
java -cp ../bin Main -parser ../tests/parser/fnGlobalError.c ../tests/parser/fnGlobalError_out.c
echo "------------- Global Function End ----------------------------------------"
echo
echo "------------- Global Function Args Pass ----------------------------------"
java -cp ../bin Main -parser ../tests/parser/fnCallArgs.c ../tests/parser/fnCallArgs_out.c
echo "------------- Global Function Args End -----------------------------------"
echo
echo "------------- Declarations Pass ------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/declarations.c ../tests/parser/declarations_out.c
echo "------------- Declarations End -------------------------------------------"
echo
echo "------------- Undeclarations Pass ----------------------------------------"
java -cp ../bin Main -parser ../tests/parser/undeclared.c ../tests/parser/undeclared_out.c
echo "------------- Undeclarations End -----------------------------------------"
echo
echo "------------- Key Word Identifier FAIL -----------------------------------"
java -cp ../bin Main -parser ../tests/parser/keyWordIdent.c ../tests/parser/keyWordIdent_out.c
echo "------------- Key Word Identifier End ------------------------------------"
echo
echo "------------- Comment at the End PASS ------------------------------------"
java -cp ../bin Main -parser ../tests/parser/commentEnd.c ../tests/parser/commentEnd_out.c
echo "------------- Comment at the end End -------------------------------------"
echo
echo "------------- IF and Else PASS -------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/elseIf.c ../tests/parser/elseIf_out.c
echo "------------- IF and Else End --------------------------------------------"
echo
echo "------------- Broken Maths FAIL ------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/mathsBroken.c ../tests/parser/mathsBroken_out.c
echo "------------- Broken Maths End -------------------------------------------"
echo
echo "------------- Lots PASS --------------------------------------------------"
java -cp ../bin Main -parser ../tests/parser/lots.c ../tests/parser/lots.c
echo "------------- Lots End ---------------------------------------------------"
echo
echo "============= Parser Testing Finished ===================================="