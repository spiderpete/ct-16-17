#!/usr/bin/env bash

printf "\033c" # clear the terminal

echo "============= Lexer Testing Initiated ====================================="
echo
echo "------------- Empty program PASS ------------------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/empty.c ../tests/lexer/empty_out.c
echo "------------- Empty program PASS ------------------------------------------"
echo
echo "------------- Comments program PASS ---------------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/comments/comments.c ../tests/lexer/comments/comments_out.c
echo "------------- Comments program End ----------------------------------------"
echo
echo "------------- Stand alone Multi Line Comments program FAIL ----------------"
java -cp ../bin Main -lexer ../tests/lexer/comments/standAloneOpenMultiLineComment_FAIL.c ../tests/lexer/comments/standAloneOpenMultiLineComment_FAIL_out.c
echo "------------- Stand alone Multi Line Comments program End -----------------"
echo
echo "------------- Stand alone Multi Line Comments program 2 FAIL --------------"
java -cp ../bin Main -lexer ../tests/lexer/comments/standAloneOpenMiltiLineComment2_FAIL.c ../tests/lexer/comments/standAloneOpenMiltiLineComment2_FAIL_out.c
echo "------------- Stand alone Multi Line Comments program 2 End ---------------"
echo
echo "------------- Stand alone Multi Line Comments program 3 Pass --------------"
java -cp ../bin Main -lexer ../tests/lexer/comments/standAloneCompleteMultiLineComments.c ../tests/lexer/comments/standAloneCompleteMultiLineComments_out.c
echo "------------- Stand alone Multi Comments program 3 End --------------------"
echo
echo "------------- Stand alone Single Line Comment program PASS ----------------"
java -cp ../bin Main -lexer ../tests/lexer/comments/standAloneSingleLineComments.c ../tests/lexer/comments/standAloneSingleLineComments_out.c
echo "------------- Stand alone Single Line Comments program End ----------------"
echo
echo "------------- Stand alone Single Line Comments program 2 PASS -------------"
java -cp ../bin Main -lexer ../tests/lexer/comments/singleLineComments.c ../tests/lexer/comments/singleLineComments_out.c
echo "------------- Stand alone Single Line Comments program End ----------------"
echo
echo "------------- Comments with code PASS -------------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/comments/commentsAndCode.c ../tests/lexer/comments/commentsAndCode_out.c
echo "------------- Comments with code End --------------------------------------"
echo
echo "------------- Comments program FAIL ---------------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/comments/comments_FAIL.c ../tests/lexer/comments/comments_FAIL_out.c
echo "------------- Comments program End ----------------------------------------"
echo
echo "------------- Stand alone identifier program PASS -------------------------"
java -cp ../bin Main -lexer ../tests/lexer/identifiers_types_keywords/standAloneIdentifier.c ../tests/lexer/identifiers_types_keywords/standAloneIdentifier_out.c
echo "------------- Stand alone identifier program End --------------------------"
echo
echo "------------- Identifier with specials program PASS -----------------------"
java -cp ../bin Main -lexer ../tests/lexer/identifiers_types_keywords/identifierSpecialsBrackets.c ../tests/lexer/identifiers_types_keywords/identifierSpecialsBrackets_out.c
echo "------------- Identifier with specials program END ------------------------"
echo
echo "------------- Identifiers with division program PASS ----------------------"
java -cp ../bin Main -lexer ../tests/lexer/identifiers_types_keywords/identifierDivision.c ../tests/lexer/identifiers_types_keywords/identifierDivision_out.c
echo "------------- Identifiers with division program End -----------------------"
echo
echo "------------- Types program PASS ------------------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/identifiers_types_keywords/types.c ../tests/lexer/identifiers_types_keywords/types_out.c
echo "------------- Types program End -------------------------------------------"
echo
echo "------------- Types program 2 FAIL ----------------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/identifiers_types_keywords/types_FAIL.c ../tests/lexer/identifiers_types_keywords/types_FAIL_out.c
echo "------------- Types program 2 End -----------------------------------------"
echo
echo "------------- Stand alone Int program PASS --------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/identifiers_types_keywords/standAloneInt.c ../tests/lexer/identifiers_types_keywords/standAloneInt_out.c
echo "------------- Stand alone Int program End ---------------------------------"
echo
echo "------------- Stand alone Void program PASS -------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/identifiers_types_keywords/standAloneVoid.c ../tests/lexer/identifiers_types_keywords/standAloneVoid_out.c
echo "------------- Stand alone Void program End --------------------------------"
echo
echo "------------- Stand alone Char program PASS -------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/identifiers_types_keywords/standAloneChar.c ../tests/lexer/identifiers_types_keywords/standAloneChar_out.c
echo "------------- Stand alone Char program End --------------------------------"
echo
echo "------------- Merged types and keywords program PASS ----------------------"
java -cp ../bin Main -lexer ../tests/lexer/identifiers_types_keywords/mergedTypesKeywords.c ../tests/lexer/identifiers_types_keywords/mergedTypesKeywords_out.c
echo "------------- Merged types and keywords program End -----------------------"
echo
echo "------------- Stand Alone Assignment program PASS -------------------------"
java -cp ../bin Main -lexer ../tests/lexer/eqVsAssign/standAloneAssign.c ../tests/lexer/eqVsAssign/standAloneAssign_out.c
echo "------------- Stand Alone Assignment program End --------------------------"
echo
echo "------------- Stand Alone Equality program PASS ---------------------------"
java -cp ../bin Main -lexer ../tests/lexer/eqVsAssign/standAloneEq.c ../tests/lexer/eqVsAssign/standAloneEq_out.c
echo "------------- Stand Alone Equality program End ----------------------------"
echo
echo "------------- Equality and Assignment program PASS ------------------------"
java -cp ../bin Main -lexer ../tests/lexer/eqVsAssign/eqVsAssign.c ../tests/lexer/eqVsAssign/eqVsAssign_out.c
echo "------------- Equality and Assignment program End -------------------------"
echo
echo "------------- Delimiters program PASS -------------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/delimiters/delimiters.c ../tests/lexer/delimiters/delimiters_out.c
echo "------------- Delimiters program End --------------------------------------"
echo
echo "------------- Stand Alone Include program PASS ----------------------------"
java -cp ../bin Main -lexer ../tests/lexer/include/standAloneInclude.c ../tests/lexer/include/standAloneInclude_out.c
echo "------------- Stand Alone Include program End -----------------------------"
echo
echo "------------- Full Include program PASS -----------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/include/includeFull.c ../tests/lexer/include/includeFull_out.c
echo "------------- Full Include program End ------------------------------------"
echo
echo "------------- Include program FAIL ----------------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/include/include_FAIL.c ../tests/lexer/include/include_FAIL_out.c
echo "------------- Include program End -----------------------------------------"
echo
echo "------------- Include String program PASS ---------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/include/includeString.c ../tests/lexer/include/includeString_out.c
echo "------------- Include String program End ----------------------------------"
echo
echo "------------- String literals Specials program PASS -----------------------"
java -cp ../bin Main -lexer ../tests/lexer/literals/standAloneString.c ../tests/lexer/literals/standAloneString_out.c
echo "------------- String literals Specials program End ------------------------"
echo
echo "------------- Unfinished string program FAIL ------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/literals/unfinishedString_FAIL.c ../tests/lexer/literals/unfinishedString_FAIL_out.c
echo "------------- Unfinished string program End -------------------------------"
echo
echo "------------- Int literal program PASS ------------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/literals/intLiteral.c ../tests/lexer/literals/intLiteral_out.c
echo "------------- Int literal program End -------------------------------------"
echo
echo "------------- Int literal program 2 PASS ----------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/literals/intLiteral2.c ../tests/lexer/literals/intLiteral2_out.c
echo "------------- Int literal program 2 End -----------------------------------"
echo
echo "------------- Stand Alone Char literal program PASS -----------------------"
java -cp ../bin Main -lexer ../tests/lexer/literals/standAloneCharLiteral.c ../tests/lexer/literals/standAloneCharLiteral_out.c
echo "------------- Stand Alone Char literal program End ------------------------"
echo
echo "------------- Unfinished Char literal program FAIL ------------------------"
java -cp ../bin Main -lexer ../tests/lexer/literals/unfinishedCharLiteral_FAIL.c ../tests/lexer/literals/unfinishedCharLiteral_FAIL_out.c
echo "------------- Unfinished Char literal program End -------------------------"
echo
echo "------------- Char literal program FAIL -----------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/literals/wrongCharLiteral2_FAIL.c ../tests/lexer/literals/wrongCharLiteral2_FAIL.c
echo "------------- Char literal program End ------------------------------------"
echo
echo "------------- No-Char literal program FAIL --------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/literals/CharLiteral_FAIL.c ../tests/lexer/literals/CharLiteral_FAIL.c
echo "------------- No-Char literal program End ---------------------------------"
echo
echo "------------- Char literal program PASS -----------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/literals/charLiteral2.c ../tests/lexer/literals/charLiteral2_out.c
echo "------------- Char literal program End ------------------------------------"
echo
echo "------------- Stand Alone AND program PASS --------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneAnd.c ../tests/lexer/operators/standAloneAnd_out.c
echo "------------- Stand Alone AND program End ---------------------------------"
echo
echo "------------- Stand Alone OR program PASS ---------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneOR.c ../tests/lexer/operators/standAloneOR_out.c
echo "------------- Stand Alone OR program End ----------------------------------"
echo
echo "------------- Stand Alone EQ program PASS ---------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneEQ.c ../tests/lexer/operators/standAloneEQ_out.c
echo "------------- Stand Alone EQ program End ----------------------------------"
echo
echo "------------- Stand Alone NE program PASS ---------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneNE.c ../tests/lexer/operators/standAloneNE_out.c
echo "------------- Stand Alone NE program End ----------------------------------"
echo
echo "------------- Stand Alone LT program PASS ---------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneLT.c ../tests/lexer/operators/standAloneLT_out.c
echo "------------- Stand Alone LT program End ----------------------------------"
echo
echo "------------- Stand Alone GT program PASS ---------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneGT.c ../tests/lexer/operators/standAloneGT_out.c
echo "------------- Stand Alone GT program End ----------------------------------"
echo
echo "------------- Stand Alone LE program PASS ---------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneLE.c ../tests/lexer/operators/standAloneLE_out.c
echo "------------- Stand Alone LE program End ----------------------------------"
echo
echo "------------- Stand Alone GE program PASS ---------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneGE.c ../tests/lexer/operators/standAloneGE_out.c
echo "------------- Stand Alone GE program End ----------------------------------"
echo
echo "------------- Stand Alone PLUS program PASS -------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAlonePLUS.c ../tests/lexer/operators/standAlonePLUS_out.c
echo "------------- Stand Alone PLUS program End --------------------------------"
echo
echo "------------- Stand Alone MINUS program PASS ------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneMINUS.c ../tests/lexer/operators/standAloneMINUS_out.c
echo "------------- Stand Alone MINUS program End -------------------------------"
echo
echo "------------- Stand Alone ASTERIX program PASS ----------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneASTERIX.c ../tests/lexer/operators/standAloneASTERIX_out.c
echo "------------- Stand Alone ASTERIX program End -----------------------------"
echo
echo "------------- Stand Alone DIV program PASS --------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneDIV.c ../tests/lexer/operators/standAloneDIV_out.c
echo "------------- Stand Alone DIV program End ---------------------------------"
echo
echo "------------- Stand Alone REM program PASS --------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/standAloneREM.c ../tests/lexer/operators/standAloneREM_out.c
echo "------------- Stand Alone REM program End ---------------------------------"
echo
echo "------------- Stand Alone Ampersand program FAIL --------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/ampersand_FAIL.c ../tests/lexer/operators/ampersand_FAIL_out.c
echo "------------- Stand Alone Ampersand program End ---------------------------"
echo
echo "------------- Stand Alone Bar program FAIL --------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/bar_FAIL.c ../tests/lexer/operators/baf_FAIL_out.c
echo "------------- Stand Alone Bar program End ---------------------------------"
echo
echo "------------- Stand Alone Exclamation Mark program FAIL -------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/exclamationMark_FAIL.c ../tests/lexer/operators/exclamationMark_FAIL_out.c
echo "------------- Stand Alone Exclamation Mark program End --------------------"
echo
echo "------------- Mixed Operators program PASS --------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/operators.c ../tests/lexer/operators/operators.c
echo "------------- Mixed Operators program End ---------------------------------"
echo
echo "------------- Mixed Operators program FAIL --------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/operators/operators_FAIL.c ../tests/lexer/operators/operators_FAIL_out.c
echo "------------- Mixed Operators program End ---------------------------------"
echo
echo "------------- Full program Hello World PASS ------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/helloWorld.c ../tests/lexer/helloWorld_out.c
echo "------------- Hello World End --------------------------------------------"
echo
echo "------------- Full program Fibonacci PASS --------------------------------"
java -cp ../bin Main -lexer ../tests/lexer/fibonacci.c ../tests/lexer/fibonacci_out.c
echo "------------- Fibonacci End ----------------------------------------------"
echo
echo "============= Lexer Testing Finished ====================================="