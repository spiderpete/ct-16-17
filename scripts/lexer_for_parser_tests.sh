#!/usr/bin/env bash
printf "\033c" # clear the terminal

echo "============= Testing Initiated ==========================================="
echo
echo "------------- Full program Fibonacci PASS --------------------------------"
java -cp ../bin Main -lexer ../tests/fibonacci.c ../tests/fibonacci_out.c
echo "------------- Fibonacci End ----------------------------------------------"
echo
echo "============= Testing Finished ============================================"