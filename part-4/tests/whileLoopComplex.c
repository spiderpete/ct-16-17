int foo() {
    int a = 0; // = a0  // USE{},       DEF{a0},OUT{a0},        IN{}
    int c;  // = c0     // USE{},       DEF{c0},OUT{a0,c0},     IN{a0}
    do {
        int b = a + 1;  // USE{a0},     DEF{b}, OUT{b,a0,c0},   IN{a0,c0}
        int c = c + b;  // USE{b,c0},   DEF{c1},OUT{b,a0,c0},   IN{b,a0,c0}    // dead! (but isn't) WTF?????
        int a = b * 2;  // USE{b},      DEF{a1},OUT{a0,c0},     IN{b,a0,c0} // dead
    } while (a < 9);    // USE{a0},     DEF{},  OUT{a0,c0},     IN{a0,c0}   // !succ: "return c", "int b = a + 1"
    return c;           // USE{c0},     DEF{},  OUT{},          IN{c0}
}