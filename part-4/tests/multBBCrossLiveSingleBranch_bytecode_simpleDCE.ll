; ModuleID = './multBBCrossLiveSingleBranch.ll'
source_filename = "./multBBCrossLiveSingleBranch.c"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.12.0"

@.str = private unnamed_addr constant [3 x i8] c"%d\00", align 1

; Function Attrs: nounwind ssp uwtable
define i32 @foo() #0 {
  %1 = mul nsw i32 7, 2
  %2 = sub nsw i32 %1, 7
  %3 = icmp sgt i32 %1, 10
  br i1 %3, label %4, label %9

; <label>:4:                                      ; preds = %0
  %5 = mul nsw i32 8, 3
  %6 = sub nsw i32 %5, 8
  %7 = sdiv i32 %6, 8
  %8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str, i32 0, i32 0), i32 %7)
  br label %11

; <label>:9:                                      ; preds = %0
  %10 = sub nsw i32 %1, %2
  br label %13

; <label>:11:                                     ; preds = %4
  %12 = add nsw i32 %1, 7
  br label %13

; <label>:13:                                     ; preds = %11, %9
  %.0 = phi i32 [ %12, %11 ], [ %10, %9 ]
  ret i32 %.0
}

declare i32 @printf(i8*, ...) #1

attributes #0 = { nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 4.0.0 (https://github.com/spiderpete/clang.git 8a6ea813424dabc71bf4514942e487bd0268a317) (https://github.com/spiderpete/llvm.git c170429d499f008b204e15cb7a6ef9d45e309d1f)"}
