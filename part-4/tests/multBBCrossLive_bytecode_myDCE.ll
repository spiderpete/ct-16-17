; ModuleID = './multBBCrossLive.ll'
source_filename = "./multBBCrossLive.c"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.12.0"

; Function Attrs: nounwind ssp uwtable
define i32 @foo() #0 {
  %1 = mul nsw i32 7, 2
  %2 = sub nsw i32 %1, 7
  %3 = icmp sgt i32 %2, 10
  br i1 %3, label %4, label %7

; <label>:4:                                      ; preds = %0
  %5 = mul nsw i32 8, 3
  %6 = add nsw i32 %5, 1
  br label %9

; <label>:7:                                      ; preds = %0
  %8 = sub nsw i32 %1, 1
  br label %11

; <label>:9:                                      ; preds = %4
  %10 = add nsw i32 %6, 7
  br label %11

; <label>:11:                                     ; preds = %9, %7
  %.0 = phi i32 [ %10, %9 ], [ %8, %7 ]
  ret i32 %.0
}

attributes #0 = { nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 4.0.0 (https://github.com/spiderpete/clang.git 8a6ea813424dabc71bf4514942e487bd0268a317) (https://github.com/spiderpete/llvm.git c170429d499f008b204e15cb7a6ef9d45e309d1f)"}
