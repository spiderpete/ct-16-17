#include <stdio.h>

int foo() {
    int a1 = 7;
    int b1 = a1 * 2;
    int c1 = b1 - a1;   // dead
    int d1 = c1 / a1;   // dead
    if (b1 > 10) {
        int a2 = 8;
        int b2 = a2 * 3;
        int c2 = b2 - a2;   // dead
        int d2 = c2 / a2;   // dead
        c1 = b2 - a2;
        d2 = c1 / a2;
        printf("%d", d2);
    } else {
        int a3 = 9;
        int b3 = a3 * 4;
        int c3 = b3 - a3;   // dead
        int d3 = c3 / a3;   // dead
        b3 = b1 - c1;
        return b3;
    }
    b1 = b1 + a1;
    return b1;
}

/*
BEFORE DCE
add: 1
br: 4
call: 1
icmp: 1
mul: 3
phi: 1
ret: 1
sdiv: 4
sub: 5
DCE START
add: 1
br: 4
call: 1
icmp: 1
mul: 2
phi: 1
ret: 1
sdiv: 1
sub: 3
DCE END
*/