int foo() {
    int a1 = 7;
    int b1 = a1 * 2;
    int c1 = b1 - a1;   // dead
    int d1 = c1 / a1;   // dead
    if (1 == 0) {
        int a2 = 8;
        int b2 = a2 * 3;
        int c2 = b2 - a2;   // dead
        int d2 = c2 / a2;   // dead
        return b2;
    } else {
        int a3 = 9;
        int b3 = a3 * 4;
        int c3 = b3 - a3;   // dead
        int d3 = c3 / a3;   // dead
        return b3;
    }
    return b1;
}
