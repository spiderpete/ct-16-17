#!/usr/bin/env bash
printf "\033c" # clear the terminal

# Variables for output colour
RC='\033[0;31m' # Red colour
YC='\033[1;33m' # Yellow colour
NC='\033[0m'    # No Color

function runFile {
    # Variable for test file name
    FILE=$1

    # Source -> Bytecode
    ../../../../Phase_2/build/bin/clang -c -S -emit-llvm ./${FILE}.c -o ./${FILE}.ll
    # Bytecode -> optimised Bytecode (by LLVM -dce)
    ../../../../Phase_2/build/bin/opt -mem2reg -dce ./${FILE}.ll -S -o ./${FILE}_bytecode_GT.ll
    # Bytecode -> optimised Bytecode (by LLVM -adce)
    ../../../../Phase_2/build/bin/opt -mem2reg -adce ./${FILE}.ll -S -o ./${FILE}_bytecode_aGT.ll
    # Bytecode -> optimised Bytecode (by my -simpledce)
    ../../../../Phase_2/build/bin/opt -load ../llvm-pass-simple-dce/build/skeleton/libSkeletonPass.so -mem2reg -simpledce ./${FILE}.ll -S -o ./${FILE}_bytecode_simpleDCE.ll 2> ${FILE}_simpledce_COUNTS.txt
    # Bytecode -> optimised Bytecode (by my -mydce)
    ../../../../Phase_2/build/bin/opt -load ../llvm-pass-my-dce/build/skeleton/libSkeletonPass.so -mem2reg -mydce ./${FILE}.ll -S -o ./${FILE}_bytecode_myDCE.ll 2> ${FILE}_mydce_COUNTS.txt

    # Check if LLVM -dce == -simpledce
    diff ./${FILE}_bytecode_GT.ll ./${FILE}_bytecode_simpleDCE.ll > ${FILE}_[dce_to_simpledce].txt
    if [ ! -s ${FILE}_[dce_to_simpledce].txt ]
    then
        echo -e "[-dce -> -simpledce]:\t[OK]"
        rm -f ${FILE}_[dce_to_simpledce].txt
    else
        echo -e "[-dce -> -simpledce]:\t[${RC}FAIL${NC}]"
    fi

    # Check if LLVM -adce == -mydce
    diff ./${FILE}_bytecode_aGT.ll ./${FILE}_bytecode_myDCE.ll > ${FILE}_[adce_to_mydce].txt
    if [ ! -s ${FILE}_[adce_to_mydce].txt ]
    then
        echo -e "[-adce -> -mydce]:\t[OK]"
        rm -f ${FILE}_[adce_to_mydce].txt
    else
        echo -e "[-adce -> -mydce]:\t[${RC}FAIL${RC}]"
    fi

    # Check if -simpledce == -mydce
    diff ./${FILE}_bytecode_simpleDCE.ll ./${FILE}_bytecode_myDCE.ll > ${FILE}_[simpledce_to_mydce].txt
    if [ ! -s ${FILE}_[simpledce_to_mydce].txt ]
    then
        echo -e "[-simpledce -> -mydce]:\t[OK]"
        rm -f ${FILE}_[simpledce_to_mydce].txt
        rm -f ${FILE}_simpledce_COUNTS.txt
        rm -f ${FILE}_mydce_COUNTS.txt
    else
        echo -e "[-simpledce -> -mydce]:\t[${YC}WARNING${NC}]"
        rm -f ${FILE}_[simpledce_to_mydce].txt
    fi
}

echo "============= Code Optimisation Testing Initiated ========================"
echo
RUN_FILE='dead'
echo "------------- ${RUN_FILE}.c -----------------------------------------------------"
runFile ${RUN_FILE}
echo "------------- ${RUN_FILE}.c End -------------------------------------------------"
echo
RUN_FILE='deadMultBB'
echo "------------- ${RUN_FILE}.c -----------------------------------------------------"
runFile ${RUN_FILE}
echo "------------- ${RUN_FILE}.c End -------------------------------------------------"
echo
RUN_FILE='multBBCrossLive'
echo "------------- ${RUN_FILE}.c -----------------------------------------------------"
runFile ${RUN_FILE}
echo "------------- ${RUN_FILE}.c End -------------------------------------------------"
echo
RUN_FILE='multBBCrossLiveSingleBranch'
echo "------------- ${RUN_FILE}.c -----------------------------------------------------"
runFile ${RUN_FILE}
echo "------------- ${RUN_FILE}.c End -------------------------------------------------"
echo
RUN_FILE='multBBMultReturn'
echo "------------- ${RUN_FILE}.c -----------------------------------------------------"
runFile ${RUN_FILE}
echo "------------- ${RUN_FILE}.c End -------------------------------------------------"
echo
RUN_FILE='multBBMultReturnBranchPredict'
echo "------------- ${RUN_FILE}.c -----------------------------------------------------"
runFile ${RUN_FILE}
echo "------------- ${RUN_FILE}.c End -------------------------------------------------"
echo
RUN_FILE='multDeadMultBB'
echo "------------- ${RUN_FILE}.c -----------------------------------------------------"
runFile ${RUN_FILE}
echo "------------- ${RUN_FILE}.c End -------------------------------------------------"
echo
RUN_FILE='whileLoop'
echo "------------- ${RUN_FILE}.c -----------------------------------------------------"
runFile ${RUN_FILE}
echo "------------- ${RUN_FILE}.c End -------------------------------------------------"
echo
RUN_FILE='whileLoopComplex'
echo "------------- ${RUN_FILE}.c -----------------------------------------------------"
runFile ${RUN_FILE}
echo "------------- ${RUN_FILE}.c End -------------------------------------------------"
echo
echo "============= Code Optimisation Testing Finished ========================="