; ModuleID = './whileLoop.ll'
source_filename = "./whileLoop.c"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.12.0"

; Function Attrs: nounwind ssp uwtable
define i32 @foo() #0 {
  br label %1

; <label>:1:                                      ; preds = %5, %0
  %.01 = phi i32 [ undef, %0 ], [ %3, %5 ]
  %.0 = phi i32 [ 0, %0 ], [ %4, %5 ]
  %2 = add nsw i32 %.0, 1
  %3 = add nsw i32 %.01, %2
  %4 = mul nsw i32 %2, 2
  br label %5

; <label>:5:                                      ; preds = %1
  %6 = icmp slt i32 %4, 9
  br i1 %6, label %1, label %7

; <label>:7:                                      ; preds = %5
  ret i32 %3
}

attributes #0 = { nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 4.0.0 (https://github.com/spiderpete/clang.git 8a6ea813424dabc71bf4514942e487bd0268a317) (https://github.com/spiderpete/llvm.git c170429d499f008b204e15cb7a6ef9d45e309d1f)"}
