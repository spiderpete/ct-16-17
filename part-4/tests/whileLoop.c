int foo() {
    int a = 0;
    int c;
    do {
        int b = a + 1;
        c = c + b;
        a = b * 2;
    } while (a < 9);
    return c;
}