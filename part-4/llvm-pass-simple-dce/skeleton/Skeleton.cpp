#define DEBUG_TYPE "opCounter"
#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Utils/Local.h"
#include <map>
#include <vector>
using namespace llvm;
using namespace std;
namespace {
    struct SimpleDCE : public FunctionPass {
        vector<Instruction*> deadInst;
        static char ID;
        SimpleDCE() : FunctionPass(ID) {}

        virtual bool runOnFunction(Function &F) {
            bool deadInstExist = false;
            errs() << "BEFORE DCE\n";
            //Count before dead code elimination here
            printCounts(F);
            errs() << "DCE START\n";

            //Eliminate dead code and produce counts here
            //(You can use is instruction trivially dead)
            do {
                deadInstExist = liveAnalysis(F, deadInst);
                if (deadInstExist) {
                    removeDeadCode(deadInst);
                }
            } while (deadInstExist);

            printCounts(F);
            errs() << "DCE END\n";
            return deadInstExist;
        }

        void printCounts(Function &F) {
            map<string, int> opCounter;
            for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
                for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                    if (opCounter.find(i->getOpcodeName()) == opCounter.end()) {
                        opCounter[i->getOpcodeName()] = 1;
                    } else {
                        opCounter[i->getOpcodeName()] += 1;
                    }
                }
            }

            map<string, int>::iterator i = opCounter.begin();
            map<string, int>::iterator e = opCounter.end();
            while (i != e) {
                errs() << i->first << ": " << i->second << "\n";
                i++;
            }

            opCounter.clear();
        }

	    bool liveAnalysis(Function &F, vector<Instruction*> &deadInst) {
	        bool removeInst = false;

            for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
                for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                    Instruction* inst = & *i;
                    if (isInstructionTriviallyDead(& *inst)) {
                        deadInst.push_back(inst);
                        removeInst = true;
                    }
                }
            }

            return removeInst;
	    }

	    void removeDeadCode(vector<Instruction*> &deadInst) {
            while (!deadInst.empty()) {
                Instruction* inst = deadInst.back();
                deadInst.pop_back();
                inst->eraseFromParent();
            }
        }

    };
}
char SimpleDCE::ID = 0;
static RegisterPass<SimpleDCE> X("simpledce", "Simple dead code elimination");

