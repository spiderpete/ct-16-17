#define DEBUG_TYPE "opCounter"
#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/CFG.h"
#include "llvm-c/Core.h"
#include <map>
#include <vector>
#include <set>
#include <algorithm>
using namespace llvm;
using namespace std;
namespace {
    struct MyDCE : public FunctionPass {
    	vector<Instruction*> deadInst;
        static char ID;
        MyDCE() : FunctionPass(ID) {}
     
        virtual bool runOnFunction(Function &F) {
            bool deadInstExist = false;
            errs() << "BEFORE DCE\n";
            //Count before dead code elimination here
            printCounts(F);

            //Implement your live variable analysis here
            errs() << "DCE START\n";
            //Eliminate dead code and produce counts here
            do {
                deadInstExist = liveAnalysis(F, deadInst);
                if (deadInstExist) {
	                removeDeadCode(deadInst);
	            }
            } while (deadInstExist);

            printCounts(F);
            errs() << "DCE END\n";
            return deadInstExist;
        }

        void printCounts(Function &F) {
            map<string, int> opCounter;
            for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
                for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                    if (opCounter.find(i->getOpcodeName()) == opCounter.end()) {
                        opCounter[i->getOpcodeName()] = 1;
                    } else {
                        opCounter[i->getOpcodeName()] += 1;
                    }
                }
            }

            map<string, int>::iterator i = opCounter.begin();
            map<string, int>::iterator e = opCounter.end();
            while (i != e) {
                errs() << i->first << ": " << i->second << "\n";
                i++;
            }

            opCounter.clear();
        }

        bool liveAnalysis(Function &F, vector<Instruction*> &deadInst) {
            bool removeInst = false;
            bool instSetsConverged = false;
            unsigned timesRun = 0;

            map<Instruction*, vector<set<Instruction*>>> oldInstSetsTable;
            map<Instruction*, vector<set<Instruction*>>> newInstSetsTable;

            // Build all instructions' USE{}, DEF{}, OUT{}, IN{} sets
            do {
                oldInstSetsTable = newInstSetsTable;
                buildInstSets(F, newInstSetsTable, timesRun);

                //printInstSetsTable(newInstSetsTable);

                instSetsConverged = areInstSetsSame(oldInstSetsTable, newInstSetsTable);
            } while (instSetsConverged);

            // Having all instructions' sets, iterate through them and decide which are dead
            // Check live-ness of current instruction
            for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
                for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                    Instruction* inst = & *i;

                    // Make sure not to remove side effecting instructions like function calls
                    // Also be careful not to remove terminating instructions
                    if (inst->isTerminator() || inst->mayHaveSideEffects()) {
                        continue;
                    }

                    vector<set<Instruction*>> instSets = newInstSetsTable.find(inst)->second;
                    instSets.pop_back();        // take off IN{}
                    set<Instruction*> outSet = instSets.back();
                    instSets.pop_back();        // take off OUT{}
                    set<Instruction*> defSet = instSets.back();

                    if (isCurrentInstDead(& *inst, defSet, outSet)) {
                        deadInst.push_back(inst);
                        removeInst = true;
                    }
                }
            }

            return removeInst;
        }

        void buildInstSets(Function &F, map<Instruction*, vector<set<Instruction*>>> &instSetsTable, unsigned &timesRun) {
            set<Instruction*> inSetSucc;

            timesRun++;
            for (Function::iterator bb = F.end(), e = F.begin(); bb != e; ) {
                --bb;
                BasicBlock* currBB = & *bb;
                for (BasicBlock::reverse_iterator i = bb->rbegin(), e = bb->rend(); i != e; ++i) {
                    Instruction* inst = & *i;
                    // Vector standard: top -> [USE{}, DEF{}, OUT{}, IN{}] <- back
                    vector<set<Instruction*>> newInstSetsVector;
                    vector<set<Instruction*>> oldInstSetsVector;

                    set<Instruction*> useSet;
                    set<Instruction*> defSet;
                    set<Instruction*> newOutSet;
                    set<Instruction*> oldOutSet;
                    set<Instruction*> newInSet;
                    set<Instruction*> oldInSet;

                    // Take OUT{} and IN{} from previous run for current instruction
                    // As well as USE{} and DEF{} to ensure they are always the same and never change
                    if (timesRun != 1) {
                        oldInstSetsVector = instSetsTable.find(inst)->second;

                        oldInSet = oldInstSetsVector.back();
                        oldInstSetsVector.pop_back();
                        oldOutSet = oldInstSetsVector.back();
                        oldInstSetsVector.pop_back();
                        defSet = oldInstSetsVector.back();
                        oldInstSetsVector.pop_back();
                        useSet = oldInstSetsVector.back();
                        oldInstSetsVector.pop_back();
                    }

                    // USE{} and DEF{} only need to be calculated once as they do not change
                    if (timesRun == 1) {
                        // USE{} set
                        useSet = buildUseSet(inst);

                        // DEF{} set
                        defSet = buildDefSet(inst);
                    }
                    newInstSetsVector.push_back(useSet);
                    newInstSetsVector.push_back(defSet);

                    // OUT{} set
                    newOutSet = buildOutSet(inst, currBB, inSetSucc, instSetsTable);
                    // Merge OUT{} of previous and current iteration
                    set_union(newOutSet.begin(), newOutSet.end(), oldOutSet.begin(), oldOutSet.end(), inserter(newOutSet, newOutSet.begin()));
                    newInstSetsVector.push_back(newOutSet);

                    // IN{} set
                    newInSet = buildInSet(useSet, newOutSet, defSet);
                    // Merge IN{} of previous and current iteration
                    set_union(newInSet.begin(), newInSet.end(), oldInSet.begin(), oldInSet.end(), inserter(newInSet, newInSet.begin()));
                    newInstSetsVector.push_back(newInSet);

                    // Add a record of Instruction -> vector[USE{}, DEF{}, OUT{}, IN{}] for currently processed instruction
                    set_union(newInstSetsVector.begin(), newInstSetsVector.end(), oldInstSetsVector.begin(), oldInstSetsVector.end(), inserter(newInstSetsVector, newInstSetsVector.begin()));
                    instSetsTable[inst] = newInstSetsVector;

                    // Clear sets and prepare them for next instruction iteration
                    if (inst != & *currBB->begin()) {
                        inSetSucc = newInSet;
                    }
                    useSet.clear();
                    defSet.clear();
                    newOutSet.clear();
                    oldOutSet.clear();
                    newInSet.clear();
                    oldInSet.clear();
                    newInstSetsVector.clear();
                    oldInstSetsVector.clear();
                }
            }
        }

        bool areInstSetsSame(map<Instruction*, vector<set<Instruction*>>> lhs, map<Instruction*, vector<set<Instruction*>>> rhs) {
            if (lhs.size() != rhs.size()) {
                return false;
            }

            map<Instruction*, vector<set<Instruction*>>>::iterator lhsInstBegin = lhs.begin();
            map<Instruction*, vector<set<Instruction*>>>::iterator lhsInstEnd   = lhs.end();
            map<Instruction*, vector<set<Instruction*>>>::iterator rhsInstBegin = rhs.begin();
            map<Instruction*, vector<set<Instruction*>>>::iterator rhsInstEnd   = rhs.end();
            map<Instruction*, vector<set<Instruction*>>>::iterator lhsInst;
            map<Instruction*, vector<set<Instruction*>>>::iterator rhsInst;
            for (lhsInst = lhsInstBegin, rhsInst = rhsInstBegin; lhsInst != lhsInstEnd && rhsInst != rhsInstEnd; ++lhsInst, ++rhsInst) {
                // Assume instructions are ordered in the map of instructions to sets,
                // so no need to check for similarity of instructions
                if (lhsInst->first != lhsInst->first) {
                    return false;
                }

                vector<set<Instruction*>> lhsSets = lhsInst->second;
                vector<set<Instruction*>> rhsSets = rhsInst->second;

                if (lhsSets != rhsSets) {
                    return false;
                }
            }

            return true;
        }

        set<Instruction*> buildUseSet(Instruction* inst) {
            set<Instruction*> useSet;
            for (User::op_iterator o = inst->op_begin(), e = inst->op_end(); o != e; ++o) {
                if (isa<Instruction>(*o)) {
                    Instruction* opInst = cast<Instruction>(*o);
                    useSet.insert(opInst);
                }
            }
            return useSet;
        }

        set<Instruction*> buildDefSet(Instruction* inst) {
            set<Instruction*> defSet;
            defSet.insert(inst);
            return defSet;
        }

        set<Instruction*> buildOutSet(Instruction* inst, BasicBlock* currBB, set<Instruction*> inSetSucc, map<Instruction*, vector<set<Instruction*>>> &instSetsTable) {
            set<Instruction*> outSet;

            // Get last element of the current Basic Block
            BasicBlock::iterator endCheckIter = currBB->end();
            --endCheckIter;
            // If the last instruction in a Basic Block, look up the successors of the Basic Block
            // for the construction of the this instruction's OUT{} set
            if (inst == & *endCheckIter) {
                for (succ_iterator si = succ_begin(currBB), e = succ_end(currBB); si != e; ++si) {
                    BasicBlock* succBB = *si;
                    BasicBlock::iterator instInSuccBB = succBB->begin();
                    Instruction* firstInstInSuccBB = & *instInSuccBB;

                    // If the BasicBlock has been processed already, use it's IN{} set
                    // If the BasicBlock has not been processed yet, simply do nothing
                    if (instSetsTable.find(firstInstInSuccBB) != instSetsTable.end()) {
                        vector<set<Instruction*>> firstInstSets = instSetsTable.find(firstInstInSuccBB)->second;
                        // Get IN{} of successor Basic Block's first instruction
                        set<Instruction*> firstInstINSet = firstInstSets.back();
                        // Merge all IN{} of successors Basic Block's first instructions
                        set_union(outSet.begin(), outSet.end(), firstInstINSet.begin(), firstInstINSet.end(), inserter(outSet, outSet.begin()));
                    }
                }
            } else {
                outSet = inSetSucc;
            }

            return outSet;
        }

        set<Instruction*> buildInSet(set<Instruction*> useSet, set<Instruction*> outSet, set<Instruction*> defSet) {
            set<Instruction*> inSet;
            set<Instruction*> outMinusDefSet;
            set_difference(outSet.begin(), outSet.end(), defSet.begin(), defSet.end(), inserter(outMinusDefSet, outMinusDefSet.begin()));
            set_union(useSet.begin(), useSet.end(), outMinusDefSet.begin(), outMinusDefSet.end(), inserter(inSet, inSet.begin()));

            return inSet;
        }

        bool isCurrentInstDead(Instruction* inst, set<Instruction*> defSet, set<Instruction*> outSet) {
            bool removeInst = false;
            set<Instruction*> liveCheck;
            set_difference(defSet.begin(), defSet.end(), outSet.begin(), outSet.end(), inserter(liveCheck, liveCheck.begin()));

            if (!liveCheck.empty()) {
                removeInst = true;
            }

            liveCheck.clear();
            return removeInst;
        }

        void removeDeadCode(vector<Instruction*> &deadInst) {
        	while (!deadInst.empty()) {
                Instruction* inst = deadInst.back();
                deadInst.pop_back();
                inst->eraseFromParent();
            }
        }

        void printSets(set<Instruction*> useSet, set<Instruction*> defSet, set<Instruction*> outSet, set<Instruction*> inSet) {
            // DEBUG Printing START
            errs() << "USE: { ";
            for (set<Instruction*> ::const_iterator i = useSet.begin(); i != useSet.end(); ++i) {
                errs() << *i << " ";
            }
            errs() << "} || ";
            errs() << "DEF: { ";
            for (set<Instruction*> ::const_iterator i = defSet.begin(); i != defSet.end(); ++i) {
                errs() << *i << " ";
            }
            errs() << "} || ";
            errs() << "OUT: { ";
            for (set<Instruction*> ::const_iterator i = outSet.begin(); i != outSet.end(); ++i) {
                errs() << *i << " ";
            }
            errs() << "} || ";
            errs() << "IN: { ";
            for (set<Instruction*> ::const_iterator i = inSet.begin(); i != inSet.end(); ++i) {
                errs() << *i << " ";
            }
            errs() << "}" << "\n";
            // DEBUG Printing END
        }

        void printInstSetsTable(map<Instruction*, vector<set<Instruction*>>> instSetsTable) {
            errs() << "Sets Table: " << "\n";
            for (map<Instruction*, vector<set<Instruction*>>>::iterator mapIter = instSetsTable.begin(); mapIter != instSetsTable.end(); ++mapIter) {
                errs() << mapIter->first << " -> [";

                vector<set<Instruction*>> instSets = mapIter->second;
                set<Instruction*> inSet = instSets.back();
                instSets.pop_back();
                set<Instruction*> outSet = instSets.back();
                instSets.pop_back();
                set<Instruction*> defSet = instSets.back();
                instSets.pop_back();
                set<Instruction*> useSet = instSets.back();
                instSets.pop_back();

                printSets(useSet, defSet, outSet, inSet);
            }
            errs() << "\n";
        }

    };
}
char MyDCE::ID = 0;
static RegisterPass<MyDCE> X("mydce", "My dead code elimination");

