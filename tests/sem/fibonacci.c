#include "minic-stdlib.h"

void main() {
  int n;
  int first;
  int second;
  int next;
  int c;
  char t;

  // Additional
  char* s;
  int i;
  char cc;
  int size;
  // End of Additional

  // read n from the standard input
  n = read_i();

  first = 0;
  second = 1;

  print_s((char*)"First ");
  print_i(n);
  print_s((char*)" terms of Fibonacci series are : ");

  // Additional

  print_s(s);
  print_i(i);
  print_c(cc);
  read_c();
  read_i();
  mcmalloc(size);
  // End of Additional

  c = 0;
  while (c < n) {
    if ( c <= 1 )
      next = c;
    else
      {
	next = first + second;
	first = second;
	second = next;
      }
    print_i(next);
    print_s((char*)" ");
    c = c+1;
  }
}
