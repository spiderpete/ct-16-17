int a[20];
int aa[21];
char* b[10];
int d;
char* bb;
void* voidVarAllowed;
void voidVarNotAllowed;

void main() {
  char a[20];
  char aaa[100];
  int n;
  char t;
  char c;

  // Legal
  a[1] = 'g';
  a[20] = aaa[2];

  // Illegal
  d = 'h';
  a[20] = aa[2];
  a[2] = 2;
  n = 'e';
  aa[1] = 'f';

  // read n from the standard input
  n = read_i();

  print_s((char*)"First ");
  print_i(n);
  print_s((char*)" terms of Fibonacci series are : ");
}