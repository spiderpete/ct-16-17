struct Book {
    int pages;
    char title[5];
    int* indices[3];
};

int i;
int* p;
char l0;
char l1;
char l2;
char l3;
char l4;
int ints[100];

int main() {
    // Legal
    int i;
    struct Book book1;
    char l5;
    i=0;
    *p=i;
    l0 = 'P';
    l1 = 'e';
    l2 = 't';
    l3 = 'e';
    l4 = 'r';
    l5 = '0';
    book1.pages = 4;
    (book1.title)[0] = l0;
    (book1.title)[1] = l1;
    (book1.title)[2] = l2;
    (book1.title)[3] = l3;
    (book1.title)[4] = l4;
    (book1.title)[5] = l5;
    *(book1.indices)[1] = 4;
    ints[10] = 90;
    ints[9] = i;

    // Illegal
    i+2=3;


    return 0;
}