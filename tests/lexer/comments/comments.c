int main()
{
    // Single line comment
    //Another single line comment &*/\"""'

    /* Multi-line comment in a line*/
    /* Multi-line comment
    on two lines */
    /*Another multi-line comment
    on two lines  &\*"""'*/
}