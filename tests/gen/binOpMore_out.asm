.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp


li $t8, 40
li $s7, 10
sgt $t9, $t8, $s7

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 3
li $t8, 9
sgt $t9, $s7, $t8

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 30
li $s7, 65
slt $t9, $t8, $s7

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 2
li $t8, 1
slt $t9, $s7, $t8

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 32
li $s7, 23
sge $t9, $t8, $s7

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 35
li $t8, 35
sge $t9, $s7, $t8

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 23
li $s7, 63
sge $t9, $t8, $s7

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 43
li $t8, 87
sle $t9, $s7, $t8

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 74
li $s7, 74
sle $t9, $t8, $s7

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 54
li $t8, 23
sle $t9, $s7, $t8

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 39
li $s7, 39
seq $t9, $t8, $s7

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 43
li $t8, 23
seq $t9, $s7, $t8

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 45
li $s7, 43
sne $t9, $t8, $s7

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 54
li $t8, 54
sne $t9, $s7, $t8

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 0
li $s7, 0
bne $t8, $zero SKIP15
or $t9, $t8, $s7	# OR Identity not hit
b BinOp_EXIT15
SKIP15: 			# OR Identity hit
li $t9, 1
BinOp_EXIT15: 

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 1
li $t8, 0
bne $s7, $zero SKIP16
or $t9, $s7, $t8	# OR Identity not hit
b BinOp_EXIT16
SKIP16: 			# OR Identity hit
li $t9, 1
BinOp_EXIT16: 

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 0
li $s7, 1
bne $t8, $zero SKIP17
or $t9, $t8, $s7	# OR Identity not hit
b BinOp_EXIT17
SKIP17: 			# OR Identity hit
li $t9, 1
BinOp_EXIT17: 

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 1
li $t8, 1
bne $s7, $zero SKIP18
or $t9, $s7, $t8	# OR Identity not hit
b BinOp_EXIT18
SKIP18: 			# OR Identity hit
li $t9, 1
BinOp_EXIT18: 

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 0
li $s7, 0
beq $t8, $zero SKIP19
and $t9, $t8, $s7	# AND Identity not hit
b BinOp_EXIT19
SKIP19: 			# AND Identity hit
li $t9, 0
BinOp_EXIT19: 

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 1
li $t8, 0
beq $s7, $zero SKIP20
and $t9, $s7, $t8	# AND Identity not hit
b BinOp_EXIT20
SKIP20: 			# AND Identity hit
li $t9, 0
BinOp_EXIT20: 

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 0
li $s7, 1
beq $t8, $zero SKIP21
and $t9, $t8, $s7	# AND Identity not hit
b BinOp_EXIT21
SKIP21: 			# AND Identity hit
li $t9, 0
BinOp_EXIT21: 

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 1
li $t8, 1
beq $s7, $zero SKIP22
and $t9, $s7, $t8	# AND Identity not hit
b BinOp_EXIT22
SKIP22: 			# AND Identity hit
li $t9, 0
BinOp_EXIT22: 

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
