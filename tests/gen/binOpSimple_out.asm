.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp


li $t8, 2
li $s7, 7
add $t9, $t8, $s7

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 4
li $t8, 3
sub $t9, $s7, $t8

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 10
li $s7, 5
div $t8, $s7
mflo $t9

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 9
li $t8, 3
div $s7, $t8
mflo $t9

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t8, 5
li $s7, 2
div $t8, $s7
mfhi $t9

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 6
li $t8, 3
div $s7, $t8
mfhi $t9

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
