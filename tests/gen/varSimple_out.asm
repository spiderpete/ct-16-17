.data

.align 2
a: 	 .word 0
b: 	 .space 1

.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4

la $t9, a
li $t8, 1000
sw $t8, ($t9)

la $t8, b
li $t9, 'C'
sb $t9, ($t8)

addi $t8, $fp, -0
la $t9, ($t8)
li $t8, 10
sw $t8, ($t9)

addi $t9, $fp, -4
la $t8, ($t9)
li $t9, 'p'
sb $t9, ($t8)

addi $t8, $fp, -8
la $t9, ($t8)
li $t8, 4
sw $t8, ($t9)

addi $t9, $fp, -12
la $t8, ($t9)
li $t9, 1
sw $t9, ($t8)

addi $t8, $fp, -16
la $t9, ($t8)
li $t8, 1
sw $t8, ($t9)

addi $t9, $fp, -20
la $t8, ($t9)
li $t9, 4
sw $t9, ($t8)

addi $t8, $fp, -24
la $t9, ($t8)
li $t8, 4
sw $t8, ($t9)

addi $t9, $fp, -28
la $t8, ($t9)
li $t9, 4
sw $t9, ($t8)

lw $t9, a

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -0
lw $t9, ($t8)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -8
lw $t9, ($t8)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -12
lw $t9, ($t8)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -16
lw $t9, ($t8)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -20
lw $t9, ($t8)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -24
lw $t9, ($t8)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -28
lw $t9, ($t8)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
