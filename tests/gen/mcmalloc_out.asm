.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -4

addi $t8, $fp, -0
la $t9, ($t8)
li $t8, 4
li $v0, 9		# Prepare to allocate heap memory

move $a0, $t8
syscall			# Allocate heap memory
move $s7, $v0

sw $s7, ($t9)

addi $t9, $fp, -0
la $s7, ($t9)
lw $t9, ($s7)
li $s7, 16
sw $s7, ($t9)

addi $t9, $fp, -0
la $s7, ($t9)
lw $t9, ($s7)# Get the address
lw $t9, ($t9)# Get the value at that address

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s7, $fp, -4
la $t9, ($s7)
li $s7, 1
li $v0, 9		# Prepare to allocate heap memory

move $a0, $s7
syscall			# Allocate heap memory
move $t8, $v0

sw $t8, ($t9)

addi $t9, $fp, -4
la $t8, ($t9)
lw $t9, ($t8)
li $t8, 'P'
sw $t8, ($t9)

addi $t9, $fp, -4
la $t8, ($t9)
lw $t9, ($t8)# Get the address
lw $t9, ($t9)# Get the value at that address

move $a0, $t9		# Prepare to print a char
li $v0, 11
syscall			# Print a char


li $v0, 10
syscall
