.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4

addi $t8, $fp, -0
la $t9, ($t8)
li $t8, 0
sw $t8, ($t9)

addi $t9, $fp, -4
la $t8, ($t9)
li $t9, 0
sw $t9, ($t8)

addi $t8, $fp, -8
la $t9, ($t8)
li $t8, 10
sw $t8, ($t9)

addi $t9, $fp, -0
lw $t8, ($t9)

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s7, $fp, -0
lw $t9, ($s7)
li $s7, 2
slt $t8, $t9, $s7
############## Enter While 1 Statement #############
beq $t8, $zero, While_EXIT1
While_BODY1: 

addi $t9, $fp, -0
la $s7, ($t9)
addi $s5, $fp, -0
lw $s6, ($s5)
li $s5, 1
add $t9, $s6, $s5
sw $t9, ($s7)

addi $s7, $fp, -0
lw $t9, ($s7)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s5, $fp, -0
lw $s7, ($s5)
li $s5, 2
slt $t9, $s7, $s5
bne $t9, $zero, While_BODY1
While_EXIT1: 
############## Leave While 1 Statement #############
addi $s7, $fp, -4
lw $s5, ($s7)
addi $s6, $fp, -8
lw $s7, ($s6)
sne $t9, $s5, $s7
############## Enter While 2 Statement #############
beq $t9, $zero, While_EXIT2
While_BODY2: 

addi $s5, $fp, -4
la $s7, ($s5)
addi $s4, $fp, -4
lw $s6, ($s4)
li $s4, 1
add $s5, $s6, $s4
sw $s5, ($s7)

addi $s7, $fp, -8
la $s5, ($s7)
addi $s6, $fp, -8
lw $s4, ($s6)
li $s6, 1
sub $s7, $s4, $s6
sw $s7, ($s5)

addi $s5, $fp, -4
lw $s7, ($s5)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s5, $fp, -8
lw $s7, ($s5)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s6, $fp, -4
lw $s5, ($s6)
addi $s4, $fp, -8
lw $s6, ($s4)
sne $s7, $s5, $s6
bne $s7, $zero, While_BODY2
While_EXIT2: 
############## Leave While 2 Statement #############
li $s7, 0

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
