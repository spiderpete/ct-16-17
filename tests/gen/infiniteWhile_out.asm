.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4

addi $t8, $fp, -0
la $t9, ($t8)
li $t8, 0
sw $t8, ($t9)

addi $s7, $fp, -0
lw $t9, ($s7)
li $s7, 0
seq $t8, $t9, $s7
############## Enter While 1 Statement #############
beq $t8, $zero, While_EXIT1
While_BODY1: 

addi $t9, $fp, -0
lw $s7, ($t9)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s6, $fp, -0
lw $t9, ($s6)
li $s6, 0
seq $s7, $t9, $s6
bne $s7, $zero, While_BODY1
While_EXIT1: 
############## Leave While 1 Statement #############

li $v0, 10
syscall
