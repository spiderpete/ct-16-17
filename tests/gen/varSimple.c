int a;
char b;

void main () {

    //int a;
    //char b;
    int c;
    char d;

    int sizeInt;
    int sizeChar;
    int sizeVoid;
    int sizeIntP;
    int sizeCharP;
    int sizeVoidP;

    a = 1000;
    b = 'C';
    c = 10;
    d = 'p';

    sizeInt = sizeof(int);
    sizeChar = sizeof(char);
    sizeVoid = sizeof(void);
    sizeIntP = sizeof(int*);
    sizeCharP = sizeof(char*);
    sizeVoidP = sizeof(void*);

    print_i(a);
    print_i(c);

    print_i(sizeInt);
    print_i(sizeChar);
    print_i(sizeVoid);
    print_i(sizeIntP);
    print_i(sizeCharP);
    print_i(sizeVoidP);

    // 100010411444

}