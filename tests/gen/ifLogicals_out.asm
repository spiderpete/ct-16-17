.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp


li $t8, 0
li $s7, 1
bne $t8, $zero SKIP1
or $t9, $t8, $s7	# OR Identity not hit
b BinOp_EXIT1
SKIP1: 			# OR Identity hit
li $t9, 1
BinOp_EXIT1: 
############### Enter If 1 Statement ###############
beq $t9, $zero, ELSE1

li $s7, 1

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

b If_EXIT1
ELSE1: 

li $s7, 2

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

If_EXIT1: 
############### Leave If 1 Statement ###############

li $s7, 1
li $s6, 0
li $s5, 0
bne $s6, $zero SKIP3
or $t8, $s6, $s5	# OR Identity not hit
b BinOp_EXIT3
SKIP3: 			# OR Identity hit
li $t8, 1
BinOp_EXIT3: 
bne $s7, $zero SKIP2
or $t9, $s7, $t8	# OR Identity not hit
b BinOp_EXIT2
SKIP2: 			# OR Identity hit
li $t9, 1
BinOp_EXIT2: 
############### Enter If 2 Statement ###############
beq $t9, $zero, ELSE2

li $t8, 1000

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int

b If_EXIT2
ELSE2: 

li $t8, 0

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int

If_EXIT2: 
############### Leave If 2 Statement ###############

li $t8, 0
li $s5, 1
li $s6, 1
beq $s5, $zero SKIP5
and $s7, $s5, $s6	# AND Identity not hit
b BinOp_EXIT5
SKIP5: 			# AND Identity hit
li $s7, 0
BinOp_EXIT5: 
beq $t8, $zero SKIP4
and $t9, $t8, $s7	# AND Identity not hit
b BinOp_EXIT4
SKIP4: 			# AND Identity hit
li $t9, 0
BinOp_EXIT4: 
############### Enter If 3 Statement ###############
beq $t9, $zero, ELSE3

li $s7, 1000

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

b If_EXIT3
ELSE3: 

li $s7, 3

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

If_EXIT3: 
############### Leave If 3 Statement ###############


li $v0, 10
syscall
