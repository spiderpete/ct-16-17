.data

.align 2
a: 	 .word 0
b: 	 .space 1
.align 2
c: 	 .word 0
d: 	 .space 1
.align 2
aa: 	 .space 12
.align 2
aa2: 	 .space 16
bb: 	 .space 5
bb2: 	 .space 8
.align 2
aaa: 	 .space 8
.align 2
aaa2: 	 .space 16
.align 2
bbb: 	 .space 4
.align 2
bbb2: 	 .space 16

.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -12
addi $sp, $sp, -16
addi $sp, $sp, -8
addi $sp, $sp, -8
addi $sp, $sp, -8
addi $sp, $sp, -16
addi $sp, $sp, -4
addi $sp, $sp, -16

la $t9, a
li $t8, 1
sw $t8, ($t9)

la $t8, b
li $t9, '2'
sb $t9, ($t8)

la $t8, aa
li $s7, 2
li $t9, 4
mul $s7, $s7, $t9
add $t9, $t8 $s7
li $s7, 3
sw $s7, ($t9)

la $t9, aa
li $t8, 0
li $s7, 4
mul $t8, $t8, $s7
add $s7, $t9 $t8
li $t8, 4
sw $t8, ($s7)

la $s7, aa2
li $t9, 2
li $t8, 4
mul $t9, $t9, $t8
add $t8, $s7 $t9
li $t9, 5
sw $t9, ($t8)

la $t8, aa2
li $s7, 0
li $t9, 4
mul $s7, $s7, $t9
add $t9, $t8 $s7
li $s7, 6
sw $s7, ($t9)

la $t9, bb
li $t8, 3
li $s7, 1
mul $t8, $t8, $s7
add $s7, $t9 $t8
li $t8, '7'
sb $t8, ($s7)

la $s7, bb
li $t9, 0
li $t8, 1
mul $t9, $t9, $t8
add $t8, $s7 $t9
li $t9, '8'
sb $t9, ($t8)

la $t8, bb2
li $s7, 5
li $t9, 1
mul $s7, $s7, $t9
add $t9, $t8 $s7
li $s7, '9'
sb $s7, ($t9)

la $t9, bb2
li $t8, 0
li $s7, 1
mul $t8, $t8, $s7
add $s7, $t9 $t8
li $t8, 'A'
sb $t8, ($s7)

la $t8, c
lw $s7, a
sw $s7, ($t8)

la $s7, d
lb $t8, b
sb $t8, ($s7)

la $s7, aa
li $t9, 1
li $t8, 4
mul $t9, $t9, $t8
add $t8, $s7 $t9
la $s7, aa
li $s6, 2
li $t9, 4
mul $s6, $s6, $t9
add $t9, $s7 $s6
lw $t9, ($t9)
sw $t9, ($t8)

la $t8, aa2
li $s6, 1
li $t9, 4
mul $s6, $s6, $t9
add $t9, $t8 $s6
la $t8, aa2
li $s7, 0
li $s6, 4
mul $s7, $s7, $s6
add $s6, $t8 $s7
lw $s6, ($s6)
sw $s6, ($t9)

la $t9, bb
li $s7, 2
li $s6, 1
mul $s7, $s7, $s6
add $s6, $t9 $s7
la $t9, bb
li $t8, 3
li $s7, 1
mul $t8, $t8, $s7
add $s7, $t9 $t8
lb $s7, ($s7)
sb $s7, ($s6)

la $s6, bb2
li $t8, 3
li $s7, 1
mul $t8, $t8, $s7
add $s7, $s6 $t8
la $s6, bb2
li $t9, 0
li $t8, 1
mul $t9, $t9, $t8
add $t8, $s6 $t9
lb $t8, ($t8)
sb $t8, ($s7)

addi $s7, $fp, -0
la $t8, ($s7)
li $s7, 10
sw $s7, ($t8)

addi $t8, $fp, -4
la $s7, ($t8)
li $t8, 'a'
sb $t8, ($s7)

addi $t9, $fp, -16
la $s7, ($t9)
li $t9, 2
li $t8, 4
mul $t9, $t9, $t8
sub $t8, $s7 $t9
li $t9, 11
sw $t9, ($t8)

addi $s7, $fp, -16
la $t8, ($s7)
li $s7, 0
li $t9, 4
mul $s7, $s7, $t9
sub $t9, $t8 $s7
li $s7, 12
sw $s7, ($t9)

addi $t8, $fp, -28
la $t9, ($t8)
li $t8, 2
li $s7, 4
mul $t8, $t8, $s7
sub $s7, $t9 $t8
li $t8, 13
sw $t8, ($s7)

addi $t9, $fp, -28
la $s7, ($t9)
li $t9, 0
li $t8, 4
mul $t9, $t9, $t8
sub $t8, $s7 $t9
li $t9, 14
sw $t9, ($t8)

addi $s7, $fp, -44
la $t8, ($s7)
li $s7, 3
li $t9, 1
mul $s7, $s7, $t9
sub $t9, $t8 $s7
li $s7, 'b'
sb $s7, ($t9)

addi $t8, $fp, -44
la $t9, ($t8)
li $t8, 0
li $s7, 1
mul $t8, $t8, $s7
sub $s7, $t9 $t8
li $t8, 'c'
sb $t8, ($s7)

addi $t9, $fp, -52
la $s7, ($t9)
li $t9, 6
li $t8, 1
mul $t9, $t9, $t8
sub $t8, $s7 $t9
li $t9, 'd'
sb $t9, ($t8)

addi $s7, $fp, -52
la $t8, ($s7)
li $s7, 0
li $t9, 1
mul $s7, $s7, $t9
sub $t9, $t8 $s7
li $s7, 'e'
sb $s7, ($t9)

addi $t9, $fp, -8
la $s7, ($t9)
addi $t8, $fp, -0
lw $t9, ($t8)
sw $t9, ($s7)

addi $s7, $fp, -12
la $t9, ($s7)
addi $t8, $fp, -4
lb $s7, ($t8)
sb $s7, ($t9)

addi $t8, $fp, -16
la $t9, ($t8)
li $t8, 1
li $s7, 4
mul $t8, $t8, $s7
sub $s7, $t9 $t8
addi $s6, $fp, -16
la $t9, ($s6)
li $s6, 2
li $t8, 4
mul $s6, $s6, $t8
sub $t8, $t9 $s6
lw $t8, ($t8)
sw $t8, ($s7)

addi $s6, $fp, -28
la $s7, ($s6)
li $s6, 1
li $t8, 4
mul $s6, $s6, $t8
sub $t8, $s7 $s6
addi $t9, $fp, -28
la $s7, ($t9)
li $t9, 0
li $s6, 4
mul $t9, $t9, $s6
sub $s6, $s7 $t9
lw $s6, ($s6)
sw $s6, ($t8)

addi $t9, $fp, -44
la $t8, ($t9)
li $t9, 2
li $s6, 1
mul $t9, $t9, $s6
sub $s6, $t8 $t9
addi $s7, $fp, -44
la $t8, ($s7)
li $s7, 3
li $t9, 1
mul $s7, $s7, $t9
sub $t9, $t8 $s7
lb $t9, ($t9)
sb $t9, ($s6)

addi $s7, $fp, -52
la $s6, ($s7)
li $s7, 3
li $t9, 1
mul $s7, $s7, $t9
sub $t9, $s6 $s7
addi $t8, $fp, -52
la $s6, ($t8)
li $t8, 0
li $s7, 1
mul $t8, $t8, $s7
sub $s7, $s6 $t8
lb $s7, ($s7)
sb $s7, ($t9)

li $s7, 0

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

lw $s7, a

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

lb $s7, b

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t9, aa
li $t8, 2
li $s7, 4
mul $t8, $t8, $s7
add $s7, $t9 $t8
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t8, aa
li $t9, 0
li $s7, 4
mul $t9, $t9, $s7
add $s7, $t8 $t9
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t9, aa2
li $t8, 2
li $s7, 4
mul $t8, $t8, $s7
add $s7, $t9 $t8
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t8, aa2
li $t9, 0
li $s7, 4
mul $t9, $t9, $s7
add $s7, $t8 $t9
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t9, bb
li $t8, 3
li $s7, 1
mul $t8, $t8, $s7
add $s7, $t9 $t8
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t8, bb
li $t9, 0
li $s7, 1
mul $t9, $t9, $s7
add $s7, $t8 $t9
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t9, bb2
li $t8, 5
li $s7, 1
mul $t8, $t8, $s7
add $s7, $t9 $t8
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t8, bb2
li $t9, 0
li $s7, 1
mul $t9, $t9, $s7
add $s7, $t8 $t9
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

lw $s7, c

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

lb $s7, d

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t9, aa
li $t8, 1
li $s7, 4
mul $t8, $t8, $s7
add $s7, $t9 $t8
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t8, aa2
li $t9, 1
li $s7, 4
mul $t9, $t9, $s7
add $s7, $t8 $t9
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t9, bb
li $t8, 2
li $s7, 1
mul $t8, $t8, $s7
add $s7, $t9 $t8
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t8, bb2
li $t9, 3
li $s7, 1
mul $t9, $t9, $s7
add $s7, $t8 $t9
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -0
lw $s7, ($t9)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -4
lb $s7, ($t9)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -16
la $t9, ($t8)
li $t8, 2
li $s7, 4
mul $t8, $t8, $s7
sub $s7, $t9 $t8
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -16
la $t8, ($t9)
li $t9, 0
li $s7, 4
mul $t9, $t9, $s7
sub $s7, $t8 $t9
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -28
la $t9, ($t8)
li $t8, 2
li $s7, 4
mul $t8, $t8, $s7
sub $s7, $t9 $t8
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -28
la $t8, ($t9)
li $t9, 0
li $s7, 4
mul $t9, $t9, $s7
sub $s7, $t8 $t9
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -44
la $t9, ($t8)
li $t8, 3
li $s7, 1
mul $t8, $t8, $s7
sub $s7, $t9 $t8
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -44
la $t8, ($t9)
li $t9, 0
li $s7, 1
mul $t9, $t9, $s7
sub $s7, $t8 $t9
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -52
la $t9, ($t8)
li $t8, 6
li $s7, 1
mul $t8, $t8, $s7
sub $s7, $t9 $t8
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -52
la $t8, ($t9)
li $t9, 0
li $s7, 1
mul $t9, $t9, $s7
sub $s7, $t8 $t9
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -8
lw $s7, ($t9)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -12
lb $s7, ($t9)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -16
la $t9, ($t8)
li $t8, 1
li $s7, 4
mul $t8, $t8, $s7
sub $s7, $t9 $t8
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -28
la $t8, ($t9)
li $t9, 1
li $s7, 4
mul $t9, $t9, $s7
sub $s7, $t8 $t9
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -44
la $t9, ($t8)
li $t8, 2
li $s7, 1
mul $t8, $t8, $s7
sub $s7, $t9 $t8
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -52
la $t8, ($t9)
li $t9, 3
li $s7, 1
mul $t9, $t9, $s7
sub $s7, $t8 $t9
lb $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $s7, 0

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
