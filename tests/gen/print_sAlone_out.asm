.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4

addi $t8, $fp, -0
la $t9, ($t8)

.data
label_1:	.asciiz "hello"

.text
la $t8, label_1
sw $t8, ($t9)

addi $t9, $fp, -0
lw $t8, ($t9)

move $a0, $t8		# Prepare to print a string
li $v0, 4
syscall			# Print a string


.data
label_2:	.asciiz "String Literal"

.text
la $t8, label_2

move $a0, $t8		# Prepare to print a string
li $v0, 4
syscall			# Print a string


li $v0, 10
syscall
