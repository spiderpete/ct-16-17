.data

.align 2
book1_id: 	 .word 0
.align 2
book1_name: 	 .space 4
book1_nick: 	 .space 1

.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4

la $t9, book1_id
li $t8, 1
sw $t8, ($t9)

li $t8, 0
sub $t8, $fp, $t8
la $t8, ($t8)
li $t9, 2
sw $t9, ($t8)

lw $t9, book1_id

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t9, 0
sub $t9, $fp, $t9
la $t9, ($t9)
lw $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
