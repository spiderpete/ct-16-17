void main () {

    // Comparison Operators
    print_i(40 > 10);   // expect 1 - true
    print_i(3 > 9);     // expect 0 - false

    print_i(30 < 65);   // expect 1 - true
    print_i(2 < 1);     // expect 0 - false

    print_i(32 >= 23);  // expect 1 - true
    print_i(35 >= 35);  // expect 1 - true
    print_i(23 >= 63);  // expect 0 - false

    print_i(43 <= 87);  // expect 1 - true
    print_i(74 <= 74);  // expect 1 - true
    print_i(54 <= 23);  // expect 0 - false

    print_i(39 == 39);  // expect 1 - true
    print_i(43 == 23);  // expect 0 - false

    print_i(45 != 43);  // expect 1 - true
    print_i(54 != 54);  // expect 0 - false

    // Logical Operators
    print_i(0 || 0);    // expect 0 - false
    print_i(1 || 0);    // expect 1 - true
    print_i(0 || 1);    // expect 1 - true
    print_i(1 || 1);    // expect 1 - true

    print_i(0 && 0);    // expect 0 - false
    print_i(1 && 0);    // expect 0 - false
    print_i(0 && 1);    // expect 0 - false
    print_i(1 && 1);    // expect 1 - true

    // 1010110110101001110001

}