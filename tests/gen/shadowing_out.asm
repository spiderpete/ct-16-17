.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4

addi $t8, $fp, -0
la $t9, ($t8)
li $t8, 1
sw $t8, ($t9)

addi $t9, $fp, -0
lw $t8, ($t9)

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int


addi $t9, $fp, -0
la $t8, ($t9)
li $t9, 4
sw $t9, ($t8)

addi $t8, $fp, -0
lw $t9, ($t8)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -0
lw $t9, ($t8)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
