.data


.text

j main

Fun_bar:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -4
addi $t9, $fp, -0
la $t9, ($t9)
addi $t8, $fp, 16
lw $t8, ($t8)
sw $t8, ($t9)
addi $t8, $fp, -4
la $t8, ($t8)
addi $t9, $fp, 12
lw $t9, ($t9)
sw $t9, ($t8)
addi $sp, $sp, -4

addi $t8, $fp, -8
la $t9, ($t8)
li $s7, 3
addi $s5, $fp, -0
lw $s6, ($s5)
add $t8, $s7, $s6
sw $t8, ($t9)

addi $t9, $fp, -8
lw $t8, ($t9)
move $v0, $t8
move $sp, $fp
jr $ra			 # Going back to the caller function

Fun_sum:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -4
addi $t8, $fp, -0
la $t8, ($t8)
addi $t9, $fp, 16
lw $t9, ($t9)
sw $t9, ($t8)
addi $t9, $fp, -4
la $t9, ($t9)
addi $t8, $fp, 12
lw $t8, ($t8)
sw $t8, ($t9)

addi $s6, $fp, -0
lw $t9, ($s6)
addi $s7, $fp, -4
lw $s6, ($s7)
add $t8, $t9, $s6
move $v0, $t8
move $sp, $fp
jr $ra			 # Going back to the caller function

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -40

addi $t9, $fp, -4
la $s6, ($t9)
li $t9, 1
li $t8, 4
mul $t9, $t9, $t8
sub $t8, $s6 $t9
li $t9, 0
sw $t9, ($t8)

addi $t8, $fp, -0
la $t9, ($t8)

############## Save all Temporary Registers #############

sw $t0, ($sp)
addi $sp, $sp, -4


sw $t1, ($sp)
addi $sp, $sp, -4


sw $t2, ($sp)
addi $sp, $sp, -4


sw $t3, ($sp)
addi $sp, $sp, -4


sw $t4, ($sp)
addi $sp, $sp, -4


sw $t5, ($sp)
addi $sp, $sp, -4


sw $t6, ($sp)
addi $sp, $sp, -4


sw $t7, ($sp)
addi $sp, $sp, -4


sw $s0, ($sp)
addi $sp, $sp, -4


sw $s1, ($sp)
addi $sp, $sp, -4


sw $s2, ($sp)
addi $sp, $sp, -4


sw $s3, ($sp)
addi $sp, $sp, -4


sw $s4, ($sp)
addi $sp, $sp, -4


sw $s5, ($sp)
addi $sp, $sp, -4


sw $s6, ($sp)
addi $sp, $sp, -4


sw $s7, ($sp)
addi $sp, $sp, -4


sw $t8, ($sp)
addi $sp, $sp, -4


sw $t9, ($sp)
addi $sp, $sp, -4

########## Done Save all Temporary Registers #############

li $t8, 4
sw $t8, ($sp)
addi $sp, $sp, -4
addi $s7, $fp, -4
la $s6, ($s7)
li $s7, 1
li $t8, 4
mul $s7, $s7, $t8
sub $t8, $s6 $s7
lw $t8, ($t8)
sw $t8, ($sp)
addi $sp, $sp, -4
sw $fp, ($sp)
addi $sp, $sp, -4
sw $ra, ($sp)
addi $sp, $sp, -4

jal Fun_bar		 # Going the called function

addi $sp, $sp, 4
lw $ra, ($sp)
addi $sp, $sp, 4
lw $fp, ($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4

############ Restore all Temporary Registers ############

addi $sp, $sp, 4
lw $t9, ($sp)


addi $sp, $sp, 4
lw $t8, ($sp)


addi $sp, $sp, 4
lw $s7, ($sp)


addi $sp, $sp, 4
lw $s6, ($sp)


addi $sp, $sp, 4
lw $s5, ($sp)


addi $sp, $sp, 4
lw $s4, ($sp)


addi $sp, $sp, 4
lw $s3, ($sp)


addi $sp, $sp, 4
lw $s2, ($sp)


addi $sp, $sp, 4
lw $s1, ($sp)


addi $sp, $sp, 4
lw $s0, ($sp)


addi $sp, $sp, 4
lw $t7, ($sp)


addi $sp, $sp, 4
lw $t6, ($sp)


addi $sp, $sp, 4
lw $t5, ($sp)


addi $sp, $sp, 4
lw $t4, ($sp)


addi $sp, $sp, 4
lw $t3, ($sp)


addi $sp, $sp, 4
lw $t2, ($sp)


addi $sp, $sp, 4
lw $t1, ($sp)


addi $sp, $sp, 4
lw $t0, ($sp)

######## Done Restore all Temporary Registers ############

move $t8, $v0
sw $t8, ($t9)

addi $t9, $fp, -0
lw $t8, ($t9)

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s7, $fp, -4
la $t9, ($s7)
li $s7, 2
li $t8, 4
mul $s7, $s7, $t8
sub $t8, $t9 $s7
li $s7, 10
sw $s7, ($t8)

addi $t9, $fp, -4
la $t8, ($t9)
li $t9, 3
li $s7, 4
mul $t9, $t9, $s7
sub $s7, $t8 $t9
li $t9, 100
sw $t9, ($s7)

addi $s7, $fp, -0
la $t9, ($s7)

############## Save all Temporary Registers #############

sw $t0, ($sp)
addi $sp, $sp, -4


sw $t1, ($sp)
addi $sp, $sp, -4


sw $t2, ($sp)
addi $sp, $sp, -4


sw $t3, ($sp)
addi $sp, $sp, -4


sw $t4, ($sp)
addi $sp, $sp, -4


sw $t5, ($sp)
addi $sp, $sp, -4


sw $t6, ($sp)
addi $sp, $sp, -4


sw $t7, ($sp)
addi $sp, $sp, -4


sw $s0, ($sp)
addi $sp, $sp, -4


sw $s1, ($sp)
addi $sp, $sp, -4


sw $s2, ($sp)
addi $sp, $sp, -4


sw $s3, ($sp)
addi $sp, $sp, -4


sw $s4, ($sp)
addi $sp, $sp, -4


sw $s5, ($sp)
addi $sp, $sp, -4


sw $s6, ($sp)
addi $sp, $sp, -4


sw $s7, ($sp)
addi $sp, $sp, -4


sw $t8, ($sp)
addi $sp, $sp, -4


sw $t9, ($sp)
addi $sp, $sp, -4

########## Done Save all Temporary Registers #############

addi $s6, $fp, -4
la $t8, ($s6)
li $s6, 2
li $s7, 4
mul $s6, $s6, $s7
sub $s7, $t8 $s6
lw $s7, ($s7)
sw $s7, ($sp)
addi $sp, $sp, -4
addi $t8, $fp, -4
la $s6, ($t8)
li $t8, 3
li $s7, 4
mul $t8, $t8, $s7
sub $s7, $s6 $t8
lw $s7, ($s7)
sw $s7, ($sp)
addi $sp, $sp, -4
sw $fp, ($sp)
addi $sp, $sp, -4
sw $ra, ($sp)
addi $sp, $sp, -4

jal Fun_sum		 # Going the called function

addi $sp, $sp, 4
lw $ra, ($sp)
addi $sp, $sp, 4
lw $fp, ($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4

############ Restore all Temporary Registers ############

addi $sp, $sp, 4
lw $t9, ($sp)


addi $sp, $sp, 4
lw $t8, ($sp)


addi $sp, $sp, 4
lw $s7, ($sp)


addi $sp, $sp, 4
lw $s6, ($sp)


addi $sp, $sp, 4
lw $s5, ($sp)


addi $sp, $sp, 4
lw $s4, ($sp)


addi $sp, $sp, 4
lw $s3, ($sp)


addi $sp, $sp, 4
lw $s2, ($sp)


addi $sp, $sp, 4
lw $s1, ($sp)


addi $sp, $sp, 4
lw $s0, ($sp)


addi $sp, $sp, 4
lw $t7, ($sp)


addi $sp, $sp, 4
lw $t6, ($sp)


addi $sp, $sp, 4
lw $t5, ($sp)


addi $sp, $sp, 4
lw $t4, ($sp)


addi $sp, $sp, 4
lw $t3, ($sp)


addi $sp, $sp, 4
lw $t2, ($sp)


addi $sp, $sp, 4
lw $t1, ($sp)


addi $sp, $sp, 4
lw $t0, ($sp)

######## Done Restore all Temporary Registers ############

move $s7, $v0
sw $s7, ($t9)

addi $t9, $fp, -0
lw $s7, ($t9)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
