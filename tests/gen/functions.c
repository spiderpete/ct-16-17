int bar (int a, int aa) {

    int temp;
    temp = 3 + a;

    return temp;
}

int sum (int a, int b) {
    return a + b;
}

void main () {

    int result;
    int array[10];

    array[1] = 0;
    result = bar(4, array[1]);
    print_i(result);

    array[2] = 10;
    array[3] = 100;
    result = sum(array[2], array[3]);
    print_i(result);

    // 7110
}