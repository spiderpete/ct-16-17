.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp


li $s4, 5
li $s3, 6
bne $s4, $zero SKIP5
or $s5, $s4, $s3	# OR Identity not hit
b BinOp_EXIT5
SKIP5: 			# OR Identity hit
li $s5, 1
BinOp_EXIT5: 
li $s4, 1
li $s2, 0
bne $s4, $zero SKIP6
or $s3, $s4, $s2	# OR Identity not hit
b BinOp_EXIT6
SKIP6: 			# OR Identity hit
li $s3, 1
BinOp_EXIT6: 
add $s6, $s5, $s3
li $s5, 0
li $s2, 1
bne $s5, $zero SKIP7
or $s3, $s5, $s2	# OR Identity not hit
b BinOp_EXIT7
SKIP7: 			# OR Identity hit
li $s3, 1
BinOp_EXIT7: 
add $s7, $s6, $s3
li $s6, 0
li $s2, 0
bne $s6, $zero SKIP8
or $s3, $s6, $s2	# OR Identity not hit
b BinOp_EXIT8
SKIP8: 			# OR Identity hit
li $s3, 1
BinOp_EXIT8: 
add $t8, $s7, $s3
li $s7, 1
li $s2, 1
bne $s7, $zero SKIP9
or $s3, $s7, $s2	# OR Identity not hit
b BinOp_EXIT9
SKIP9: 			# OR Identity hit
li $s3, 1
BinOp_EXIT9: 
add $t9, $t8, $s3

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
