void main () {
    int trueVar;
    int falseVar;
    int cond1;
    int cond2;
    int cond3;

    trueVar = 1;
    falseVar = 0;

    cond1 = falseVar || trueVar;
    cond2 = trueVar || (falseVar || falseVar);
    cond3 = falseVar && (trueVar && trueVar);

    print_i(0);

    while (cond1) {
        print_i(1);
        cond1 = falseVar;
    }


    while (cond2) {
        print_i(2);
        cond2 = falseVar;
    }

    while (cond3) {
        print_i(3);
        cond3 = falseVar;
    }

    print_i(0);

    // 0120

}