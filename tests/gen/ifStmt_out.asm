.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp


li $t9, 0
############### Enter If 1 Statement ###############
beq $t9, $zero, ELSE1

li $t8, 0

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int

b If_EXIT1
ELSE1: 

li $t8, 1

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int

If_EXIT1: 
############### Leave If 1 Statement ###############

li $t9, 10
############### Enter If 2 Statement ###############
beq $t9, $zero, ELSE2

li $t8, 0

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int

b If_EXIT2
ELSE2: 

li $t8, 10

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int

If_EXIT2: 
############### Leave If 2 Statement ###############

li $t9, 3
############### Enter If 3 Statement ###############
beq $t9, $zero, ELSE3

li $t8, 0
############### Enter If 4 Statement ###############
beq $t8, $zero, ELSE4

li $s7, 100

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

b If_EXIT4
ELSE4: 

li $s7, 200

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

If_EXIT4: 
############### Leave If 4 Statement ###############

b If_EXIT3
ELSE3: 

li $t8, 0
############### Enter If 5 Statement ###############
beq $t8, $zero, ELSE5

li $s7, 300

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

b If_EXIT5
ELSE5: 

li $s7, 400

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

If_EXIT5: 
############### Leave If 5 Statement ###############

If_EXIT3: 
############### Leave If 3 Statement ###############


li $v0, 10
syscall
