.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4

addi $t8, $fp, -0
la $t9, ($t8)
li $v0, 5
syscall			# Read an int

move $t8, $v0
sw $t8, ($t9)

addi $t9, $fp, -0
lw $t8, ($t9)

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
