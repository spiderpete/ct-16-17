.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -40
addi $sp, $sp, -28

addi $t8, $fp, -4
la $t9, ($t8)
li $t8, 0
sw $t8, ($t9)

addi $t9, $fp, -0
la $t8, ($t9)
li $t9, 1
sw $t9, ($t8)

addi $s7, $fp, -8
la $t8, ($s7)
addi $s6, $fp, -0
lw $s7, ($s6)
li $t9, 4
mul $s7, $s7, $t9
sub $t9, $t8 $s7
li $s7, 6
sw $s7, ($t9)

addi $t8, $fp, -8
la $t9, ($t8)
addi $s5, $fp, -4
lw $s6, ($s5)
li $s5, 1
add $t8, $s6, $s5
li $s7, 4
mul $t8, $t8, $s7
sub $s7, $t9 $t8
lw $s7, ($s7)

move $a0, $s7		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -48
la $t8, ($t9)
addi $s6, $fp, -8
la $s5, ($s6)
addi $s3, $fp, -0
lw $s4, ($s3)
addi $s2, $fp, -4
lw $s3, ($s2)
add $s6, $s4, $s3
li $t9, 4
mul $s6, $s6, $t9
sub $t9, $s5 $s6
lw $t9, ($t9)
li $s7, 4
mul $t9, $t9, $s7
sub $s7, $t8 $t9
li $t9, 69
sw $t9, ($s7)

addi $t8, $fp, -48
la $s7, ($t8)
li $t8, 6
li $t9, 4
mul $t8, $t8, $t9
sub $t9, $s7 $t8
lw $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
