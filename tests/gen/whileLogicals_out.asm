.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4

addi $t8, $fp, -0
la $t9, ($t8)
li $t8, 1
sw $t8, ($t9)

addi $t9, $fp, -4
la $t8, ($t9)
li $t9, 0
sw $t9, ($t8)

addi $t8, $fp, -8
la $t9, ($t8)
addi $s6, $fp, -4
lw $s7, ($s6)
addi $s5, $fp, -0
lw $s6, ($s5)
bne $s7, $zero SKIP1
or $t8, $s7, $s6	# OR Identity not hit
b BinOp_EXIT1
SKIP1: 			# OR Identity hit
li $t8, 1
BinOp_EXIT1: 
sw $t8, ($t9)

addi $t9, $fp, -12
la $t8, ($t9)
addi $s7, $fp, -0
lw $s6, ($s7)
addi $s4, $fp, -4
lw $s5, ($s4)
addi $s3, $fp, -4
lw $s4, ($s3)
bne $s5, $zero SKIP3
or $s7, $s5, $s4	# OR Identity not hit
b BinOp_EXIT3
SKIP3: 			# OR Identity hit
li $s7, 1
BinOp_EXIT3: 
bne $s6, $zero SKIP2
or $t9, $s6, $s7	# OR Identity not hit
b BinOp_EXIT2
SKIP2: 			# OR Identity hit
li $t9, 1
BinOp_EXIT2: 
sw $t9, ($t8)

addi $t8, $fp, -16
la $t9, ($t8)
addi $s6, $fp, -4
lw $s7, ($s6)
addi $s5, $fp, -0
lw $s4, ($s5)
addi $s3, $fp, -0
lw $s5, ($s3)
beq $s4, $zero SKIP5
and $s6, $s4, $s5	# AND Identity not hit
b BinOp_EXIT5
SKIP5: 			# AND Identity hit
li $s6, 0
BinOp_EXIT5: 
beq $s7, $zero SKIP4
and $t8, $s7, $s6	# AND Identity not hit
b BinOp_EXIT4
SKIP4: 			# AND Identity hit
li $t8, 0
BinOp_EXIT4: 
sw $t8, ($t9)

li $t8, 0

move $a0, $t8		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t9, $fp, -8
lw $t8, ($t9)
############## Enter While 1 Statement #############
beq $t8, $zero, While_EXIT1
While_BODY1: 

li $t9, 1

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s6, $fp, -8
la $t9, ($s6)
addi $s7, $fp, -4
lw $s6, ($s7)
sw $s6, ($t9)

addi $t9, $fp, -8
lw $s6, ($t9)
bne $s6, $zero, While_BODY1
While_EXIT1: 
############## Leave While 1 Statement #############
addi $t9, $fp, -12
lw $s6, ($t9)
############## Enter While 2 Statement #############
beq $s6, $zero, While_EXIT2
While_BODY2: 

li $t9, 2

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s7, $fp, -12
la $t9, ($s7)
addi $s5, $fp, -4
lw $s7, ($s5)
sw $s7, ($t9)

addi $t9, $fp, -12
lw $s7, ($t9)
bne $s7, $zero, While_BODY2
While_EXIT2: 
############## Leave While 2 Statement #############
addi $t9, $fp, -16
lw $s7, ($t9)
############## Enter While 3 Statement #############
beq $s7, $zero, While_EXIT3
While_BODY3: 

li $t9, 3

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s5, $fp, -16
la $t9, ($s5)
addi $s4, $fp, -4
lw $s5, ($s4)
sw $s5, ($t9)

addi $t9, $fp, -16
lw $s5, ($t9)
bne $s5, $zero, While_BODY3
While_EXIT3: 
############## Leave While 3 Statement #############
li $s5, 0

move $a0, $s5		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
