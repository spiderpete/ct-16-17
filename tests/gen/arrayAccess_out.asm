.data

.align 2
a: 	 .space 8
b: 	 .space 5

.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -16
addi $sp, $sp, -20
addi $sp, $sp, -4
addi $sp, $sp, -4

la $t8, a
li $s7, 1
li $t9, 4
mul $s7, $s7, $t9
add $t9, $t8 $s7
li $s7, 9
sw $s7, ($t9)

la $t9, a
li $t8, 0
li $s7, 4
mul $t8, $t8, $s7
add $s7, $t9 $t8
li $t8, 8
sw $t8, ($s7)

la $s7, b
li $t9, 4
li $t8, 1
mul $t9, $t9, $t8
add $t8, $s7 $t9
li $t9, '7'
sb $t9, ($t8)

la $t8, b
li $s7, 2
li $t9, 1
mul $s7, $s7, $t9
add $t9, $t8 $s7
li $s7, '6'
sb $s7, ($t9)

addi $t8, $fp, -0
la $t9, ($t8)
li $t8, 3
li $s7, 4
mul $t8, $t8, $s7
sub $s7, $t9 $t8
li $t8, 5
sw $t8, ($s7)

addi $t9, $fp, -0
la $s7, ($t9)
li $t9, 0
li $t8, 4
mul $t9, $t9, $t8
sub $t8, $s7 $t9
li $t9, 4
sw $t9, ($t8)

addi $s7, $fp, -16
la $t8, ($s7)
li $s7, 4
li $t9, 4
mul $s7, $s7, $t9
sub $t9, $t8 $s7
li $s7, 3
sw $s7, ($t9)

addi $t8, $fp, -16
la $t9, ($t8)
li $t8, 2
li $s7, 4
mul $t8, $t8, $s7
sub $s7, $t9 $t8
li $t8, 2
sw $t8, ($s7)

addi $t9, $fp, -36
la $s7, ($t9)
li $t9, 2
li $t8, 1
mul $t9, $t9, $t8
sub $t8, $s7 $t9
li $t9, '1'
sb $t9, ($t8)

addi $s7, $fp, -36
la $t8, ($s7)
li $s7, 0
li $t9, 1
mul $s7, $s7, $t9
sub $t9, $t8 $s7
li $s7, 'o'
sb $s7, ($t9)

addi $t8, $fp, -40
la $t9, ($t8)
li $t8, 3
li $s7, 1
mul $t8, $t8, $s7
sub $s7, $t9 $t8
li $t8, 'd'
sb $t8, ($s7)

addi $t9, $fp, -40
la $s7, ($t9)
li $t9, 1
li $t8, 1
mul $t9, $t9, $t8
sub $t8, $s7 $t9
li $t9, 'D'
sb $t9, ($t8)

li $t9, 0

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t8, a
li $s7, 1
li $t9, 4
mul $s7, $s7, $t9
add $t9, $t8 $s7
lw $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $s7, a
li $t8, 0
li $t9, 4
mul $t8, $t8, $t9
add $t9, $s7 $t8
lw $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $t8, b
li $s7, 4
li $t9, 1
mul $s7, $s7, $t9
add $t9, $t8 $s7
lb $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

la $s7, b
li $t8, 2
li $t9, 1
mul $t8, $t8, $t9
add $t9, $s7 $t8
lb $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t9, 0

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s7, $fp, -0
la $t8, ($s7)
li $s7, 3
li $t9, 4
mul $s7, $s7, $t9
sub $t9, $t8 $s7
lw $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -0
la $s7, ($t8)
li $t8, 0
li $t9, 4
mul $t8, $t8, $t9
sub $t9, $s7 $t8
lw $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s7, $fp, -16
la $t8, ($s7)
li $s7, 4
li $t9, 4
mul $s7, $s7, $t9
sub $t9, $t8 $s7
lw $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -16
la $s7, ($t8)
li $t8, 2
li $t9, 4
mul $t8, $t8, $t9
sub $t9, $s7 $t8
lw $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t9, 0

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s7, $fp, -36
la $t8, ($s7)
li $s7, 2
li $t9, 1
mul $s7, $s7, $t9
sub $t9, $t8 $s7
lb $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -36
la $s7, ($t8)
li $t8, 0
li $t9, 1
mul $t8, $t8, $t9
sub $t9, $s7 $t8
lb $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $s7, $fp, -40
la $t8, ($s7)
li $s7, 3
li $t9, 1
mul $s7, $s7, $t9
sub $t9, $t8 $s7
lb $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

addi $t8, $fp, -40
la $s7, ($t8)
li $t8, 1
li $t9, 1
mul $t8, $t8, $t9
sub $t9, $s7 $t8
lb $t9, ($t9)

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int

li $t9, 0

move $a0, $t9		# Prepare to print an int
li $v0, 1
syscall			# Print an int


li $v0, 10
syscall
