void main () {

    if (0) {            // false
        print_i(0);
    } else {
        print_i(1);
    }

    if (10) {           // true
        print_i(0);
    } else {
        print_i(10);
    }

    if (3) {            // true
        if (0) {        // false
            print_i(100);
        } else {
            print_i(200);
        }
    } else {
        if (0) {        // false
            print_i(300);
        } else {
            print_i(400);
        }
    }

    // 10200

}