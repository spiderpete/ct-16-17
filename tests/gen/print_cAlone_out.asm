.data


.text

j main

.globl main		 # Start program execution from here
main:

move $fp, $sp

addi $sp, $sp, -4
addi $sp, $sp, -4
addi $sp, $sp, -4

addi $t8, $fp, -0
la $t9, ($t8)
li $t8, '9'
sb $t8, ($t9)

addi $t9, $fp, -4
la $t8, ($t9)
li $t9, 'A'
sb $t9, ($t8)

addi $t8, $fp, -8
la $t9, ($t8)
li $t8, 'a'
sb $t8, ($t9)

addi $t9, $fp, -0
lb $t8, ($t9)

move $a0, $t8		# Prepare to print a char
li $v0, 11
syscall			# Print a char

addi $t9, $fp, -4
lb $t8, ($t9)

move $a0, $t8		# Prepare to print a char
li $v0, 11
syscall			# Print a char

addi $t9, $fp, -8
lb $t8, ($t9)

move $a0, $t8		# Prepare to print a char
li $v0, 11
syscall			# Print a char


li $v0, 10
syscall
