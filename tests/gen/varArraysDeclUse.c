// Globals
int a;
char b;
int c;
char d;

int aa[3];
int aa2[4];

char bb[5];
char bb2[8];

int* aaa[2];
int* aaa2[4];

char* bbb[1];
char* bbb2[4];


void main () {

    // Locals
    int aL;
    char bL;
    int cL;
    char dL;

    int aaL[3];
    int aa2L[4];

    char bbL[5];
    char bb2L[8];

    int* aaaL[2];
    int* aaa2L[4];

    char* bbbL[1];
    char* bbb2L[4];


    // Addresses global
    a = 1;
    b = '2';

    aa[2] = 3;
    aa[0] = 4;
    aa2[2] = 5;
    aa2[0] = 6;

    bb[3] = '7';
    bb[0] = '8';
    bb2[5] = '9';
    bb2[0] = 'A';

    // Uses global
    c = a;
    d = b;

    aa[1] = aa[2];
    aa2[1] = aa2[0];

    bb[2] = bb[3];
    bb2[3] = bb2[0];

    // Addresses local
    aL = 10;
    bL = 'a';

    aaL[2] = 11;
    aaL[0] = 12;
    aa2L[2] = 13;
    aa2L[0] = 14;

    bbL[3] = 'b';
    bbL[0] = 'c';
    bb2L[6] = 'd';
    bb2L[0] = 'e';

    // Uses local
    cL = aL;
    dL = bL;

    aaL[1] = aaL[2];
    aa2L[1] = aa2L[0];

    bbL[2] = bbL[3];
    bb2L[3] = bb2L[0];

    print_i(0);     // 0
    print_i(a);     // 1
    print_i(b);     // 50
    print_i(aa[2]); // 3
    print_i(aa[0]); // 4
    print_i(aa2[2]);// 5
    print_i(aa2[0]);// 6
    print_i(bb[3]); // 55
    print_i(bb[0]); // 56
    print_i(bb2[5]);// 57
    print_i(bb2[0]);// 65
    print_i(c);     // 1
    print_i(d);     // 50
    print_i(aa[1]); // 3
    print_i(aa2[1]);// 6
    print_i(bb[2]); // 55
    print_i(bb2[3]);// 65
    print_i(aL);    // 10
    print_i(bL);    // 97
    print_i(aaL[2]);// 11
    print_i(aaL[0]);// 12
    print_i(aa2L[2]);//13
    print_i(aa2L[0]);//14
    print_i(bbL[3]);// 98
    print_i(bbL[0]);// 99
    print_i(bb2L[6]);//100
    print_i(bb2L[0]);//101
    print_i(cL);    // 10
    print_i(dL);    // 97
    print_i(aaL[1]);// 11
    print_i(aa2L[1]);//14
    print_i(bbL[2]);// 98
    print_i(bb2L[3]);//101
    print_i(0);     // 0

    // 0150345655565765150365565109711121314989910010110971114981010

}