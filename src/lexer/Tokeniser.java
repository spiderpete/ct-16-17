package lexer;

import lexer.Token.TokenClass;

import java.io.EOFException;
import java.io.IOException;

/**
 * @author Peter Stefanov
 */
public class Tokeniser {

    private Scanner scanner;

    private int error = 0;
    public int getErrorCount() {
	return this.error;
    }

    public Tokeniser(Scanner scanner) {
        this.scanner = scanner;
    }

    private void error(char c, int line, int col) {
        System.out.println("Lexing error: unrecognised character ("+c+") at "+line+":"+col);
	error++;
    }


    public Token nextToken() {
        Token result;
        try {
             result = next();
        } catch (EOFException eof) {
            // end of file, nothing to worry about, just return EOF token
            return new Token(TokenClass.EOF, scanner.getLine(), scanner.getColumn());
        } catch (IOException ioe) {
            ioe.printStackTrace();
            // something went horribly wrong, abort
            System.exit(-1);
            return null;
        }
        return result;
    }

    private Token next() throws IOException {

        int line = scanner.getLine();
        int column = scanner.getColumn();

        // get the next character
        char c = scanner.next();

        // skip white spaces
        if (Character.isWhitespace(c))
            return next();

        // distinguish between identifiers, types and keywords
        if (Character.isLetter(c) || c == '_') {
            StringBuilder sb = new StringBuilder();
            sb.append(c);

            try {
                c = scanner.peek();

                while (Character.isLetterOrDigit(c) || c == '_') {
                    sb.append(c);
                    scanner.next();
                    c = scanner.peek();
                }
            } catch (Exception e) {
                // recognises identifiers
                return new Token(TokenClass.IDENTIFIER, sb.toString(), line, column);
            }

            switch (sb.toString()) {
                // recognises types
                case "int":     return new Token(TokenClass.INT, line, column);
                case "void":    return new Token(TokenClass.VOID, line, column);
                case "char":    return new Token(TokenClass.CHAR, line, column);

                // recognises keywords
                case "if":      return new Token(TokenClass.IF, line, column);
                case "else":    return new Token(TokenClass.ELSE, line, column);
                case "while":   return new Token(TokenClass.WHILE, line, column);
                case "return":  return new Token(TokenClass.RETURN, line, column);
                case "struct":  return new Token(TokenClass.STRUCT, line, column);
                case "sizeof":  return new Token(TokenClass.SIZEOF, line, column);
            }

            // recognises identifiers
            return new Token(TokenClass.IDENTIFIER, sb.toString(), line, column);
        }

        // distinguishes between equals and assigning
        if (c == '=') {
            try {
                c = scanner.peek();
            } catch (Exception e) {
                return new Token(TokenClass.ASSIGN, line, column);
            }

            // recognises equals
            if (c == '=') {
                scanner.next();
                return new Token(TokenClass.EQ, line, column);
            } else {
                // recognises assignment
                return new Token(TokenClass.ASSIGN, line, column);
            }
        }

        // recognises delimiters
        if (c == '{')
            return new Token(TokenClass.LBRA, line, column);
        if (c == '}')
            return new Token(TokenClass.RBRA, line, column);
        if (c == '(')
            return new Token(TokenClass.LPAR,line, column);
        if (c == ')')
            return new Token(TokenClass.RPAR, line, column);
        if (c == '[')
            return new Token(TokenClass.LSBR, line, column);
        if (c == ']')
            return new Token(TokenClass.RSBR, line, column);
        if (c == ';')
            return new Token(TokenClass.SC, line, column);
        if (c == ',')
            return new Token(TokenClass.COMMA, line, column);

        // recognises include statements
        if (c == '#') {
            StringBuilder sb = new StringBuilder();
            c = scanner.peek();

            try {
                while (Character.isLetter(c)) {
                    sb.append(c);
                    scanner.next();
                    c = scanner.peek();
                }
            } catch (Exception e) {
                if (sb.toString().equals("include")) {
                    return new Token(TokenClass.INCLUDE, line, column);
                } else {
                    // if we reach this point, it means we did not recognise a valid token
                    error(c, line, column);
                    return new Token(TokenClass.INVALID, line, column);
                }
            }

            if (sb.toString().equals("include")) {
                return new Token(TokenClass.INCLUDE, line, column);
            }
        }

        // recognises string literals
        if (c == '\"') {
            StringBuilder sb = new StringBuilder();

            try {
                c = scanner.peek();

                while (c != '\"') {
                    if (c == '\\') {
                        scanner.next();
                        c = scanner.peek();

                        if (c == '\'' || c == '\"' || c == '\\') {
                        } else switch (c) {
                            case 't':
                                sb.append("\t");
                                scanner.next();
                                c = scanner.peek();
                                continue;
                            case 'b':
                                sb.append("\b");
                                scanner.next();
                                c = scanner.peek();
                                continue;
                            case 'n':
                                sb.append("\n");
                                scanner.next();
                                c = scanner.peek();
                                continue;
                            case 'r':
                                sb.append("\r");
                                scanner.next();
                                c = scanner.peek();
                                continue;
                            case 'f':
                                sb.append("\f");
                                scanner.next();
                                c = scanner.peek();
                                continue;
                            default:
                                error(c, line, column);
                                return new Token(TokenClass.INVALID, line, column);
                        }

                    }
                    sb.append(c);
                    scanner.next();
                    c = scanner.peek();
                }
                scanner.next();
            } catch (Exception e) {
                // if we reach this point, it means we did not recognise a valid token
                error(c, line, column);
                return new Token(TokenClass.INVALID, line, column);
            }

            return new Token(TokenClass.STRING_LITERAL, sb.toString(), line, column);
        }

        // recognises int literals
        if (Character.isDigit(c)) {
            StringBuilder sb = new StringBuilder();
            sb.append(c);

            try {
                c = scanner.peek();

                while (Character.isDigit(c)) {
                    sb.append(c);
                    scanner.next();
                    c = scanner.peek();
                }
            } catch (Exception e) {
                return new Token(TokenClass.INT_LITERAL, sb.toString(), line, column);
            }

            return new Token(TokenClass.INT_LITERAL, sb.toString(), line, column);
        }

        // recognises char literals
        if (c == '\'') {
            StringBuilder sb = new StringBuilder();

            try {
                c = scanner.peek();
                int ascii = (int) c;

                if ((c >= 32 && c <= 126 && c != 92 && c != 34) || c == '\\') {
                //if (Character.isLetterOrDigit(c) || c == '\\' || c == '.' || c == ',' || c == '_' || c == ' ') {
                    if (c == '\\') {
                        scanner.next();
                        c = scanner.peek();

                        switch (c) {
                            case 't':
                                scanner.next();
                                sb.append('\t');
                                break;
                            case 'n':
                                scanner.next();
                                sb.append('\n');
                                break;
                            case 'b':
                                scanner.next();
                                sb.append('\b');
                                break;
                            case 'r':
                                scanner.next();
                                sb.append('\r');
                                break;
                            case 'f':
                                scanner.next();
                                sb.append('\f');
                                break;
                            case '\\':
                                scanner.next();
                                sb.append('\\');
                                break;
                            case '\'':
                                scanner.next();
                                sb.append('\'');
                                break;
                            case '\"':
                                scanner.next();
                                sb.append('\"');
                                break;
                            default:
                                // if we reach this point, it means we did not recognise a valid token
                                error(c, line, column);
                                return new Token(TokenClass.INVALID, line, column);
                        }
                    } else {
                        sb.append(c);
                        scanner.next();
                    }

                    c = scanner.peek();
                    if (c == '\'') {
                        scanner.next();
                        return new Token(TokenClass.CHAR_LITERAL, sb.toString(), line, column);
                    } else {
                        while (c != '\'') {
                            scanner.next();
                            c = scanner.peek();
                        }
                        scanner.next();
                        // if we reach this point, it means we did not recognise a valid token
                        error(c, line, column);
                        return new Token(TokenClass.INVALID, line, column);
                    }
                } else if (c =='\'' ) {
                    // if we reach this point, it means the case of ('') was reached
                    scanner.next();
                    error(c, line, column);
                    return new Token(TokenClass.INVALID, line, column);
                }
            } catch (Exception e) {
                // if we reach this point, it means we did not recognise a valid token
                error(c, line, column);
                return new Token(TokenClass.INVALID, line, column);
            }
        }

        // recognises and
        if (c == '&') {
            try {
                c = scanner.peek();

                if (c == '&') {
                    scanner.next();
                    return new Token(TokenClass.AND, line, column);
                }
                throw new EOFException();
            } catch (Exception e) {
                error(c, line, column);
                return new Token(TokenClass.INVALID, line, column);
            }
        }

        // recognises or
        if (c == '|') {
            try {
                c = scanner.peek();

                if (c == '|') {
                    scanner.next();
                    return new Token(TokenClass.OR, line, column);
                }
                throw new EOFException();
            } catch (Exception e) {
                error(c, line, column);
                return new Token(TokenClass.INVALID, line, column);
            }
        }

        // recognises not equals
        if (c == '!') {
            try {
                c = scanner.peek();

                if (c == '=') {
                    scanner.next();
                    return new Token(TokenClass.NE, line, column);
                }
                throw new EOFException();
            } catch (Exception e) {
                error(c, line, column);
                return new Token(TokenClass.INVALID, line, column);
            }
        }

        // distinguishes between less than and less than or equals
        if (c == '<') {

            try {
                c = scanner.peek();

                // recognises less than or equals
                if (c == '=') {
                    scanner.next();
                    return new Token(TokenClass.LE, line, column);
                } else {
                    //recognises less than
                    return new Token(TokenClass.LT, line, column);
                }
            } catch (Exception e) {
                return new Token(TokenClass.LT, line, column);
            }
        }

        // distinguishes between greater than and greater than or equals
        if (c == '>') {

            try {
                c = scanner.peek();

                // recognises greater than or equals
                if (c == '=') {
                    scanner.next();
                    return new Token(TokenClass.GE, line, column);
                } else {
                    //recognises greater than
                    return new Token(TokenClass.GT, line, column);
                }
            } catch (Exception e) {
                return new Token(TokenClass.GT, line, column);
            }
        }

        // recognises operators
        if (c == '+')
            return new Token(TokenClass.PLUS, line, column);
        if (c == '-')
            return new Token(TokenClass.MINUS, line, column);
        if (c == '*')
            return new Token(TokenClass.ASTERIX, line, column);
        if (c == '%')
            return new Token(TokenClass.REM, line, column);

        // distinguishes between comments and division
        if (c == '/') {
            try {
                c = scanner.peek();
            } catch (Exception e) {
                return new Token(TokenClass.DIV, line, column);
            }

            // recognises multi-line comments
            if (c == '*') {
                scanner.next();
                try {
                    c = scanner.peek();
                } catch (Exception e) {
                    error(c, line, column);
                    return new Token(TokenClass.INVALID, line, column);
                }

                try {
                    while (true) {
                        if (c == '*') {
                            scanner.next();
                            c = scanner.peek();

                            if (c == '/') {
                                scanner.next();
                                break;
                            }
                        } else {
                            scanner.next();
                            c = scanner.peek();
                        }
                    }
                } catch (Exception e) {
                    error(c, line, column);
                    return new Token(TokenClass.INVALID, line, column);
                }
                return next();
            }

            // recognises single line comments
            if (c == '/') {
                scanner.next();
                c = scanner.peek();

                while (c != '\n') {
                    scanner.next();
                    c = scanner.peek();
                }
                return next();
            }

            // recognises division
            return new Token(TokenClass.DIV, line, column);
        }

        // recognises struct member access
        if (c == '.')
            return new Token(TokenClass.DOT, line, column);


        // if we reach this point, it means we did not recognise a valid token
        error(c, line, column);
        return new Token(TokenClass.INVALID, line, column);
    }

}
