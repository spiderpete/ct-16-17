package gen;

import ast.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.EmptyStackException;
import java.util.Stack;

public class CodeGenerator implements ASTVisitor<Register> {

    /*
     * Simple register allocator.
     */
    private boolean globalVars = false;
    private int localVarOffes = 0;
    private int globalIfCounter = 0;
    private int globalBinOpCounter;
    private int globalWhileCounter;
    private int globalStrLiteralCounter;

    // contains all the free temporary registers
    protected Stack<Register> freeRegs = new Stack<Register>();

    public CodeGenerator() {
        freeRegs.addAll(Register.tmpRegs);
    }

    private class RegisterAllocationError extends Error {}

    private Register getRegister() {
        try {
            return freeRegs.pop();
        } catch (EmptyStackException ese) {
            throw new RegisterAllocationError(); // no more free registers, bad luck!
        }
    }

    private void freeRegister(Register reg) {
        freeRegs.push(reg);
    }

    protected PrintWriter writer; // use this writer to output the assembly instructions


    public void emitProgram(Program program, File outputFile) throws FileNotFoundException {
        writer = new PrintWriter(outputFile);

        visitProgram(program);
        writer.close();
    }


    @Override
    public Register visitProgram(Program p) {
        // TODO: to complete
        writer.println(".data");
        writer.println();

        this.globalVars = true;
        for (VarDecl vd : p.varDecls) {
            vd.accept(this);
        }
        this.globalVars = false;

        writer.println();
        writer.println(".text");
        writer.println();
        writer.printf("j main\n");
        writer.println();

        for (FunDecl fd : p.funDecls) {
            this.localVarOffes = 0; // initialise the offset for stacking local variables
            fd.accept(this);
        }
        return null;
    }

    @Override
    public Register visitBaseType(BaseType bt) {

        Register result = getRegister();

        if (bt == BaseType.INT) {
            writer.printf("li %s, %d\n", result, 4);
        } else if (bt == BaseType.CHAR) {
            writer.printf("li %s, %d\n", result, 1);
        } else if (bt == BaseType.VOID) {
            writer.printf("li %s, %d\n", result, 1);
        }

        return result;
    }

    @Override
    public Register visitPointerType(PointerType pt) {

        Register result = getRegister();
        writer.printf("li %s, %d\n", result, 4);
        return result;
    }

    @Override
    public Register visitArrayType(ArrayType at) {
        return null;
    }

    @Override
    public Register visitVarDecl(VarDecl vd) {
        // TODO: to complete

        //System.out.println("I was in VarDecl, beginning");

        // GLOBALs
        if (this.globalVars) {
            String name = vd.varName;
            if (vd.type == BaseType.INT) {                  // basic
                writer.printf(".align 2\n");
                writer.printf("%s: \t .word 0\n", name);
                vd.isGlobalVar = true;
            } else if (vd.type == BaseType.CHAR) {          // basic
                writer.printf("%s: \t .space 1\n", name);
                vd.isGlobalVar = true;
            } else if (vd.type instanceof PointerType) {    // pointers
                writer.printf(".align 2\n");
                writer.printf("%s: \t .space 4\n", name);
                vd.isGlobalVar = true;
            } else if (vd.type instanceof ArrayType) {      // arrays
                int numOfMembers = ((ArrayType) vd.type).integer;

                if (((ArrayType) vd.type).type == BaseType.INT) {                   // array [basic]
                    writer.printf(".align 2\n");
                    writer.printf("%s: \t .space %d\n", name, numOfMembers * 4);
                    vd.isGlobalVar = true;
                } else if (((ArrayType) vd.type).type == BaseType.CHAR) {           // array [basic]
                    writer.printf("%s: \t .space %d\n", name, numOfMembers * 1);
                    vd.isGlobalVar = true;
                } else if (((ArrayType) vd.type).type == BaseType.VOID) {           // array [basic]
                    writer.printf("%s: \t .space %d\n", name, numOfMembers * 1);
                    vd.isGlobalVar = true;
                } else if (((ArrayType) vd.type).type instanceof PointerType) {     // array [pointers]
                    writer.printf(".align 2\n");
                    writer.printf("%s: \t .space %d\n", name, numOfMembers * 4);
                    vd.isGlobalVar = true;
                }
            } else if (vd.type instanceof StructType) {     // structs
                //System.out.println("I was in VarDecl, StructType section");

                //vd.std.accept(this);

                vd.isGlobalVar = true;

                for (VarDecl vdone : vd.std.varDecls) {

                    if (this.globalVars) {

                        name = vd.varName + "_" + vdone.varName;
                        //System.out.println(name);

                        if (vdone.type == BaseType.INT) {                  // basic
                            writer.printf(".align 2\n");
                            writer.printf("%s: \t .word 0\n", name);
                            vdone.isGlobalVar = true;
                        } else if (vdone.type == BaseType.CHAR) {          // basic
                            writer.printf("%s: \t .space 1\n", name);
                            vdone.isGlobalVar = true;
                        } else if (vdone.type instanceof PointerType) {    // pointers
                            writer.printf(".align 2\n");
                            writer.printf("%s: \t .space 4\n", name);
                            vdone.isGlobalVar = true;
                        } else if (vdone.type instanceof ArrayType) {      // arrays
                            int numOfMembers = ((ArrayType) vdone.type).integer;

                            if (((ArrayType) vdone.type).type == BaseType.INT) {                   // array [basic]
                                writer.printf(".align 2\n");
                                writer.printf("%s: \t .space %d\n", name, numOfMembers * 4);
                                vdone.isGlobalVar = true;
                            } else if (((ArrayType) vdone.type).type == BaseType.CHAR) {           // array [basic]
                                writer.printf("%s: \t .space %d\n", name, numOfMembers * 1);
                                vdone.isGlobalVar = true;
                            } else if (((ArrayType) vdone.type).type == BaseType.VOID) {           // array [basic]
                                writer.printf("%s: \t .space %d\n", name, numOfMembers * 1);
                                vdone.isGlobalVar = true;
                            } else if (((ArrayType) vdone.type).type instanceof PointerType) {     // array [pointers]
                                writer.printf(".align 2\n");
                                writer.printf("%s: \t .space %d\n", name, numOfMembers * 4);
                                vdone.isGlobalVar = true;
                            }
                        }
                    }


                }

                //vd.type.accept(this);
                //vd.isGlobalVar = true;


            }
            // LOCALs
        } else {
            if (vd.type == BaseType.INT || vd.type == BaseType.CHAR) {      // basic
                writer.printf("addi $sp, $sp, -4\n");
                vd.isGlobalVar = false;
                vd.offset = this.localVarOffes;
                this.localVarOffes += 4;
            } else if (vd.type instanceof PointerType) {                    // pointers
                writer.printf("addi $sp, $sp, -4\n");
                vd.isGlobalVar = false;
                vd.offset = this.localVarOffes;
                this.localVarOffes += 4;
            } else if (vd.type instanceof ArrayType) {                      // arrays
                int numOfMembers = ((ArrayType) vd.type).integer;
                int necessarySize;

                if (((ArrayType) vd.type).type == BaseType.INT) {                   // array [basic]
                    necessarySize = numOfMembers * 4;

                    writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                    vd.isGlobalVar = false;
                    vd.offset = this.localVarOffes;
                    this.localVarOffes += necessarySize;
                } else if (((ArrayType) vd.type).type == BaseType.CHAR) {           // array [basic]
                    necessarySize = numOfMembers * 1;
                    int residual = necessarySize % 4;

                    if (residual == 0) {
                        writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                        vd.isGlobalVar = false;
                        vd.offset = this.localVarOffes;
                        this.localVarOffes += necessarySize;
                    } else {
                        writer.printf("addi $sp, $sp, -%d\n", necessarySize + (4 - residual));
                        vd.isGlobalVar = false;
                        vd.offset = this.localVarOffes;
                        this.localVarOffes += necessarySize + (4 - residual);
                    }

                } else if (((ArrayType) vd.type).type == BaseType.VOID) {           // array [basic]
                    necessarySize = numOfMembers * 1;
                    int residual = necessarySize % 4;

                    if (residual == 0) {
                        writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                        vd.isGlobalVar = false;
                        vd.offset = this.localVarOffes;
                        this.localVarOffes += necessarySize;
                    } else {
                        writer.printf("addi $sp, $sp, -%d\n", necessarySize + (4 - residual));
                        vd.isGlobalVar = false;
                        vd.offset = this.localVarOffes;
                        this.localVarOffes += necessarySize + (4 - residual);
                    }
                } else if (((ArrayType) vd.type).type instanceof PointerType) {     // array [pointer]
                    necessarySize = numOfMembers * 4;

                    writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                    vd.isGlobalVar = false;
                    vd.offset = this.localVarOffes;
                    this.localVarOffes += necessarySize;
                }
            } else if (vd.type instanceof StructType) {
                //int numOfMembers = ((StructType) vd.type).varDecls.size();
                //int necessarySize;

                //vd.std.accept(this);

                //vd.type.accept(this);
                //vd.isGlobalVar = false;

                vd.isGlobalVar = false;

                for (VarDecl vdone : vd.std.varDecls) {


                    if (vdone.type == BaseType.INT || vdone.type == BaseType.CHAR) {      // basic
                        writer.printf("addi $sp, $sp, -4\n");
                        vdone.isGlobalVar = false;
                        vdone.offset = this.localVarOffes;
                        this.localVarOffes += 4;
                    } else if (vdone.type instanceof PointerType) {                    // pointers
                        writer.printf("addi $sp, $sp, -4\n");
                        vdone.isGlobalVar = false;
                        vdone.offset = this.localVarOffes;
                        this.localVarOffes += 4;
                    } else if (vdone.type instanceof ArrayType) {                      // arrays
                        int numOfMembers = ((ArrayType) vdone.type).integer;
                        int necessarySize;

                        if (((ArrayType) vdone.type).type == BaseType.INT) {                   // array [basic]
                            necessarySize = numOfMembers * 4;

                            writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                            vdone.isGlobalVar = false;
                            vdone.offset = this.localVarOffes;
                            this.localVarOffes += necessarySize;
                        } else if (((ArrayType) vdone.type).type == BaseType.CHAR) {           // array [basic]
                            necessarySize = numOfMembers * 1;
                            int residual = necessarySize % 4;

                            if (residual == 0) {
                                writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                                vdone.isGlobalVar = false;
                                vdone.offset = this.localVarOffes;
                                this.localVarOffes += necessarySize;
                            } else {
                                writer.printf("addi $sp, $sp, -%d\n", necessarySize + (4 - residual));
                                vdone.isGlobalVar = false;
                                vdone.offset = this.localVarOffes;
                                this.localVarOffes += necessarySize + (4 - residual);
                            }

                        } else if (((ArrayType) vdone.type).type == BaseType.VOID) {           // array [basic]
                            necessarySize = numOfMembers * 1;
                            int residual = necessarySize % 4;

                            if (residual == 0) {
                                writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                                vdone.isGlobalVar = false;
                                vdone.offset = this.localVarOffes;
                                this.localVarOffes += necessarySize;
                            } else {
                                writer.printf("addi $sp, $sp, -%d\n", necessarySize + (4 - residual));
                                vdone.isGlobalVar = false;
                                vdone.offset = this.localVarOffes;
                                this.localVarOffes += necessarySize + (4 - residual);
                            }
                        } else if (((ArrayType) vdone.type).type instanceof PointerType) {     // array [pointer]
                            necessarySize = numOfMembers * 4;

                            writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                            vdone.isGlobalVar = false;
                            vdone.offset = this.localVarOffes;
                            this.localVarOffes += necessarySize;
                        }
                    }
                }





                    //vd.offset = this.localVarOffes;
                //this.localVarOffes +=
            }
        }

        return null;
    }

    @Override
    public Register visitFunDecl(FunDecl p) {
        // TODO: to complete

        if (p.name.equals("main")) {

            writer.printf(".globl main\t\t # Start program execution from here\n");
            writer.printf("main:\n");
            writer.println();

            writer.println("move $fp, $sp");                    // Prologue: Initialise the frame pointer
            writer.println();

            p.block.accept(this);

            writer.println();
            writer.println("li $v0, 10");
            writer.println("syscall");
        } else {

            String name = p.name;
            writer.printf("Fun_%s:\n", name);
            writer.println();

            writer.println("move $fp, $sp");                    // Prologue: Initialise the frame pointer
            writer.println();

            int numArgs = 0;
            for (VarDecl vd : p.params) {                       // Prologue: Declare all parameters as local variables
                vd.accept(this);
                numArgs++;
            }

            for (VarDecl vd : p.params) {                       // Prologue: Assign the passed arg values to the just declared local variables
                // Get the address
                Register likeLhsReg = getRegister();
                writer.printf("addi %s, $fp, -%d\n", likeLhsReg, vd.offset);
                writer.printf("la %s, (%s)\n", likeLhsReg, likeLhsReg);

                // Get the value
                Register likeRhsReg = getRegister();
                writer.printf("addi %s, $fp, %d\n", likeRhsReg, 8 + (numArgs * 4));
                writer.printf("lw %s, (%s)\n", likeRhsReg, likeRhsReg);

                // Associate
                writer.printf("sw %s, (%s)\n", likeRhsReg, likeLhsReg);

                freeRegister(likeLhsReg);
                freeRegister(likeRhsReg);
                numArgs--;
            }


            p.block.accept(this);
                                                                // Epilogue: Put return into register $v0

            writer.printf("move $sp, $fp");                     // Epilogue: Move stack pointer to frame pointer address

            writer.println();
            writer.printf("jr $ra\t\t\t # Going back to the caller function\n");
            writer.println();


            return null;
        }


        return null;
    }

    @Override
    public Register visitIntLiteral(IntLiteral il) {
        Register result = getRegister();
        writer.printf("li %s, %d\n", result, il.integer);
        return result;
    }

    @Override
    public Register visitStrLiteral(StrLiteral sl) {
        this.globalStrLiteralCounter++;
        int localStrLiteralCounter = this.globalStrLiteralCounter;

        Register result = getRegister();

        String label = String.format("label_%d", localStrLiteralCounter);
        String name = sl.str;
        name = name.replace("\n", "\\n");

        writer.println();
        writer.println(".data");
        writer.printf("%s:\t.asciiz \"%s\"\n", label, name);
        writer.println();
        writer.println(".text");
        writer.printf("la %s, %s\n", result, label);

        return result;
    }

    @Override
    public Register visitChrLiteral(ChrLiteral cl) {
        Register result = getRegister();
        if (cl.character == '\n') {
            writer.printf("li %s, \'%s\'\n", result, "\\n");
        } else {
            writer.printf("li %s, \'%c\'\n", result, cl.character);
        }
        return result;
    }

    @Override
    public Register visitVarExpr(VarExpr v) {
        // TODO: to complete

        Register result = getRegister();

        if (v.vd.isGlobalVar) {

            if (v.vd.type instanceof ArrayType) {                                       // Check if Variable is an array variable
                writer.printf("la %s, %s\t# Should never be seen\n", result, v.name);   // Does not make sense for programming!!!
            } else if (v.vd.type == BaseType.CHAR || v.vd.type == BaseType.VOID) {
                writer.printf("lb %s, %s\n", result, v.name);
            } else {                                                                    // Has to be a basic variable
                writer.printf("lw %s, %s\n", result, v.name);
            }
        } else {

            if (v.vd.type instanceof ArrayType) {                                       // Check if Variable is an array variable
                Register addressReg = getRegister();                                    // Does not make sense for programming!!!
                writer.printf("addi %s, $fp, -%d\t# Should never be seen\n", addressReg, v.vd.offset);
                writer.printf("lw %s, (%s)\t# Should never be seen\n", result, addressReg);
                freeRegister(addressReg);
            } else if (v.vd.type == BaseType.CHAR || v.vd.type == BaseType.VOID) {
                Register addressReg = getRegister();
                writer.printf("addi %s, $fp, -%d\n", addressReg, v.vd.offset);
                writer.printf("lb %s, (%s)\n", result, addressReg);
                freeRegister(addressReg);
            } else {
                Register addressReg = getRegister();
                writer.printf("addi %s, $fp, -%d\n", addressReg, v.vd.offset);
                writer.printf("lw %s, (%s)\n", result, addressReg);
                freeRegister(addressReg);
            }
        }

        return result;
    }

    @Override
    public Register visitFunCallExpr(FunCallExpr fce) {

        if (fce.str.equals("print_i")) {
            Register funArg = fce.exprList.get(0).accept(this);

            writer.println();
            writer.printf("move $a0, %s\t\t# Prepare to print an int\n", funArg);

            freeRegister(funArg);
            writer.println("li $v0, 1");
            writer.printf("syscall\t\t\t# Print an int\n\n");

            return null;
        }

        if (fce.str.equals("print_c")) {
            Register funArg = fce.exprList.get(0).accept(this);

            writer.println();
            writer.printf("move $a0, %s\t\t# Prepare to print a char\n", funArg);

            freeRegister(funArg);
            writer.println("li $v0, 11");
            writer.printf("syscall\t\t\t# Print a char\n\n");

            return null;
        }

        if (fce.str.equals("print_s")) {
            Register funArg = fce.exprList.get(0).accept(this);

            writer.println();
            writer.printf("move $a0, %s\t\t# Prepare to print a string\n", funArg);

            freeRegister(funArg);
            writer.println("li $v0, 4");
            writer.printf("syscall\t\t\t# Print a string\n\n");

            return null;
        }

        if (fce.str.equals("read_i")) {

            Register result = getRegister();

            writer.println("li $v0, 5");
            writer.printf("syscall\t\t\t# Read an int\n\n");
            writer.printf("move %s, $v0\n", result);

            return result;
        }

        if (fce.str.equals("read_c")) {

            Register result = getRegister();

            writer.println("li $v0, 12");
            writer.printf("syscall\t\t\t# Read an int\n\n");
            writer.printf("move %s, $v0\n", result);

            return result;
        }

        // TODO: finish
        if (fce.str.equals("mcmalloc")) {
            Register numBytes = fce.exprList.get(0).accept(this);

            Register result = getRegister();

            writer.printf("li $v0, 9\t\t# Prepare to allocate heap memory\n");
            writer.println();
            writer.printf("move $a0, %s\n", numBytes);
            writer.printf("syscall\t\t\t# Allocate heap memory\n");
            writer.printf("move %s, $v0\n\n", result);

            freeRegister(numBytes);
            return result;
        }


        writer.println();
        writer.printf("############## Save all Temporary Registers #############\n");
        for (Register reg: Register.tmpRegs) {                                  // Pre-call: Save ALL, 18, temporary registers onto the stack
            writer.println();
            writer.printf("sw %s, ($sp)\n", reg);
            writer.print("addi $sp, $sp, -4\n");
            writer.println();
        }
        //freeRegs.addAll(Register.tmpRegs);
        writer.printf("########## Done Save all Temporary Registers #############\n");
        writer.println();

        String name = fce.str;

        //int argNum = 0;
        for (Expr e : fce.exprList) {

            /*
            if (argNum < 4) {                                           // 1) Pass the arguments via dedicated registers
                Register result = e.accept(this);

                writer.printf("move $a%d, %s\n", argNum, result);
                argNum++;
                freeRegister(result);
            } else {

                e.accept(this);
            }
            */

            Register arg = e.accept(this);                              // Pre-call: Pass the arguments via the stack
            writer.printf("sw %s, ($sp)\n", arg);
            writer.printf("addi $sp, $sp, -%d\n", 4);
            freeRegister(arg);
        }

        writer.printf("sw $fp, ($sp)\n");                               // Pre-call: Save the current frame pointer
        writer.printf("addi $sp, $sp, -%d\n", 4);
        writer.printf("sw $ra, ($sp)\n");                               // Pre-call: Save the current return address
        writer.printf("addi $sp, $sp, -%d\n", 4);

        writer.println();
        writer.printf("jal Fun_%s\t\t # Going the called function\n", name);
        writer.println();

        writer.printf("addi $sp, $sp, %d\n", 4);
        writer.printf("lw $ra, ($sp)\n");                               // Post-return: Restore the current return address
        writer.printf("addi $sp, $sp, %d\n", 4);
        writer.printf("lw $fp, ($sp)\n");                               // Post-return: Restore the current frame pointer

        for (Expr e : fce.exprList) {
            writer.printf("addi $sp, $sp, %d\n", 4);                    // Post-return: Pop the arguments off the stack
        }

        writer.println();
        writer.printf("############ Restore all Temporary Registers ############\n");
        for (int i = (Register.tmpRegs.size() - 1); i >= 0; i--) {//(Register reg: freeRegs) {    // Post-return: Restore ALL, 18, temporary registers from the stack
            writer.println();
            writer.print("addi $sp, $sp, 4\n");
            writer.printf("lw %s, ($sp)\n", Register.tmpRegs.get(i));
            writer.println();
        }
        writer.printf("######## Done Restore all Temporary Registers ############\n");
        writer.println();

        Register result = getRegister();
        writer.printf("move %s, $v0\n", result);                        // Post-return: Read the return value from the
                                                                        // dedicated register

        return result;
    }

    @Override
    public Register visitBinOp(BinOp bo) {
        this.globalBinOpCounter++;
        int localBinOpCounter = this.globalBinOpCounter;

        Register result = getRegister();
        Register lhsReg = bo.lhs.accept(this);
        Register rhsReg = bo.rhs.accept(this);

        switch (bo.op) {
            case ADD:
                writer.printf("add %s, %s, %s\n", result, lhsReg, rhsReg);
                break;
            case SUB:
                writer.printf("sub %s, %s, %s\n", result, lhsReg, rhsReg);
                break;
            case MUL:
                writer.printf("mul %s, %s, %s\n", result, lhsReg, rhsReg);
                break;
            case DIV:
                writer.printf("div %s, %s\n", lhsReg, rhsReg);
                writer.printf("mflo %s\n", result);
                break;
            case MOD:
                writer.printf("div %s, %s\n", lhsReg, rhsReg);
                writer.printf("mfhi %s\n", result);
                break;

            case GT:
                writer.printf("sgt %s, %s, %s\n", result, lhsReg, rhsReg);
                break;
            case LT:
                writer.printf("slt %s, %s, %s\n", result, lhsReg, rhsReg);
                break;
            case GE:
                writer.printf("sge %s, %s, %s\n", result, lhsReg, rhsReg);
                break;
            case LE:
                writer.printf("sle %s, %s, %s\n", result, lhsReg, rhsReg);
                break;
            case NE:
                writer.printf("sne %s, %s, %s\n", result, lhsReg, rhsReg);
                break;
            case EQ:
                writer.printf("seq %s, %s, %s\n", result, lhsReg, rhsReg);
                break;

            case OR:
                writer.printf("bne %s, $zero SKIP%d\n", lhsReg, localBinOpCounter);
                writer.printf("or %s, %s, %s\t# OR Identity not hit\n", result, lhsReg, rhsReg);
                writer.printf("b BinOp_EXIT%d\n", localBinOpCounter);
                writer.printf("SKIP%d: \t\t\t# OR Identity hit\n", localBinOpCounter);
                writer.printf("li %s, 1\n", result);
                writer.printf("BinOp_EXIT%d: \n", localBinOpCounter);
                break;
            case AND:
                writer.printf("beq %s, $zero SKIP%d\n", lhsReg, localBinOpCounter);
                writer.printf("and %s, %s, %s\t# AND Identity not hit\n", result, lhsReg, rhsReg);
                writer.printf("b BinOp_EXIT%d\n", localBinOpCounter);
                writer.printf("SKIP%d: \t\t\t# AND Identity hit\n", localBinOpCounter);
                writer.printf("li %s, 0\n", result);
                writer.printf("BinOp_EXIT%d: \n", localBinOpCounter);
                break;
        }

        freeRegister(lhsReg);
        freeRegister(rhsReg);
        return result;
    }

    @Override
    public Register visitOp(Op o) {
        return null;
    }

    @Override
    public Register visitArrayAccessExpr(ArrayAccessExpr aae) {

        Register resultAddress = getRegister();

        Register variable = aae.array.accept(new CodeGeneratorAddress(this));
        Register position = aae.index.accept(this);

        // GLOBALs
        if (((VarExpr) aae.array).vd.isGlobalVar) {

            if (((ArrayType) ((VarExpr) aae.array).vd.type).type == BaseType.CHAR ||
                    ((ArrayType) ((VarExpr) aae.array).vd.type).type == BaseType.VOID) {   // only types taking 1 byte, as opposed ot the usual 4
                writer.printf("li %s, %d\n", resultAddress, 1);                         // dirty reusing for returnAddress
                writer.printf("mul %s, %s, %s\n", position, position, resultAddress);   // dirty reusing for returnAddress | reusing (overwriting) of position
                writer.printf("add %s, %s %s\n", resultAddress, variable, position);    // clean use of returnAddress
                writer.printf("lb %s, (%s)\n", resultAddress, resultAddress);
            } else {
                writer.printf("li %s, %d\n", resultAddress, 4);                         // dirty reusing for returnAddress
                writer.printf("mul %s, %s, %s\n", position, position, resultAddress);   // dirty reusing for returnAddress | reusing (overwriting) of position
                writer.printf("add %s, %s %s\n", resultAddress, variable, position);    // clean use of returnAddress
                writer.printf("lw %s, (%s)\n", resultAddress, resultAddress);
            }

        } else {
            // LOCALs

            if (((ArrayType) ((VarExpr) aae.array).vd.type).type == BaseType.CHAR ||
                    ((ArrayType) ((VarExpr) aae.array).vd.type).type == BaseType.VOID) {   // only types taking 1 byte, as opposed ot the usual 4
                writer.printf("li %s, %d\n", resultAddress, 1);                         // dirty reusing for returnAddress
                writer.printf("mul %s, %s, %s\n", position, position, resultAddress);   // dirty reusing for returnAddress | reusing (overwriting) of position
                writer.printf("sub %s, %s %s\n", resultAddress, variable, position);    // clean use of returnAddress
                writer.printf("lb %s, (%s)\n", resultAddress, resultAddress);
            } else {
                writer.printf("li %s, %d\n", resultAddress, 4);                         // dirty reusing for returnAddress
                writer.printf("mul %s, %s, %s\n", position, position, resultAddress);   // dirty reusing for returnAddress | reusing (overwriting) of position
                writer.printf("sub %s, %s %s\n", resultAddress, variable, position);    // clean use of returnAddress
                writer.printf("lw %s, (%s)\n", resultAddress, resultAddress);
            }

        }


        freeRegister(variable);
        freeRegister(position);
        return resultAddress;   // returns value, not address
    }

    @Override
    public Register visitValueAtExpr(ValueAtExpr vae) {
        Register address = vae.expr.accept(new CodeGeneratorAddress(this));
        Register result = getRegister();

        if (vae.expr instanceof VarExpr && ((VarExpr) vae.expr).vd.type == BaseType.CHAR) {
            writer.printf("lb %s, (%s)\n", result, address);
        } else {
            writer.printf("lw %s, (%s)# Get the address\n", result, address);
            writer.printf("lw %s, (%s)# Get the value at that address\n", result, result);
        }

        freeRegister(address);
        return result;
    }

    @Override
    public Register visitSizeOfExpr(SizeOfExpr soe) {
        Register result = soe.type.accept(this);
        return result;
    }

    @Override
    public Register visitTypecastExpr(TypecastExpr te) {
        Register result = te.expr.accept(this);
        return result;
    }

    @Override
    public Register visitExprStmt(ExprStmt es) {
        es.expr.accept(this);
        return null;
    }

    @Override
    public Register visitWhile(While w) {
        this.globalWhileCounter++;
        int localWhileCounter = this.globalWhileCounter;

        Register condition = w.expr.accept(this);

        writer.printf("############## Enter While %d Statement #############\n", localWhileCounter);
        writer.printf("beq %s, $zero, While_EXIT%d\n", condition, localWhileCounter);
        writer.printf("While_BODY%d: \n", localWhileCounter);

        w.stmt.accept(this);
        condition = w.expr.accept(this);

        writer.printf("bne %s, $zero, While_BODY%d\n", condition, localWhileCounter);
        writer.printf("While_EXIT%d: \n", localWhileCounter);
        writer.printf("############## Leave While %d Statement #############\n", localWhileCounter);

        freeRegister(condition);
        return null;
    }

    @Override
    public Register visitIf(If i) {
        this.globalIfCounter++;
        int localIfCounter = this.globalIfCounter;

        Register condition = i.expr.accept(this);

        writer.printf("############### Enter If %d Statement ###############\n", localIfCounter);
        writer.printf("beq %s, $zero, ELSE%d\n", condition, localIfCounter);
        i.stmt1.accept(this);
        writer.printf("b If_EXIT%d\n", localIfCounter);
        writer.printf("ELSE%d: \n", localIfCounter);
        if (i.stmt2 != null) {
            i.stmt2.accept(this);
        }
        writer.printf("If_EXIT%d: \n", localIfCounter);
        writer.printf("############### Leave If %d Statement ###############\n", localIfCounter);
        writer.println();

        freeRegister(condition);
        return null;
    }

    @Override
    public Register visitAssign(Assign a) {

        Register lhsReg = a.lhs.accept(new CodeGeneratorAddress(this));
        Register rhsReg = a.rhs.accept(this);


        // Check in case of ArrayAccessExpression, we have an array of chars so can do 'sb' not 'sw'
        if (a.lhs instanceof ArrayAccessExpr &&
                ((ArrayAccessExpr) a.lhs).array instanceof VarExpr &&
                ((VarExpr) ((ArrayAccessExpr) a.lhs).array).vd.type instanceof ArrayType) {

            if (((ArrayType) ((VarExpr) ((ArrayAccessExpr) a.lhs).array).vd.type).type == BaseType.CHAR ||
                    ((ArrayType) ((VarExpr) ((ArrayAccessExpr) a.lhs).array).vd.type).type == BaseType.VOID) {
                writer.printf("sb %s, (%s)\n\n", rhsReg, lhsReg);
            } else {
                writer.printf("sw %s, (%s)\n\n", rhsReg, lhsReg);
            }

        } else if (a.lhs instanceof VarExpr) {

            // Check in case loading a variable of type char, do 'sb', not 'sw'
            if (((VarExpr) a.lhs).vd.type == BaseType.CHAR || ((VarExpr) a.lhs).vd.type == BaseType.VOID) {
                writer.printf("sb %s, (%s)\n\n", rhsReg, lhsReg);
            } else {
                writer.printf("sw %s, (%s)\n\n", rhsReg, lhsReg);
            }

        } else {
            writer.printf("sw %s, (%s)\n\n", rhsReg, lhsReg);
        }

        freeRegister(lhsReg);
        freeRegister(rhsReg);
        return null;
    }

    @Override
    public Register visitReturn(Return r) {

        if (r.expr != null) {
            Register returnReg = r.expr.accept(this);
            writer.printf("move $v0, %s\n", returnReg);                 // Epilogue: Put return into register $v0
            freeRegister(returnReg);
        }

        return null;
    }

    @Override
    public Register visitBlock(Block b) {
        // TODO: to complete

        for (VarDecl vd : b.varDeclList) {
            vd.accept(this);
        }
        writer.println();

        for (Stmt s : b.stmtList) {
            s.accept(this);
        }
        return null;
    }

    @Override
    public Register visitStructType(StructType st) {

        System.out.println("I was in StrutType, beginning");

        /*
        writer.print(st.str);
        if (st.varDecls != null) {
            String delimiter = ",";
            for (VarDecl vd : st.varDecls) {
                writer.print(delimiter);
                vd.accept(this);
            }
        }
        */


//        ====================================

        String structType = st.str;
        // Check if Struct Declaration or Struct Use
        if (st.varDecls == null || st.varDecls.isEmpty()) { // Use

            System.out.println("I was in StrucType (" + structType + "), Use section");

        } else { // Declaration

            System.out.println("I was in StrucType (" + structType + "), Declaration section");

            for (VarDecl vd : st.varDecls) {

                if (this.globalVars) {

                    String name = st.str + "_" + vd.varName;
                    System.out.println(name);

                    if (vd.type == BaseType.INT) {                  // basic
                        writer.printf(".align 2\n");
                        writer.printf("%s: \t .word 0\n", name);
                        vd.isGlobalVar = true;
                    } else if (vd.type == BaseType.CHAR) {          // basic
                        writer.printf("%s: \t .space 1\n", name);
                        vd.isGlobalVar = true;
                    } else if (vd.type instanceof PointerType) {    // pointers
                        writer.printf(".align 2\n");
                        writer.printf("%s: \t .space 4\n", name);
                        vd.isGlobalVar = true;
                    } else if (vd.type instanceof ArrayType) {      // arrays
                        int numOfMembers = ((ArrayType) vd.type).integer;

                        if (((ArrayType) vd.type).type == BaseType.INT) {                   // array [basic]
                            writer.printf(".align 2\n");
                            writer.printf("%s: \t .space %d\n", name, numOfMembers * 4);
                            vd.isGlobalVar = true;
                        } else if (((ArrayType) vd.type).type == BaseType.CHAR) {           // array [basic]
                            writer.printf("%s: \t .space %d\n", name, numOfMembers * 1);
                            vd.isGlobalVar = true;
                        } else if (((ArrayType) vd.type).type == BaseType.VOID) {           // array [basic]
                            writer.printf("%s: \t .space %d\n", name, numOfMembers * 1);
                            vd.isGlobalVar = true;
                        } else if (((ArrayType) vd.type).type instanceof PointerType) {     // array [pointers]
                            writer.printf(".align 2\n");
                            writer.printf("%s: \t .space %d\n", name, numOfMembers * 4);
                            vd.isGlobalVar = true;
                        }
                    }
                } else {
                    if (vd.type == BaseType.INT || vd.type == BaseType.CHAR) {      // basic
                        writer.printf("addi $sp, $sp, -4\n");
                        vd.isGlobalVar = false;
                        vd.offset = this.localVarOffes;
                        this.localVarOffes += 4;
                    } else if (vd.type instanceof PointerType) {                    // pointers
                        writer.printf("addi $sp, $sp, -4\n");
                        vd.isGlobalVar = false;
                        vd.offset = this.localVarOffes;
                        this.localVarOffes += 4;
                    } else if (vd.type instanceof ArrayType) {                      // arrays
                        int numOfMembers = ((ArrayType) vd.type).integer;
                        int necessarySize;

                        if (((ArrayType) vd.type).type == BaseType.INT) {                   // array [basic]
                            necessarySize = numOfMembers * 4;

                            writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                            vd.isGlobalVar = false;
                            vd.offset = this.localVarOffes;
                            this.localVarOffes += necessarySize;
                        } else if (((ArrayType) vd.type).type == BaseType.CHAR) {           // array [basic]
                            necessarySize = numOfMembers * 1;
                            int residual = necessarySize % 4;

                            if (residual == 0) {
                                writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                                vd.isGlobalVar = false;
                                vd.offset = this.localVarOffes;
                                this.localVarOffes += necessarySize;
                            } else {
                                writer.printf("addi $sp, $sp, -%d\n", necessarySize + (4 - residual));
                                vd.isGlobalVar = false;
                                vd.offset = this.localVarOffes;
                                this.localVarOffes += necessarySize + (4 - residual);
                            }

                        } else if (((ArrayType) vd.type).type == BaseType.VOID) {           // array [basic]
                            necessarySize = numOfMembers * 1;
                            int residual = necessarySize % 4;

                            if (residual == 0) {
                                writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                                vd.isGlobalVar = false;
                                vd.offset = this.localVarOffes;
                                this.localVarOffes += necessarySize;
                            } else {
                                writer.printf("addi $sp, $sp, -%d\n", necessarySize + (4 - residual));
                                vd.isGlobalVar = false;
                                vd.offset = this.localVarOffes;
                                this.localVarOffes += necessarySize + (4 - residual);
                            }
                        } else if (((ArrayType) vd.type).type instanceof PointerType) {     // array [pointer]
                            necessarySize = numOfMembers * 4;

                            writer.printf("addi $sp, $sp, -%d\n", necessarySize);
                            vd.isGlobalVar = false;
                            vd.offset = this.localVarOffes;
                            this.localVarOffes += necessarySize;
                        }
                    }
                }


            }
            return null;
        }


        return null;
    }






    @Override
    public Register visitFieldAccessExpr(FieldAccessExpr fae) {

        Register result = getRegister();


        if (fae.expr instanceof VarExpr) {

            if (((VarExpr) fae.expr).vd.isGlobalVar) {
            //if (((VarExpr) fae.expr).vd.std.varDecls.get(0).isGlobalVar) {

                String structName = "";
                if (fae.expr instanceof VarExpr) {
                    structName = ((VarExpr) fae.expr).name;

                }

                //Register baseAddress = fae.expr.accept(new CodeGeneratorAddress(this));
                String field = fae.str;

                String label = structName + "_" + field;

                writer.printf("lw %s, %s\n", result, label);

            } else {
                // ---------------------------------------------
                // Locals

                String field = fae.str;

                int index = 0;
                int size = ((VarExpr) fae.expr).vd.std.varDecls.size();
                for (int i = 0; i < size; i++) {
                    if (((VarExpr) fae.expr).vd.std.varDecls.get(i).varName.equals(field)) {
                        index = i;
                    }
                }

                if (fae.expr instanceof VarExpr) {
                    int address = ((VarExpr) fae.expr).vd.std.varDecls.get(index).offset;

                    writer.printf("li %s, %d\n", result, address);
                    writer.printf("sub %s, $fp, %s\n", result, result);
                    writer.printf("la %s, (%s)\n", result, result);
                    writer.printf("lw %s, (%s)\n", result, result);
                }
            }

        }


        //freeRegister(baseAddress);
        return result;
    }

}
