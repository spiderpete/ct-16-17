package gen;

import ast.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.EmptyStackException;
import java.util.Stack;

public class CodeGeneratorAddress implements ASTVisitor<Register> {

    /*
     * Simple register allocator.
     */
    private boolean globalVars = false;
    private int localVarOffes = 0;

    // contains all the free temporary registers
    private Stack<Register> freeRegs = new Stack<Register>();

    private CodeGenerator calleeCG;

    public CodeGeneratorAddress(CodeGenerator cr) {
        this.writer = cr.writer;
        this.freeRegs = cr.freeRegs;
        this.calleeCG = cr;
    }

    private class RegisterAllocationError extends Error {}

    private Register getRegister() {
        try {
            return freeRegs.pop();
        } catch (EmptyStackException ese) {
            throw new RegisterAllocationError(); // no more free registers, bad luck!
        }
    }

    private void freeRegister(Register reg) {
        freeRegs.push(reg);
    }

    private PrintWriter writer; // use this writer to output the assembly instructions


    public void emitProgram(Program program, File outputFile) throws FileNotFoundException {
        writer = new PrintWriter(outputFile);

        visitProgram(program);
        writer.close();
    }


    @Override
    public Register visitProgram(Program p) {
        // TODO: to complete
        return null;
    }

    @Override
    public Register visitBaseType(BaseType bt) {
        return null;
    }

    @Override
    public Register visitPointerType(PointerType pt) {
        return null;
    }

    @Override
    public Register visitStructType(StructType st) {
        return null;
    }

    @Override
    public Register visitArrayType(ArrayType at) {
        return null;
    }

    @Override
    public Register visitVarDecl(VarDecl vd) {
        // TODO: to complete
        return null;
    }

    @Override
    public Register visitFunDecl(FunDecl p) {
        // TODO: to complete
        return null;
    }

    @Override
    public Register visitIntLiteral(IntLiteral il) {
        return null;
    }

    @Override
    public Register visitStrLiteral(StrLiteral sl) {
        return null;
    }

    @Override
    public Register visitChrLiteral(ChrLiteral cl) {
        return null;
    }

    @Override
    public Register visitVarExpr(VarExpr v) {
        // TODO: to complete

        Register result = getRegister();

        if (v.vd.isGlobalVar) {

            if (v.vd.type instanceof ArrayType) {                   // Check if Variable is an array variable
                writer.printf("la %s, %s\n", result, v.name);
            } else {                                                // Has to be a basic variable
                writer.printf("la %s, %s\n", result, v.name);
            }
        } else {

            if (v.vd.type instanceof ArrayType) {                   // Check if Variable is an array variable

                Register addressReg = getRegister();
                writer.printf("addi %s, $fp, -%d\n", addressReg, v.vd.offset); // beginning of the array
                writer.printf("la %s, (%s)\n", result, addressReg);
                freeRegister(addressReg);
            } else {                                                // Has to be a basic variable
                Register addressReg = getRegister();
                writer.printf("addi %s, $fp, -%d\n", addressReg, v.vd.offset);
                writer.printf("la %s, (%s)\n", result, addressReg);
                freeRegister(addressReg);
            }
        }

        return result;
    }

    @Override
    public Register visitFunCallExpr(FunCallExpr fce) {
        return null;
    }

    @Override
    public Register visitBinOp(BinOp bo) {
        return null;
    }

    @Override
    public Register visitOp(Op o) {
        return null;
    }

    @Override
    public Register visitArrayAccessExpr(ArrayAccessExpr aae) {

        Register resultAddress = getRegister();

        Register variable = aae.array.accept(this);
        Register position = aae.index.accept(this.calleeCG);

        // GLOBALs
        if (((VarExpr) aae.array).vd.isGlobalVar) {

            if (((ArrayType) ((VarExpr) aae.array).vd.type).type == BaseType.CHAR ||
                    ((ArrayType) ((VarExpr) aae.array).vd.type).type == BaseType.VOID) {   // only types taking 1 byte, as opposed ot the usual 4
                writer.printf("li %s, %d\n", resultAddress, 1);                         // dirty reusing for returnAddress
                writer.printf("mul %s, %s, %s\n", position, position, resultAddress);   // dirty reusing for returnAddress | reusing (overwriting) of position
                writer.printf("add %s, %s %s\n", resultAddress, variable, position);    // clean use of returnAddress
            } else {
                writer.printf("li %s, %d\n", resultAddress, 4);                         // dirty reusing for returnAddress
                writer.printf("mul %s, %s, %s\n", position, position, resultAddress);   // dirty reusing for returnAddress | reusing (overwriting) of position
                writer.printf("add %s, %s %s\n", resultAddress, variable, position);    // clean use of returnAddress
            }

        } else {
            // LOCALs

            if (((ArrayType) ((VarExpr) aae.array).vd.type).type == BaseType.CHAR ||
                    ((ArrayType) ((VarExpr) aae.array).vd.type).type == BaseType.VOID) {   // only types taking 1 byte, as opposed ot the usual 4
                writer.printf("li %s, %d\n", resultAddress, 1);                         // dirty reusing for returnAddress
                writer.printf("mul %s, %s, %s\n", position, position, resultAddress);   // dirty reusing for returnAddress | reusing (overwriting) of position
                writer.printf("sub %s, %s %s\n", resultAddress, variable, position);    // clean use of returnAddress
            } else {
                writer.printf("li %s, %d\n", resultAddress, 4);                         // dirty reusing for returnAddress
                writer.printf("mul %s, %s, %s\n", position, position, resultAddress);   // dirty reusing for returnAddress | reusing (overwriting) of position
                writer.printf("sub %s, %s %s\n", resultAddress, variable, position);    // clean use of returnAddress
            }

        }

        freeRegister(variable);
        freeRegister(position);
        return resultAddress;   // returns address, not value
    }

    @Override
    public Register visitFieldAccessExpr(FieldAccessExpr fae) {
        Register result = getRegister();

        if (fae.expr instanceof VarExpr) {

            if (((VarExpr) fae.expr).vd.isGlobalVar) {

                String structName = "";
                if (fae.expr instanceof VarExpr) {
                    structName = ((VarExpr) fae.expr).name;
                }

                //Register baseAddress = fae.expr.accept(new CodeGeneratorAddress(this));
                String field = fae.str;

                String label = structName + "_" + field;

                writer.printf("la %s, %s\n", result, label);

            } else {

                // ---------------------------------------------
                // Locals

                String field = fae.str;

                int index = 0;
                int size = ((VarExpr) fae.expr).vd.std.varDecls.size();
                for (int i = 0; i < size; i++) {
                    if (((VarExpr) fae.expr).vd.std.varDecls.get(i).varName.equals(field)) {
                        index = i;
                    }
                }

                if (fae.expr instanceof VarExpr) {
                    int address = ((VarExpr) fae.expr).vd.std.varDecls.get(index).offset;

                    writer.printf("li %s, %d\n", result, address);
                    writer.printf("sub %s, $fp, %s\n", result, result);
                    writer.printf("la %s, (%s)\n", result, result);
                    //writer.printf("lw %s, (%s)\n", result, result);
                }

            }
        }

        //freeRegister(baseAddress);
        return result;
    }

    @Override
    public Register visitValueAtExpr(ValueAtExpr vae) {
        Register address = vae.expr.accept(this);
        Register result = getRegister();

        if (vae.expr instanceof VarExpr && ((VarExpr) vae.expr).vd.type == BaseType.CHAR) {
            writer.printf("lb %s, (%s)\n", result, address);
        } else {
            writer.printf("lw %s, (%s)\n", result, address);
        }

        freeRegister(address);
        return result;
    }

    @Override
    public Register visitSizeOfExpr(SizeOfExpr soe) {
        return null;
    }

    @Override
    public Register visitTypecastExpr(TypecastExpr te) {
        return null;
    }

    @Override
    public Register visitExprStmt(ExprStmt es) {
        return null;
    }

    @Override
    public Register visitWhile(While w) {
        return null;
    }

    @Override
    public Register visitIf(If i) {
        return null;
    }

    @Override
    public Register visitAssign(Assign a) {
        return null;
    }

    @Override
    public Register visitReturn(Return r) {
        return null;
    }

    @Override
    public Register visitBlock(Block b) {
        // TODO: to complete
        return null;
    }

}
