package parser;

import ast.*;
import lexer.Token;
import lexer.Tokeniser;
import lexer.Token.TokenClass;

import java.util.*;


/**
 * @author Peter Stefanov
 */
public class Parser {

    private Token token;

    // use for backtracking (useful for distinguishing decls from procs when parsing a program for instance)
    private Queue<Token> buffer = new LinkedList<Token>();

    private final Tokeniser tokeniser;


    public Parser(Tokeniser tokeniser) {
        this.tokeniser = tokeniser;
    }

    public Program parse() {
        // get the first token
        nextToken();

        return parseProgram();
    }

    public int getErrorCount() {
        return error;
    }

    private int error = 0;
    private Token lastErrorToken;

    private void error(TokenClass... expected) {

        if (lastErrorToken == token) {
            // skip this error, same token causing trouble
            return;
        }

        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (TokenClass e : expected) {
            sb.append(sep);
            sb.append(e);
            sep = "|";
        }
        System.out.println("Parsing error: expected (" + sb + ") found (" + token + ") at " + token.position);

        error++;
        lastErrorToken = token;
    }

    /*
     * Look ahead the i^th element from the stream of token.
     * i should be >= 1
     */
    private Token lookAhead(int i) {
        // ensures the buffer has the element we want to look ahead
        while (buffer.size() < i)
            buffer.add(tokeniser.nextToken());
        assert buffer.size() >= i;

        int cnt = 1;
        for (Token t : buffer) {
            if (cnt == i)
                return t;
            cnt++;
        }

        assert false; // should never reach this
        return null;
    }


    /*
     * Consumes the next token from the tokeniser or the buffer if not empty.
     */
    private void nextToken() {
        if (!buffer.isEmpty())
            token = buffer.remove();
        else
            token = tokeniser.nextToken();
    }

    /*
     * If the current token is equals to the expected one, then skip it, otherwise report an error.
     * Returns the expected token or null if an error occurred.
     */
    private Token expect(TokenClass... expected) {
        for (TokenClass e : expected) {
            if (e == token.tokenClass) {
                Token cur = token;
                nextToken();
                return cur;
            }
        }

        error(expected);
        return null;
    }

    /*
    * Returns true if the current token is equals to any of the expected ones.
    */
    private boolean accept(TokenClass... expected) {
        boolean result = false;
        for (TokenClass e : expected)
            result |= (e == token.tokenClass);
        return result;
    }


    private Program parseProgram() {
        //parseIncludes();
        parseIncludeREP();
        //List<StructType> sts = parseStructDecls();
        List<StructType> sts = parseStructdeclREPHelper();
        //List<VarDecl> vds = parseVarDecls();
        List<VarDecl> vds = parseVardeclREPHelper();
        //List<FunDecl> fds = parseFunDecls();
        List<FunDecl> fds = parseFundeclREPHelper();
        expect(TokenClass.EOF);
        return new Program(sts, vds, fds);
    }

    /*
    // includes are ignored, so does not need to return an AST node
-    private void parseIncludes() {
        if (accept(TokenClass.INCLUDE)) {
            nextToken();
            expect(TokenClass.STRING_LITERAL);
            parseIncludes();
        }
    }
    */

    private void parseIncludeREP() {
        if (accept(TokenClass.INCLUDE)) {
            parseInclude();
            parseIncludeREP();
        }
    }

    /*
    private List<StructType> parseStructDecls() {
        // to be completed ...
        return null;
    }
    */

    private List<StructType> parseStructdeclREPHelper() {
        List<StructType> structList = new ArrayList<>();
        structList = parseStructdeclREP(structList);

        return structList;
    }

    //private List<StructType> structdeclREP = new ArrayList<StructType>();
    private List<StructType> parseStructdeclREP(List<StructType> stList) {
        if (accept(TokenClass.STRUCT)) {
            TokenClass peek1 = lookAhead(2).tokenClass;
            if (peek1.equals(TokenClass.LBRA)) {
                StructType st = parseStructdecl();
                stList.add(st);
                stList = parseStructdeclREP(stList);
            }
        }
        return stList;
    }

    /*
    private List<VarDecl> parseVarDecls() {
        // to be completed ...
        return null;
    }
    */

    private List<VarDecl> parseVardeclREPHelper() {
        List<VarDecl> varList = new ArrayList<>();
        varList = parseVardeclREP(varList);

        return varList;
    }


    //private List<VarDecl> vardeclREP = new ArrayList<VarDecl>();
    private List<VarDecl> parseVardeclREP(List<VarDecl> vdList) {
        if (accept(TokenClass.INT) || accept(TokenClass.CHAR) || accept(TokenClass.VOID) || accept(TokenClass.STRUCT)) {
            TokenClass peek1 = lookAhead(1).tokenClass;
            TokenClass peek2 = lookAhead(2).tokenClass;
            if (peek1.equals(TokenClass.ASTERIX) || (peek1.equals(TokenClass.IDENTIFIER) && peek2.equals(TokenClass.ASTERIX))) {
                //peek2 = lookAhead(2).tokenClass;
                TokenClass peek3 = lookAhead(3).tokenClass;
                if (peek2.equals(TokenClass.IDENTIFIER) || (peek2.equals(TokenClass.IDENTIFIER) && peek3.equals(TokenClass.IDENTIFIER))) {
                    //peek3 = lookAhead(3).tokenClass;
                    TokenClass peek4 = lookAhead(4).tokenClass;
                    if ((peek3.equals(TokenClass.SC) || peek3.equals(TokenClass.LSBR)) || (peek4.equals(TokenClass.SC) || peek4.equals(TokenClass.LSBR))) {
                        VarDecl vd = parseVardecl();
                        vdList.add(vd);
                        vdList = parseVardeclREP(vdList);
                    }
                }
            } else if (peek1.equals(TokenClass.IDENTIFIER)) {
                peek2 = lookAhead(2).tokenClass;
                TokenClass peek3 = lookAhead(3).tokenClass;
                if ((peek2.equals(TokenClass.SC) || peek2.equals(TokenClass.LSBR)) || (peek2.equals(TokenClass.IDENTIFIER) && (peek3.equals(TokenClass.SC) || peek3.equals(TokenClass.LSBR)))) {
                    VarDecl vd = parseVardecl();
                    vdList.add(vd);
                    vdList = parseVardeclREP(vdList);
                }
            }
        }
        return vdList;
    }

    /*
    private List<FunDecl> parseFunDecls() {
        // to be completed ...
        return null;
    }
    */

    private List<FunDecl> parseFundeclREPHelper() {
        List<FunDecl> funList = new ArrayList<>();
        funList = parseFundeclREP(funList);

        return funList;
    }

    //private List<FunDecl> fundeclREP = new ArrayList<FunDecl>();
    private List<FunDecl> parseFundeclREP(List<FunDecl> fdList) {
        if (accept(TokenClass.INT) || accept(TokenClass.CHAR) || accept(TokenClass.VOID) || accept(TokenClass.STRUCT)) {
            FunDecl fd = parseFundecl();
            fdList.add(fd);
            fdList = parseFundeclREP(fdList);
        }
        return fdList;
    }

    // to be completed ...

    private void parseInclude() {
        expect(TokenClass.INCLUDE);
        expect(TokenClass.STRING_LITERAL);
    }

    private StructType parseStructdecl() {
        String str = parseStructtype();
        expect(TokenClass.LBRA);
        List<VarDecl> varDecls = parseVardeclOPTHelper();
        expect(TokenClass.RBRA);
        expect(TokenClass.SC);
        return new StructType(str, varDecls);
    }

    private List<VarDecl> parseVardeclOPTHelper() {
        List<VarDecl> varList = new ArrayList<>();
        varList = parseVardeclOPT(varList);

        return varList;
    }

    //private List<VarDecl> varDecls_parseVardeclOPT = new ArrayList<VarDecl>();
    private List<VarDecl> parseVardeclOPT(List<VarDecl> vdList) {
        VarDecl varDecl = parseVardecl();
        vdList.add(varDecl);
        if (accept(TokenClass.INT) || accept(TokenClass.CHAR) || accept(TokenClass.VOID) || accept(TokenClass.STRUCT)) {
            vdList = parseVardeclOPT(vdList);
        }
        return vdList;
    }

    private VarDecl parseVardecl() {
        Type type = parseType();
        Token tokenName = expect(TokenClass.IDENTIFIER);
        String strName = "";
        if (tokenName != null) {
            strName = tokenName.data;
        }
        if (accept(TokenClass.LSBR)) {
            nextToken();
            Token tokenInteger = expect(TokenClass.INT_LITERAL);
            int integer = 0;
            if (tokenInteger != null) {
                integer = Integer.parseInt(tokenInteger.data);
            }
            expect(TokenClass.RSBR);
            expect(TokenClass.SC);
            ArrayType arrayType = new ArrayType(type, integer);
            // Array Variable
            return new VarDecl(arrayType, strName);
        } else {
            expect(TokenClass.SC);
        }
        // Normal Variable
        return new VarDecl(type, strName);
    }

    private FunDecl parseFundecl() {
        Type type = parseType();
        Token tokenName = expect(TokenClass.IDENTIFIER);
        String strName = "";
        if (tokenName != null) {
            strName = tokenName.data;
        }
        expect(TokenClass.LPAR);
        List<VarDecl> varDecls = parseParams();
        expect(TokenClass.RPAR);
        Block block = parseBlock();
        return new FunDecl(type, strName, varDecls, block);
    }

    private Type parseType() {
        boolean isPointerType;
        if (accept(TokenClass.INT)) {
            nextToken();
            isPointerType = parsePointerOPT();
            if (isPointerType) {
                // Pointer to a INT type
                return new PointerType(BaseType.INT);
            } else {
                // Normal INT type
                return BaseType.INT;
            }
        } else if (accept(TokenClass.CHAR)) {
            nextToken();
            isPointerType = parsePointerOPT();
            if (isPointerType) {
                // Pointer to a CHAR type
                return new PointerType(BaseType.CHAR);
            } else {
                // Normal CHAR type
                return BaseType.CHAR;
            }
        } else if (accept(TokenClass.STRUCT)) {
            String str = parseStructtype();
            isPointerType = parsePointerOPT();
            if (isPointerType) {
                // Pointer to a Struct class
                return new PointerType(new StructType(str));
            }
            // Normal Struct class
            return new StructType(str);
        } else if (accept(TokenClass.VOID)) {
            nextToken();
            isPointerType = parsePointerOPT();
            if (isPointerType) {
                // Pointer to a VOID type
                return new PointerType(BaseType.VOID);
            } else {
                // Normal VOID type
                return BaseType.VOID;
            }
        } else {
            error(token.tokenClass);
            return null;
        }
    }

    private boolean parsePointerOPT() {
        if (accept(TokenClass.ASTERIX)) {
            nextToken();
            return true;
        }
        return false;
    }

    private String parseStructtype() {
        expect(TokenClass.STRUCT);
        Token tokenName = expect(TokenClass.IDENTIFIER);
        if (tokenName != null) {
            return tokenName.data;
        } else {
            return "";
        }
    }

    private List<VarDecl> parseParams() {
        return parseParamsOPTHelper();
    }

    private List<VarDecl> parseParamsOPTHelper() {
        List<VarDecl> varList = new ArrayList<>();
        varList = parseParamsOPT(varList);

        return varList;
    }

    //private List<VarDecl> varDecls_parseParamsOPT = new ArrayList<VarDecl>();
    private List<VarDecl> parseParamsOPT(List<VarDecl> vdList) {
        if (accept(TokenClass.INT) || accept(TokenClass.CHAR) || accept(TokenClass.VOID) || accept(TokenClass.STRUCT)) {
            Type type = parseType();
            Token tokenName = expect(TokenClass.IDENTIFIER);
            String strName = "";
            if (tokenName != null) {
                strName = tokenName.data;
            }
            vdList.add(new VarDecl(type, strName));
            vdList = parseParamsREP(vdList);
        }
        return vdList;
    }

    private List<VarDecl> parseParamsREP(List<VarDecl> vdList) {
        if (accept(TokenClass.COMMA)) {
            nextToken();
            Type type = parseType();
            Token tokenName = expect(TokenClass.IDENTIFIER);
            String strName = "";
            if (tokenName != null) {
                strName = tokenName.data;
            }
            vdList.add(new VarDecl(type, strName));
            vdList = parseParamsREP(vdList);
        }
        return vdList;
    }

    private Stmt parseStmt() {
        if (accept(TokenClass.LBRA)) {
            return parseBlock();
        }
        if (accept(TokenClass.WHILE)) {
            nextToken();
            expect(TokenClass.LPAR);
            Expr expr = parseExp();
            expect(TokenClass.RPAR);
            Stmt stmt = parseStmt();
            return new While(expr, stmt);
        }
        if (accept(TokenClass.IF)) {
            nextToken();
            expect(TokenClass.LPAR);
            Expr expr = parseExp();
            expect(TokenClass.RPAR);
            Stmt stmt = parseStmt();
            Stmt stmtElse = parseElseStmtOPT();
            if (stmtElse != null) {
                return new If(expr, stmt, stmtElse);
            } else {
                return new If(expr, stmt);
            }
        }
        if (accept(TokenClass.RETURN)) {
            nextToken();
            Expr expr = parseExpOPT();
            expect(TokenClass.SC);
            if (expr != null) {
                return new Return(expr);
            } else {
                return new Return();
            }
        }
        if (checkForExp()) {
            Expr expr = parseExp();
            if (accept(TokenClass.ASSIGN)) {
                nextToken();
                Expr exprRHS = parseExp();
                expect(TokenClass.SC);
                return new Assign(expr, exprRHS);
            } else if (accept(TokenClass.SC)) {
                nextToken();
                return new ExprStmt(expr);
            }
        }
        error(token.tokenClass);
        return null;
    }

    private Stmt parseElseStmtOPT() {
        if (accept(TokenClass.ELSE)) {
            nextToken();
            return parseStmt();
        }
        return null;
    }

    private Expr parseExpOPT() {
        Expr expr = null;
        if (checkForExp()) {
            expr = parseExp();
        }
        return expr;
    }

    private Block parseBlock() {
        expect(TokenClass.LBRA);
        List<VarDecl> varDecls = parseVardeclREPHelper();
        List<Stmt> stmts = parseStmtREPHelper();
        expect(TokenClass.RBRA);
        return new Block(varDecls, stmts);
    }

    private List<Stmt> parseStmtREPHelper() {
        List<Stmt> stmtList = new ArrayList<>();
        stmtList = parseStmtREP(stmtList);

        return stmtList;
    }

    //private List<Stmt> stmts_parseStmtREP = new ArrayList<Stmt>();
    private List<Stmt> parseStmtREP(List<Stmt> sList) {
        if (accept(TokenClass.LBRA) || accept(TokenClass.WHILE) || accept(TokenClass.IF) || accept(TokenClass.RETURN) || checkForExp() /*accept(TokenClass.LPAR) || accept(TokenClass.MINUS) || accept(TokenClass.IDENTIFIER) || accept(TokenClass.INT_LITERAL) || accept(TokenClass.CHAR_LITERAL) || accept(TokenClass.STRING_LITERAL) || accept(TokenClass.ASTERIX) || accept(TokenClass.SIZEOF)*/) {
            Stmt s = parseStmt();
            sList.add(s);
            sList = parseStmtREP(sList);
        }
        return sList;
    }


    private Expr parseExp() {
        Expr start = parseTermsOR();
        return parseRepOR(start);
    }

    private Expr parseRepOR(Expr lhs) {
        if (accept(TokenClass.OR)) {
            nextToken();
            Expr rhs = parseTermsOR();
            BinOp orClause = new BinOp(lhs, Op.OR, rhs);
            return parseRepOR(orClause);
        }
        return lhs;
    }

    private Expr parseTermsOR() {
        Expr start = parseTermsAND();
        return parseRepAND(start);
    }

    private Expr parseRepAND(Expr lhs) {
        if (accept(TokenClass.AND)) {
            nextToken();
            Expr rhs = parseTermsAND();
            Expr andClause = new BinOp(lhs, Op.AND, rhs);
            return parseRepAND(andClause);
        }
        return lhs;
    }

    private Expr parseTermsAND() {
        Expr start = parseTermsEQ_NOTEQ();
        return parseRepEQ_NOTEQ(start);
    }

    private Expr parseRepEQ_NOTEQ(Expr lhs) {
        if (accept(TokenClass.EQ)) {
            nextToken();
            Expr rhs = parseTermsEQ_NOTEQ();
            Expr result = new BinOp(lhs, Op.EQ, rhs);
            return parseRepEQ_NOTEQ(result);
        } else if (accept(TokenClass.NE)) {
            nextToken();
            Expr rhs = parseTermsEQ_NOTEQ();
            Expr result = new BinOp(lhs, Op.NE, rhs);
            return parseRepEQ_NOTEQ(result);
        }
        return lhs;
    }

    private Expr parseTermsEQ_NOTEQ() {
        Expr start = parseTermsCOMPAR();
        return parseRepCOMPAR(start);
    }

    private Expr parseRepCOMPAR(Expr lhs) {
        if (accept(TokenClass.LT)) {
            nextToken();
            Expr rhs = parseTermsCOMPAR();
            Expr result = new BinOp(lhs, Op.LT, rhs);
            return parseRepCOMPAR(result);
        } else if (accept(TokenClass.GT)) {
            nextToken();
            Expr rhs = parseTermsCOMPAR();
            Expr result = new BinOp(lhs, Op.GT, rhs);
            return parseRepCOMPAR(result);
        } else if (accept(TokenClass.LE)) {
            nextToken();
            Expr rhs = parseTermsCOMPAR();
            Expr result = new BinOp(lhs, Op.LE, rhs);
            return parseRepCOMPAR(result);
        } else if (accept(TokenClass.GE)) {
            nextToken();
            Expr rhs = parseTermsCOMPAR();
            Expr result = new BinOp(lhs, Op.GE, rhs);
            return parseRepCOMPAR(result);
        }
        return lhs;
    }

    private Expr parseTermsCOMPAR() {
        Expr start = parseTermsPLUS_MINUS();
        return parseRepPLUS_MINUS(start);
    }

    private Expr parseRepPLUS_MINUS(Expr lhs) {
        if (accept(TokenClass.PLUS)) {
            nextToken();
            Expr rhs = parseTermsPLUS_MINUS();
            Expr result = new BinOp(lhs, Op.ADD, rhs);
            return parseRepPLUS_MINUS(result);
        } else if (accept(TokenClass.MINUS)) {
            nextToken();
            Expr rhs = parseTermsPLUS_MINUS();
            Expr result = new BinOp(lhs, Op.SUB, rhs);
            return parseRepPLUS_MINUS(result);
        }
        return lhs;
    }

    private Expr parseTermsPLUS_MINUS() {
        Expr start = parseTermsMULT_DIV_REM();
        return parseRepMULT_DIV_REM(start);
    }

    private Expr parseRepMULT_DIV_REM(Expr lhs) {
        if (accept(TokenClass.ASTERIX)) {
            nextToken();
            Expr rhs = parseTermsMULT_DIV_REM();
            Expr result = new BinOp(lhs, Op.MUL, rhs);
            return parseRepMULT_DIV_REM(result);
        } else if (accept(TokenClass.DIV)) {
            nextToken();
            Expr rhs = parseTermsMULT_DIV_REM();
            Expr result = new BinOp(lhs, Op.DIV, rhs);
            return parseRepMULT_DIV_REM(result);
        } else if (accept(TokenClass.REM)) {
            nextToken();
            Expr rhs = parseTermsMULT_DIV_REM();
            Expr result = new BinOp(lhs, Op.MOD, rhs);
            return parseRepMULT_DIV_REM(result);
        }
        return lhs;
    }

    private Expr parseTermsMULT_DIV_REM() {
        return parseTermsHighPrecedence();
    }

    private Expr parseTermsHighPrecedence() {
        int n = 0;
        int total = parseRepStar(n);
        Expr pointVar = parseTermsValueAt();
        while (total > 0) {
            pointVar = new ValueAtExpr(pointVar);
            total--;
        }
        return pointVar;
    }

    private int parseRepStar(int n) {
        if (accept(TokenClass.ASTERIX)) {
            nextToken();
            n++;
            return parseRepStar(n);
        }
        return n;
    }

    private Expr parseTermsValueAt() {
        if (accept(TokenClass.SIZEOF)) {
            nextToken();
            expect(TokenClass.LPAR);
            Type type = parseType();
            expect(TokenClass.RPAR);
            return new SizeOfExpr(type);
        } else if (accept(TokenClass.LPAR) || accept(TokenClass.IDENTIFIER) || accept(TokenClass.MINUS) || accept(TokenClass.INT_LITERAL) || accept(TokenClass.CHAR_LITERAL) || accept(TokenClass.STRING_LITERAL)) {
            List<Type> types = parseRepTypeHelper();
            Collections.reverse(types); // has to be consumed in reverse order e.g. (String) (double) (int) a -> [(int), (double), (String)] a
            Expr toBeCasted = parseTermsTypeCast();
            for (Type type : types) {
                toBeCasted = new TypecastExpr(type, toBeCasted);
            }
            return toBeCasted;
        } else {
            error(token.tokenClass);
            return null;
        }
    }

    private List<Type> parseRepTypeHelper() {
        List<Type> types = new ArrayList<>();
        types = parseRepType(types);

        return types;
    }

    private List<Type> parseRepType(List<Type> types) {
        if (accept(TokenClass.LPAR)) {
            TokenClass peek = lookAhead(1).tokenClass;
            if (peek.equals(TokenClass.INT) || peek.equals(TokenClass.CHAR) || peek.equals(TokenClass.VOID) || peek.equals(TokenClass.STRUCT)) {
                nextToken();
                Type type = parseType();
                expect(TokenClass.RPAR);
                types.add(type);
                types = parseRepType(types);
            }
        }
        return types;
    }

    private Expr parseTermsTypeCast() {
        Expr toBeAccessed = parseTermsFieldAccess();
        return parseRepFieldAccess(toBeAccessed);
    }

    private Expr parseRepFieldAccess(Expr toBeAccessed) {
        if (accept(TokenClass.DOT)) {
            nextToken();
            Token field = expect(TokenClass.IDENTIFIER);
            String fieldName = "";
            if (field != null) {
                fieldName = field.data;
            }
            FieldAccessExpr result = new FieldAccessExpr(toBeAccessed, fieldName);
            return parseRepFieldAccess(result);
        }
        return toBeAccessed;
    }

    private Expr parseTermsFieldAccess() {
        Expr array = parseTermsArrayAccess();
        Expr index = parseRepArrayAccess(array);

        return index;
    }

    private Expr parseRepArrayAccess(Expr array) {
        if (accept(TokenClass.LSBR)) {
            nextToken();
            Expr index = parseExp();
            expect(TokenClass.RSBR);
            Expr result = new ArrayAccessExpr(array, index);
            return parseRepArrayAccess(result);
        }
        return array;
    }

    private Expr parseTermsArrayAccess() {
        String name = token.data;
        if (accept(TokenClass.IDENTIFIER)) {
            // first line rule
            TokenClass peek = lookAhead(1).tokenClass;
            if (peek.equals(TokenClass.LPAR)) {
                nextToken();
                expect(TokenClass.LPAR);
                List<Expr> exprList = parseOptFuncCall();
                expect(TokenClass.RPAR);
                return new FunCallExpr(name, exprList);
            } else {
                // second line rule, termsRest, try rule for IDENTIFIERS (last rule)
                nextToken();
                return new VarExpr(name);
            }
        } else if (accept(TokenClass.LPAR) || accept(TokenClass.MINUS) || accept(TokenClass.INT_LITERAL) || accept(TokenClass.CHAR_LITERAL) || accept(TokenClass.STRING_LITERAL)) {
            return parseTermsRest();
        } else {
            error(token.tokenClass);
            return null;
        }
    }

    private List<Expr> parseOptFuncCall() {
        List<Expr> exprs = new ArrayList<>();
        if (checkForExp()) {
            Expr expr = parseExp();
            exprs.add(expr);
            exprs = parseRepFuncCall(exprs);
        }
        return exprs;
    }

    private List<Expr> parseRepFuncCall(List<Expr> exprs) {
        if (accept(TokenClass.COMMA)) {
            nextToken();
            Expr expr = parseExp();
            exprs.add(expr);
            exprs = parseRepFuncCall(exprs);
        }
        return exprs;
    }

    private Expr parseTermsRest() {
        Token currToken = token;
        if (accept(TokenClass.LPAR)) {
            nextToken();
            Expr expr = parseExp();
            expect(TokenClass.RPAR);
            return expr;
        } else if (accept(TokenClass.MINUS)) {
            nextToken();
            String currStr = token.data;
            if (accept(TokenClass.IDENTIFIER)) {
                nextToken();
                return new BinOp(new IntLiteral(0), Op.SUB, new VarExpr(currStr));
            } else if (accept(TokenClass.INT_LITERAL)) {
                nextToken();
                return new BinOp(new IntLiteral(0), Op.SUB, new IntLiteral(Integer.parseInt(currStr)));
            } else {
                error(token.tokenClass);
                return null;
            }
        } else if (accept(TokenClass.IDENTIFIER)) {
            nextToken();
            return new VarExpr(currToken.data);
        } else if (accept(TokenClass.INT_LITERAL)) {
            nextToken();
            return new IntLiteral(Integer.parseInt(currToken.data));
        } else if (accept(TokenClass.CHAR_LITERAL)) {
            nextToken();
            return new ChrLiteral(currToken.data.charAt(0));
        } else if (accept(TokenClass.STRING_LITERAL)) {
            nextToken();
            return new StrLiteral(currToken.data);
        } else {
            error(token.tokenClass);
            return null;
        }
    }


    private boolean checkForExp() {
        return checkForExp(token.tokenClass);
    }

    private boolean checkForExp(TokenClass peek) {
        return peek.equals(TokenClass.ASTERIX) ||
                peek.equals(TokenClass.SIZEOF) ||
                peek.equals(TokenClass.LPAR) ||
                peek.equals(TokenClass.IDENTIFIER) ||
                peek.equals(TokenClass.INT_LITERAL) ||
                peek.equals(TokenClass.MINUS) ||
                peek.equals(TokenClass.CHAR_LITERAL) ||
                peek.equals(TokenClass.STRING_LITERAL);
    }
}
