package sem;

import ast.*;

import java.util.ArrayList;
import java.util.List;

public class NameAnalysisVisitor extends BaseSemanticVisitor<Void> {

    private Scope scope;
    NameAnalysisVisitor() {
        this.scope = new Scope();
        addLibFuns();
    }

    private void addLibFuns() {
        // Lib function print_s
        Type funType1 = BaseType.VOID;
        String funName1 = "print_s";
        List<VarDecl> params1 = new ArrayList<>();
        Type varType1 = new PointerType(BaseType.CHAR);
        String varName1 = "s";
        VarDecl var1 = new VarDecl(varType1, varName1);
        params1.add(var1);
        FunDecl print_s = new FunDecl(funType1, funName1, params1, null);
        FunSymbol funSymbol1 = new FunSymbol(print_s);
        scope.put(funSymbol1);

        // Lib function print_i
        Type funType2 = BaseType.VOID;
        String funName2 = "print_i";
        List<VarDecl> params2 = new ArrayList<>();
        Type varType2 = BaseType.INT;
        String varName2 = "i";
        VarDecl var2 = new VarDecl(varType2, varName2);
        params2.add(var2);
        FunDecl print_i = new FunDecl(funType2, funName2, params2, null);
        FunSymbol funSymbol2 = new FunSymbol(print_i);
        scope.put(funSymbol2);

        // Lib function print_c
        Type funType3 = BaseType.VOID;
        String funName3 = "print_c";
        List<VarDecl> params3 = new ArrayList<>();
        Type varType3 = BaseType.CHAR;
        String varName3 = "c";
        VarDecl var3 = new VarDecl(varType3, varName3);
        params3.add(var3);
        FunDecl print_c = new FunDecl(funType3, funName3, params3, null);
        FunSymbol funSymbol3 = new FunSymbol(print_c);
        scope.put(funSymbol3);

        // Lib function read_c
        Type funType4 = BaseType.CHAR;
        String funName4 = "read_c";
        List<VarDecl> params4 = new ArrayList<>();
        FunDecl read_c = new FunDecl(funType4, funName4, params4, null);
        FunSymbol funSymbol4 = new FunSymbol(read_c);
        scope.put(funSymbol4);

        // Lib function read_i
        Type funType5 = BaseType.INT;
        String funName5 = "read_i";
        List<VarDecl> params5 = new ArrayList<>();
        FunDecl read_i = new FunDecl(funType5, funName5, params5, null);
        FunSymbol funSymbol5 = new FunSymbol(read_i);
        scope.put(funSymbol5);

        // Lib function mcmalloc
        Type funType6 = new PointerType(BaseType.VOID);
        String funName6 = "mcmalloc";
        List<VarDecl> params6 = new ArrayList<>();
        Type varType6 = BaseType.INT;
        String varName6 = "size";
        VarDecl var6 = new VarDecl(varType6, varName6);
        params6.add(var6);
        FunDecl mcmalloc = new FunDecl(funType6, funName6, params6, null);
        FunSymbol funSymbol6 = new FunSymbol(mcmalloc);
        scope.put(funSymbol6);
    }

    @Override
    public Void visitProgram(Program p) {
        // To be completed...
        for (StructType st : p.structTypes) {
            st.accept(this);
        }
        for (VarDecl vd : p.varDecls) {
            vd.accept(this);
        }
        for (FunDecl fd : p.funDecls) {
            fd.accept(this);
        }
        return null;
    }

    @Override
    public Void visitBaseType(BaseType bt) {
        // To be completed...
        return null;
    }

    @Override
    public Void visitPointerType(PointerType pt) {
        //pt.pointerType.accept(this);
        return null;
    }

    @Override
    public Void visitStructType(StructType st) {
        Symbol s = scope.lookup(st.str);
        // Not registered thus far
        if (s == null) {
            // But it is a legitimate declaration
            if (st.varDecls.size() > 0) {
                scope.put(new StructTypeSymbol(st));
            } else {
                // Not legal: use
                error("Struct with name " + st.str + " has already been declared.");
            }
        } else { // Registered
            // Not legal: declaration AGAIN
            if (st.varDecls.size() > 0) {
                error("Struct with this type already exists.");
            } else {
            }
        }
        Scope oldScope = scope;
        scope = new Scope(scope);
        for (VarDecl vd : st.varDecls) {
            vd.accept(this);
        }
        scope = oldScope;
        return null;
    }

    @Override
    public Void visitArrayType(ArrayType at) {
        at.type.accept(this); //
        return null;
    }

    @Override
    public Void visitVarDecl(VarDecl vd) {
        // To be completed...
        Symbol s = scope.lookupCurrent(vd.varName);
        if (s != null && !(s instanceof StructTypeSymbol)) {
            error("Variable with name " + vd.varName + " has already been declared.");
        } else {
            scope.put(new VarSymbol(vd));
        }


        if (vd.type instanceof StructType) {
            Symbol ss = scope.lookup(((StructType) vd.type).str);
            if (/*ss != null &&*/ ss instanceof StructTypeSymbol) {
                //System.out.println("I was in NameAnalysis, StructTypeSymbol section");
                vd.std = ((StructTypeSymbol) ss).std;
            }
        }


        return null;
    }

    @Override
    public Void visitFunDecl(FunDecl p) {
        // To be completed...
        Symbol s = scope.lookupCurrent(p.name);
        if (s != null && !(s instanceof StructTypeSymbol)) {
            error("Function with name " + p.name + " has already been declared.");
        } else {
            scope.put(new FunSymbol(p));
        }

        Scope oldScope = scope;
        scope = new Scope(scope);
        for (VarDecl vd : p.params) {
            vd.accept(this);
        }
        Block b = p.block;
        for (VarDecl vd : b.varDeclList) {
            vd.accept(this);
        }
        for (Stmt stmt : b.stmtList) {
            stmt.accept(this);
        }
        scope = oldScope;
        return null;
    }

    @Override
    public Void visitIntLiteral(IntLiteral il) {
        return null;
    }

    @Override
    public Void visitStrLiteral(StrLiteral sl) {
        return null;
    }

    @Override
    public Void visitChrLiteral(ChrLiteral cl) {
        return null;
    }

    @Override
    public Void visitVarExpr(VarExpr v) {
        // To be completed...
        Symbol vs = scope.lookup(v.name);
        if (vs == null) {
            error("Variable with name " + v.name + " has not been defined.");
        } else if (!vs.isVar()) {
            error(v.name + " is not defined as a variable.");
        } else {
            v.vd = ((VarSymbol) vs).vd;
        }
        return null;
    }

    @Override
    public Void visitFunCallExpr(FunCallExpr fce) {
        Symbol vs = scope.lookup(fce.str);
        if (vs == null) {
            error("Function with name " + fce.str + " has not been declared.");
        } else if (!vs.isFun()) {
            error(fce.str + " is not declared as a function.");
        } else if (vs.isFun()) {
            if (((FunSymbol) vs).numParms != fce.exprList.size()) {
                error("Number of submitted arguments does not match the expected one.");
            }
            fce.fd = ((FunSymbol) vs).fd;

            for (Expr e : fce.exprList) {
                e.accept(this);
            }

        } else {
            fce.fd = ((FunSymbol) vs).fd;
        }
        return null;
    }

    @Override
    public Void visitBinOp(BinOp bo) {
        bo.lhs.accept(this);
        bo.rhs.accept(this);
        return null;
    }

    @Override
    public Void visitOp(Op o) {
        return null;
    }

    @Override
    public Void visitArrayAccessExpr(ArrayAccessExpr aae) {
        aae.array.accept(this);
        aae.index.accept(this);
        return null;
    }

    @Override
    public Void visitFieldAccessExpr(FieldAccessExpr fae) {
        fae.expr.accept(this);
        return null;
    }

    @Override
    public Void visitValueAtExpr(ValueAtExpr vae) {
        vae.expr.accept(this);
        return null;
    }

    @Override
    public Void visitSizeOfExpr(SizeOfExpr soe) {
        return null;
    }

    @Override
    public Void visitTypecastExpr(TypecastExpr te) {
        te.expr.accept(this);
        return null;
    }

    @Override
    public Void visitExprStmt(ExprStmt es) {
        es.expr.accept(this);
        return null;
    }

    @Override
    public Void visitWhile(While w) {
        w.expr.accept(this);
        w.stmt.accept(this);
        return null;
    }

    @Override
    public Void visitIf(If i) {
        i.expr.accept(this);
        i.stmt1.accept(this);
        if (i.stmt2 != null) {
            i.stmt2.accept(this);
        }
        return null;
    }

    @Override
    public Void visitAssign(Assign a) {
        a.lhs.accept(this);
        a.rhs.accept(this);
        return null;
    }

    @Override
    public Void visitReturn(Return r) {
        if (r.expr != null) {
            r.expr.accept(this);
        }
        return null;
    }

    @Override
	public Void visitBlock(Block b) {
		// To be completed...
        Scope oldScope = scope;
        scope = new Scope(scope);
        for (VarDecl vd : b.varDeclList) {
            vd.accept(this);
        }
        for (Stmt s : b.stmtList) {
            s.accept(this);
        }
        scope = oldScope;
		return null;
	}

    // To be completed...

}
