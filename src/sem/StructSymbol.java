package sem;

import ast.StructType;

/**
 * Created by spiderpete on 17/11/2016.
 */
public class StructSymbol extends Symbol {
    StructType std;
    public StructSymbol(StructType std) {
        super(std.str);
        this.std = std;
    }


    @Override
    boolean isVar() {
        return false;
    }

    @Override
    boolean isFun() {
        return false;
    }

    @Override
    boolean isStruct() {
        return true;
    }
}
