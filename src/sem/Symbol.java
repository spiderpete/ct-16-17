package sem;

public abstract class Symbol {
	public String name;

	public Symbol(String name) {
		this.name = name;
	}

	abstract boolean isVar();
	abstract boolean isFun();
	abstract boolean isStruct();
}
