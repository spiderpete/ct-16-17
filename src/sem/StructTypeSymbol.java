package sem;

import ast.StructType;

/**
 * Created by spiderpete on 31/10/2016.
 */
public class StructTypeSymbol extends Symbol {
    StructType std;
    public StructTypeSymbol(StructType std) {
        super(std.str);
        this.std = std;
    }

    @Override
    boolean isVar() {
        return false;
    }

    @Override
    boolean isFun() {
        return false;
    }

    @Override
    boolean isStruct() {
        return true;
    }
}
