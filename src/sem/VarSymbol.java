package sem;

import ast.VarDecl;

/**
 * Created by spiderpete on 31/10/2016.
 */
public class VarSymbol extends Symbol {
    VarDecl vd;
    public VarSymbol(VarDecl vd) {
        super(vd.varName);
        this.vd = vd;
    }

    @Override
    boolean isVar() {
        return true;
    }

    @Override
    boolean isFun() {
        return false;
    }

    @Override
    boolean isStruct() {
        return false;
    }
}
