package sem;

import ast.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TypeCheckVisitor extends BaseSemanticVisitor<Type> {
    private Map<String, StructType> structsTypes;

    public TypeCheckVisitor() {
        this.structsTypes = new HashMap<>();
    }


    @Override
    public Type visitProgram(Program p) {
        // To be completed...
        for (StructType st : p.structTypes) {
            st.accept(this);
        }
        for (VarDecl vd : p.varDecls) {
            vd.accept(this);
        }
        for (FunDecl fd : p.funDecls) {
            fd.accept(this);
        }
        return null;
    }

    @Override
    public Type visitBaseType(BaseType bt) {
        // To be completed...
        return bt;
    }

    @Override
    public Type visitPointerType(PointerType pt) {
        pt.pointerType.accept(this);
        return pt;
    }

    @Override
    public Type visitStructType(StructType st) {
        String structType = st.str;
        // Check if Struct Declaration or Struct Use
        if (st.varDecls == null || st.varDecls.isEmpty()) { // Use
            boolean isDeclared = this.structsTypes.containsKey(structType);
            if (isDeclared) {
                return st;
            } else {
                error("Struct type " + structType + " not declared.");
                return null;
            }
        } else { // Declaration
            boolean isDeclared = this.structsTypes.containsKey(structType);
            if (!isDeclared) {
                for (VarDecl vd : st.varDecls) {
                    vd.accept(this);
                }
                this.structsTypes.put(structType, st); // Keep track of declared Structs
            } else {
                error("Struct type " + structType + " already declared.");
            }
            return null;
        }
    }

    @Override
    public Type visitArrayType(ArrayType at) {
        at.type.accept(this);
        return at;
    }

    @Override
    public Type visitVarDecl(VarDecl vd) {
        // To be completed...
        //vd.type.accept(this);
        if (vd.type == BaseType.VOID) {
            error("Cannot declare variables of type void.");
        }
        return vd.type.accept(this);
    }

    @Override
    public Type visitFunDecl(FunDecl p) {
        // To be completed...
        Type funType = p.type.accept(this);
        for (VarDecl vd : p.params) {
            vd.accept(this);
        }
        Type blockReturnType = p.block.accept(this);

        if (funType == BaseType.VOID && (blockReturnType == null || blockReturnType == BaseType.VOID)) {
            return null;
        } else if (funType == BaseType.VOID && (blockReturnType != null || blockReturnType != BaseType.VOID)) {
            error("Non VOID return statement for function of type VOID.");
        } else if (funType != null && funType != BaseType.VOID && blockReturnType == null) {
            error("No return statement for function of type non VOID.");
        } else if (funType != null && funType != BaseType.VOID && blockReturnType != null) {
            if ((funType == BaseType.CHAR && blockReturnType == BaseType.INT) || (funType == BaseType.INT && blockReturnType == BaseType.CHAR)) {
                error("Function type " + funType + " does not match return statement " + blockReturnType + ".");
                return null;
            }
            if (!funType.getClass().equals(blockReturnType.getClass())) {
                error("Function type " + funType + " does not match return statement " + blockReturnType + ".");
                return null;
            } else {
                return null;
            }
        }
        return null;
    }

    @Override
    public Type visitIntLiteral(IntLiteral il) {
        return BaseType.INT;
    }

    @Override
    public Type visitStrLiteral(StrLiteral sl) {
        return new ArrayType(BaseType.CHAR, sl.str.length() + 1);
    }

    @Override
    public Type visitChrLiteral(ChrLiteral cl) {
        return BaseType.CHAR;
    }

    @Override
    public Type visitVarExpr(VarExpr v) {
        // To be completed...
        if (v.vd != null) {
            v.type = v.vd.type;
            return v.vd.type;
        } else {
            return null;
        }
    }

    @Override
    public Type visitFunCallExpr(FunCallExpr fce) {
        FunDecl fd = fce.fd;
        if (fd != null) {
            // # params == # args
            if (fce.exprList.size() != fd.params.size()) {
                error("Number of passed arguments does not match the number of expected arguments for " + fce.str);
            } else {
                // V (Type param == Type arg)
                for (int i = 0; i < fce.exprList.size(); i++) {
                    Type argiT = fce.exprList.get(i).accept(this);
                    VarDecl argiD = fd.params.get(i);
                    Type argiDT = argiD.type;

                    // In case some fun call parameters unknown, avoid nulls
                    if (argiT == null) {
                        error("One or more function call parameters has not been declared.");
                        return null;
                    }

                    if (!argiT.getClass().equals(argiDT.getClass())) {
                        error("Type of passed parameter does not match the function, " + fce.str + ", argument for " + (i + 1) + "(th) argument.");
                    }
                }
            }
            Type returnDT = fd.type;
            fce.type = returnDT;
            return returnDT;
        } else {
            return null;
        }
    }

    @Override
    public Type visitBinOp(BinOp bo) {
        Type lhsType = bo.lhs.accept(this);
        bo.op.accept(this);
        Op operator = bo.op;
        Type rhsType = bo.rhs.accept(this);

        // New
        if (lhsType == null || rhsType == null) {
            error("Either LHS or RHS are null of Binary Operation.");
            return null;
        }

        if (operator == Op.ADD || operator == Op.SUB || operator == Op.MUL || operator == Op.DIV || operator == Op.MOD ||
                operator == Op.OR || operator == Op.AND || operator == Op.GT || operator == Op.LT || operator == Op.GE ||
                operator == Op.LE) {
            if (lhsType.equals(BaseType.INT) && rhsType.equals(BaseType.INT)) {
                return BaseType.INT;
            } else {
                error("Either one or both fo the arguments are not of type INT.");
            }
        } else if (operator == Op.NE || operator == Op.EQ) {
            if (lhsType.getClass().equals(rhsType.getClass())) {
                return BaseType.INT;
            } else {
                error("The arguments are not of the same Type.");
            }
        }
        return null;
    }

    @Override
    public Type visitOp(Op o) {
        return null;
    }

    @Override
    public Type visitArrayAccessExpr(ArrayAccessExpr aae) {
        Type arrayType = aae.array.accept(this);
        Type index = aae.index.accept(this);
        if ((arrayType instanceof ArrayType) && (index == BaseType.INT)) {
            return ((ArrayType) arrayType).type.accept(this);
        } else if ((arrayType instanceof PointerType) && (index == BaseType.INT)) {
            return ((PointerType) arrayType).pointerType.accept(this);
        } else {
            error("First argument not of type Array or Pointer, or second argument not of type INT.");
            return null;
        }
    }

    @Override
    public Type visitFieldAccessExpr(FieldAccessExpr fae) {
        Type type = fae.expr.accept(this);
        String fieldName = fae.str;
        if (type instanceof StructType) {
            boolean exists = this.structsTypes.containsKey(((StructType)type).str);
            if (exists) {
                List<VarDecl> varDecls = this.structsTypes.get(((StructType)type).str).varDecls;
                Type varType = null;
                boolean fieldExists = false;
                for (VarDecl vd : varDecls) {
                    if (vd.varName.equals(fieldName)) {
                        fieldExists = true;
                        varType = vd.accept(this);
                    }
                }
                if (fieldExists) {
                    return varType;
                } else {
                    error("Struct type " + type.getClass() + " does not have field " + fieldName);
                }
            } else {
                error("Non declared Struct type. Please, declare before use.");
            }
        } else {
            error("Cannot field-access on a non-Struct type.");
        }
        return null;
    }

    @Override
    public Type visitValueAtExpr(ValueAtExpr vae) {
        Type type = vae.expr.accept(this);
        if (type instanceof PointerType) {
            return ((PointerType) type).pointerType.accept(this);
        } else {
            error("Not a pointer.");
        }
        return null;
    }

    @Override
    public Type visitSizeOfExpr(SizeOfExpr soe) {
        soe.type.accept(this);
        return BaseType.INT;
    }

    @Override
    public Type visitTypecastExpr(TypecastExpr te) {
        Type exprType = te.expr.accept(this);
        Type castType = te.type.accept(this);
        if (exprType == BaseType.CHAR) {
            if (castType == BaseType.INT) {
                return BaseType.INT;
            } else {
                error("Illegal destination for type conversion from CHAR; not to an INT.");
            }
        } else if (exprType instanceof ArrayType) {
            Type baseExprType = ((ArrayType)exprType).type.accept(this);
            if (castType instanceof PointerType) {
                Type baseCastType = ((PointerType)castType).pointerType.accept(this);
                if (baseExprType.getClass().equals(baseCastType.getClass())) {
                    return new PointerType(baseCastType);
                } else {
                    error("Source " + baseExprType + " and destination " + baseCastType + " base types mismatch.");
                }
            } else {
                error("Illegal destination for type conversion from Array type; not a Pointer");
            }
        } else if (exprType instanceof PointerType) {
            if (castType instanceof PointerType) {
                Type baseCastType = ((PointerType)castType).pointerType.accept(this);
                return new PointerType(baseCastType);
            } else {
                error("Illegal destination for type conversion from Pointer; not a Pointer.");
            }
        } else {
            error("Illegal source for type conversion. Not a CHAR, Array or Pointer.");
        }
        return null;
    }

    @Override
    public Type visitExprStmt(ExprStmt es) {
        return es.expr.accept(this);
    }

    @Override
    public Type visitWhile(While w) {
        Type type = w.expr.accept(this);
        if (type != BaseType.INT) {
            error("While conditional expression, " + type + ", is not of type INT.");
        }
        Stmt s = w.stmt;
        Type returnType = s.accept(this);

        if (returnType == null) {
            //error("No return statement in While statement.");
            return null;
        } else {
            if (returnType instanceof Return) {
                return returnType;
            } else {
                if (s instanceof Block) {
                    for (Stmt ss : ((Block) s).stmtList) {
                        if (ss instanceof Return) {
                            return ss.accept(this);
                        }
                    }
                    return null;
                } else {
                    return null;
                }
            }
        }
    }

    @Override
    public Type visitIf(If i) {
        Type type = i.expr.accept(this);
        if (type != BaseType.INT) {
            error("If conditional expression, " + type + ", is not of type INT.");
        }

        Type returnType1;
        Type returnType2;

        Stmt s1 = i.stmt1;
        returnType1 = s1.accept(this);

        Stmt s2 = i.stmt2;
        if (s2 != null) {
            returnType2 = s2.accept(this);

            if (returnType1 != null && returnType2 != null) {

                if (s1 instanceof Return && s2 instanceof Return) {

                    if (!(returnType1.equals(returnType2))) {
                        error("Non matching types for return statements.");
                        return null;
                    } else {
                        return returnType1;
                    }

                } else if (!(s1 instanceof Return) && s2 instanceof Return) {
                    return returnType2;
                } else if (s1 instanceof Return && !(s2 instanceof Return)) {
                    return returnType1;
                } else {
                    if (s1 instanceof Block) {
                        for (Stmt s : ((Block) s1).stmtList) {
                            if (s instanceof Return) {
                                return s.accept(this);
                            }
                        }
                    }
                    if (s2 instanceof Block) {
                        for (Stmt s : ((Block) s2).stmtList) {
                            if (s instanceof Return) {
                                return s.accept(this);
                            }
                        }
                    }
                    return null;
                }

            } else if (returnType1 == null && returnType2 != null) {

                if (s2 instanceof Return) {
                    return returnType2;
                } else {
                    if (s2 instanceof Block) {
                        for (Stmt s : ((Block) s2).stmtList) {
                            if (s instanceof Return) {
                                return s.accept(this);
                            }
                        }
                    }
                    return null;
                }

            } else if (returnType1 != null && returnType2 == null) {

                if (s1 instanceof Return) {
                    return returnType1;
                } else {
                    if (s1 instanceof Block) {
                        for (Stmt s : ((Block) s1).stmtList) {
                            if (s instanceof Return) {
                                return s.accept(this);
                            }
                        }
                    }
                    return null;
                }

            } else  {
                return null;
            }
        } else {

            if (returnType1 != null && s1 instanceof Return) {
                return returnType1;
            } else {
                if (s1 instanceof Block) {
                    for (Stmt s : ((Block) s1).stmtList) {
                        if (s instanceof Return) {
                            return s.accept(this);
                        }
                    }
                }
                return null;
            }
        }
    }

    @Override
    public Type visitAssign(Assign a) {
        Type lhsType = a.lhs.accept(this);
        Type rhsType = a.rhs.accept(this);

        Expr totalLeftType = a.lhs;
        boolean isTotalLeftSideValid = false;
        if (totalLeftType instanceof VarExpr || totalLeftType instanceof FieldAccessExpr || totalLeftType instanceof ArrayAccessExpr || totalLeftType instanceof ValueAtExpr) {
            isTotalLeftSideValid = true;
        }

        if (lhsType == null || rhsType == null) {
            return null;
        } else {
            if (lhsType == BaseType.VOID || lhsType instanceof ArrayType) {
                error("Left hand side is either of type VOID or Array type.");
            } else {

                /*
                System.out.println(lhsType.getClass());
                System.out.println(rhsType.getClass());
                System.out.println(lhsType.accept(this));
                System.out.println(rhsType.accept(this));
                */

                if (lhsType.getClass().equals(rhsType.getClass()) /*&& lhsType.accept(this).equals(rhsType.accept(this))*/) {
                    if (isTotalLeftSideValid) { // Task6: valid left hand side
                        return null;
                    } else {
                        error("Invalid left hand side. " + totalLeftType.getClass() + " is not VarExpr, FieldAccessExpr, ArrayAccessExpr or ValueAtExpr.");
                    }
                } else {
                    error("Left hand side type, " + lhsType + ", mismatches Right hand side, " + rhsType + ".");
                }
            }
            return null;
        }
    }

    @Override
    public Type visitReturn(Return r) {
        if (r.expr != null) {
            return r.expr.accept(this);
        } else {
            return null;
        }
    }
    @Override
	public Type visitBlock(Block b) {
		// To be completed...
        Type alternative = null;
        for (VarDecl vd : b.varDeclList) {
            vd.accept(this);
        }
        for (Stmt s : b.stmtList) {
            alternative = s.accept(this);
            if (s instanceof Return) {
                return s.accept(this);
            }
        }
		return alternative;
	}

    // To be completed...

}
