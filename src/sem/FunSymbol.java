package sem;

import ast.FunDecl;

/**
 * Created by spiderpete on 31/10/2016.
 */
public class FunSymbol extends Symbol {
    FunDecl fd;
    int numParms;
    public FunSymbol(FunDecl fd) {
        super(fd.name);
        this.fd = fd;
        this.numParms = fd.params.size();
    }

    @Override
    boolean isVar() {
        return false;
    }

    @Override
    boolean isFun() {
        return true;
    }

    @Override
    boolean isStruct() {
        return false;
    }
}
