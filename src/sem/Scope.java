package sem;

import java.util.HashMap;
import java.util.Map;

public class Scope {
	private Scope outer;
	private Map<String, Symbol> symbolTable;
	
	public Scope(Scope outer) { 
		this.outer = outer;
		this.symbolTable = new HashMap<>();
	}
	
	public Scope() {
		this(null);
		this.symbolTable = new HashMap<>();
	}
	
	public Symbol lookup(String name) {
		// To be completed...
		Symbol s = lookupCurrent(name);
		if (s != null) {
			return s;
		} else if (outer != null) {
			return outer.lookup(name);
		} else {
			return null;
		}
	}
	
	public Symbol lookupCurrent(String name) {
		// To be completed...
		return symbolTable.get(name);
	}
	
	public void put(Symbol sym) {
		symbolTable.put(sym.name, sym);
	}
}
