package ast;

import java.util.List;

/**
 * Created by spiderpete on 27/10/2016.
 */
public class FunCallExpr extends Expr {
    public final String str;
    public final List<Expr> exprList;
    public FunDecl fd; // to be filled in by the name analyser

    public FunCallExpr(String str, List<Expr> exprList) {
        this.str = str;
        this.exprList = exprList;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitFunCallExpr(this);
    }
}
