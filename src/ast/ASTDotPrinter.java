package ast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by spiderpete on 30/10/2016.
 */
public class ASTDotPrinter implements ASTVisitor<String> {
    private PrintWriter writer;
    private int nodeCnt = 0;

    public ASTDotPrinter(File f) throws FileNotFoundException {
        writer = new PrintWriter (f);
        writer.println("digraph ast {");
    }

    @Override
    public String visitProgram(Program p) {
        String programNodeID = "Node" + nodeCnt++;

        writer.println(programNodeID + " [label = \"Program\"];");

        for (StructType st : p.structTypes) {
            writer.println(programNodeID + " -> " + "Node" + nodeCnt + ";");
            st.accept(this);
            nodeCnt++;
        }
        for (VarDecl vd : p.varDecls) {
            writer.println(programNodeID + " -> " + "Node" + nodeCnt + ";");
            vd.accept(this);
            nodeCnt++;
        }
        for (FunDecl fd : p.funDecls) {
            writer.println(programNodeID + " -> " + "Node" + nodeCnt + ";");
            fd.accept(this);
            nodeCnt++;
        }
        writer.println("}");
        writer.flush();
        return "Node" + programNodeID;
    }

    @Override
    public String visitBaseType(BaseType bt) {
        writer.println("Node" + nodeCnt + " [label = \"" + bt.toString() + "\"];");
        return "Node" + nodeCnt;
    }

    @Override
    public String visitPointerType(PointerType pt) {
        String pointerTypeNodeID = "Node" + nodeCnt++;
        writer.println(pointerTypeNodeID + " [label = \"PointerType\"];");
        writer.println(pointerTypeNodeID + " -> " + "Node" + nodeCnt + ";");
        pt.pointerType.accept(this);
        nodeCnt++;
        return "Node" + pointerTypeNodeID;
    }

    @Override
    public String visitStructType(StructType st) {
        String structTypeNodeID = "Node" + nodeCnt++;
        writer.println(structTypeNodeID + " [label = \"StructType\"];");
        String str = st.str;
        writer.println(structTypeNodeID + " -> " + str + ";");
        if (st.varDecls != null) {
            for (VarDecl vd : st.varDecls) {
                writer.println(structTypeNodeID + " -> " + "Node" + nodeCnt + ";");
                vd.accept(this);
                nodeCnt++;
            }
        }
        return "Node" + structTypeNodeID;
    }

    @Override
    public String visitArrayType(ArrayType at) {
        String arrayTypeNodeID = "Node" + nodeCnt++;
        writer.println(arrayTypeNodeID + " [label = \"ArrayType\"];");
        writer.println(arrayTypeNodeID + " -> " + "Node" + nodeCnt + ";");
        at.type.accept(this);
        nodeCnt++;
        String integer = at.integer + "";
        writer.println(arrayTypeNodeID + " -> " + integer + ";");
        return "Node" + arrayTypeNodeID;
    }

    @Override
    public String visitVarDecl(VarDecl vd) {
        String varDeclNodeID = "Node" + nodeCnt++;
        writer.println(varDeclNodeID + " [label = \"VarDecl\"];");
        writer.println(varDeclNodeID + " -> " + "Node" + nodeCnt + ";");
        vd.type.accept(this);
        nodeCnt++;
        String str = vd.varName;
        writer.println(varDeclNodeID + " -> " + str + ";");
        return "Node" + varDeclNodeID;
    }

    @Override
    public String visitFunDecl(FunDecl fd) {
        String funDeclNodeID = "Node" + nodeCnt++;
        writer.println(funDeclNodeID + " [label = \"FunDecl\"];");
        writer.println(funDeclNodeID + " -> " + "Node" + nodeCnt + ";");
        fd.type.accept(this);
        nodeCnt++;
        String name = fd.name;
        writer.println(funDeclNodeID + " -> " + name + ";");
        for (VarDecl vd : fd.params) {
            writer.println(funDeclNodeID + " -> " + "Node" + nodeCnt + ";");
            vd.accept(this);
            nodeCnt++;
        }
        writer.println(funDeclNodeID + " -> " + "Node" + nodeCnt + ";");
        fd.block.accept(this);
        nodeCnt++;
        return "Node" + funDeclNodeID;
    }

    @Override
    public String visitIntLiteral(IntLiteral il) {
        String intLiteralNodeID = "Node" + nodeCnt;
        String integer = il.integer + "";
        writer.println(intLiteralNodeID + " [label = \" IntLiteral(" + integer + ")\"];");
        return "Node" + intLiteralNodeID;
    }

    @Override
    public String visitStrLiteral(StrLiteral sl) {
        String strLiteralNodeID = "Node" + nodeCnt;
        String string = sl.str;
        writer.println(strLiteralNodeID + " [label = \" StrLiteral(" + string + ")\"];");
        return "Node" + strLiteralNodeID;
    }

    @Override
    public String visitChrLiteral(ChrLiteral cl) {
        String chrLiteralNodeID = "Node" + nodeCnt;
        String character = cl.character + "";
        writer.println(chrLiteralNodeID + " [label = \" ChrLiteral(" + character + ")\"];");
        return "Node" + chrLiteralNodeID;
    }

    @Override
    public String visitVarExpr(VarExpr v) {
        String varExprNodeID = "Node" + nodeCnt;
        String name = v.name;
        writer.println(varExprNodeID + " [label = \" VarExpr(" + name + ")\"];");
        return "Node" + varExprNodeID;
    }

    @Override
    public String visitFunCallExpr(FunCallExpr fce) {
        String funCallExprNodeID = "Node" + nodeCnt++;
        writer.println(funCallExprNodeID + " [label = \"FunCallExpr\"];");
        String str = fce.str;
        writer.println(funCallExprNodeID + " -> " + str + ";");
        for (Expr e : fce.exprList) {
            writer.println(funCallExprNodeID + " -> " + "Node" + nodeCnt + ";");
            e.accept(this);
            nodeCnt++;
        }
        return "Node" + funCallExprNodeID;
    }

    @Override
    public String visitBinOp(BinOp bo) {
        String binOpNodeID = "Node" + nodeCnt++;
        writer.println(binOpNodeID + " [label = \"BinOp\"];");
        writer.println(binOpNodeID + " -> " + "Node" + nodeCnt + ";");
        bo.lhs.accept(this);
        nodeCnt++;
        writer.println(binOpNodeID + " -> " + "Node" + nodeCnt + ";");
        bo.op.accept(this);
        nodeCnt++;
        writer.println(binOpNodeID + " -> " + "Node" + nodeCnt + ";");
        bo.rhs.accept(this);
        nodeCnt++;
        return "Node" + binOpNodeID;
    }

    @Override
    public String visitOp(Op o) {
        String opNodeID = "Node" + nodeCnt;
        writer.println(opNodeID + " [label = \"" + o.toString() + "\"];");
        return "Node" + opNodeID;
    }

    @Override
    public String visitArrayAccessExpr(ArrayAccessExpr aae) {
        String arrayAccessExprNodeID = "Node" + nodeCnt++;
        writer.println(arrayAccessExprNodeID + " [label = \"ArrayAccessExpr\"];");
        writer.println(arrayAccessExprNodeID + " -> " + "Node" + nodeCnt + ";");
        aae.array.accept(this);
        nodeCnt++;
        writer.println(arrayAccessExprNodeID + " -> " + "Node" + nodeCnt + ";");
        aae.index.accept(this);
        nodeCnt++;
        return "Node" + arrayAccessExprNodeID;
    }

    @Override
    public String visitFieldAccessExpr(FieldAccessExpr fae) {
        String fieldAccessExprNodeID = "Node" + nodeCnt++;
        writer.println(fieldAccessExprNodeID + " [label = \"FieldAccessExpr\"];");
        writer.println(fieldAccessExprNodeID + " -> " + "Node" + nodeCnt + ";");
        fae.expr.accept(this);
        nodeCnt++;
        String string = fae.str;
        writer.println(fieldAccessExprNodeID + " -> " + string + ";");
        return "Node" + fieldAccessExprNodeID;
    }

    @Override
    public String visitValueAtExpr(ValueAtExpr vae) {
        String valueAtExprNodeID = "Node" + nodeCnt++;
        writer.println(valueAtExprNodeID + " [label = \"ValueAtExpr\"];");
        writer.println(valueAtExprNodeID + " -> " + "Node" + nodeCnt + ";");
        vae.expr.accept(this);
        nodeCnt++;
        return "Node" + valueAtExprNodeID;
    }

    @Override
    public String visitSizeOfExpr(SizeOfExpr soe) {
        String sizeOfExprNodeID = "Node" + nodeCnt++;
        writer.println(sizeOfExprNodeID + " [label = \"SizeOfExpr\"];");
        writer.println(sizeOfExprNodeID + " -> " + "Node" + nodeCnt + ";");
        soe.type.accept(this);
        nodeCnt++;
        return "Node" + sizeOfExprNodeID;
    }

    @Override
    public String visitTypecastExpr(TypecastExpr te) {
        String typecastExprNodeID = "Node" + nodeCnt++;
        writer.println(typecastExprNodeID + " [label = \"TypecastExpr\"];");
        writer.println(typecastExprNodeID + " -> " + "Node" + nodeCnt + ";");
        te.type.accept(this);
        nodeCnt++;
        writer.println(typecastExprNodeID + " -> " + "Node" + nodeCnt + ";");
        te.expr.accept(this);
        nodeCnt++;
        return "Node" + typecastExprNodeID;
    }

    @Override
    public String visitExprStmt(ExprStmt es) {
        String exprStmtNodeID = "Node" + nodeCnt++;
        writer.println(exprStmtNodeID + " [label = \"ExprStmt\"];");
        writer.println(exprStmtNodeID + " -> " + "Node" + nodeCnt + ";");
        es.expr.accept(this);
        nodeCnt++;
        return "Node" + exprStmtNodeID;
    }

    @Override
    public String visitWhile(While w) {
        String whileNodeID = "Node" + nodeCnt++;
        writer.println(whileNodeID + " [label = \"While\"];");
        writer.println(whileNodeID + " -> " + "Node" + nodeCnt + ";");
        w.expr.accept(this);
        nodeCnt++;
        writer.println(whileNodeID + " -> " + "Node" + nodeCnt + ";");
        w.stmt.accept(this);
        nodeCnt++;
        return "Node" + whileNodeID;
    }

    @Override
    public String visitIf(If i) {
        String ifNodeID = "Node" + nodeCnt++;
        writer.println(ifNodeID + " [label = \"If\"];");
        writer.println(ifNodeID + " -> " + "Node" + nodeCnt + ";");
        i.expr.accept(this);
        nodeCnt++;
        writer.println(ifNodeID + " -> " + "Node" + nodeCnt + ";");
        i.stmt1.accept(this);
        nodeCnt++;
        if (i.stmt2 != null) {
            writer.println(ifNodeID + " -> " + "Node" + nodeCnt + ";");
            i.stmt2.accept(this);
            nodeCnt++;
        }
        return "Node" + ifNodeID;
    }

    @Override
    public String visitAssign(Assign a) {
        String assignNodeID = "Node" + nodeCnt++;
        writer.println(assignNodeID + " [label = \"Assign\"];");
        writer.println(assignNodeID + " -> " + "Node" + nodeCnt + ";");
        a.lhs.accept(this);
        nodeCnt++;
        writer.println(assignNodeID + " -> " + "Node" + nodeCnt + ";");
        a.rhs.accept(this);
        nodeCnt++;
        return "Node" + assignNodeID;
    }

    @Override
    public String visitReturn(Return r) {
        String returnNodeID = "Node" + nodeCnt++;
        writer.println(returnNodeID + " [label = \"Return\"];");
        if (r.expr != null) {
            writer.println(returnNodeID + " -> " + "Node" + nodeCnt + ";");
            r.expr.accept(this);
            nodeCnt++;
        }
        return "Node" + returnNodeID;
    }

    @Override
    public String visitBlock(Block b) {
        String blockNodeID = "Node" + nodeCnt++;
        writer.println(blockNodeID + " [label = \"Block\"];");
        for (VarDecl vd : b.varDeclList) {
            writer.println(blockNodeID + " -> " + "Node" + nodeCnt + ";");
            vd.accept(this);
            nodeCnt++;
        }
        for (Stmt s : b.stmtList) {
            writer.println(blockNodeID + " -> " + "Node" + nodeCnt + ";");
            s.accept(this);
            nodeCnt++;
        }
        return "Node" + blockNodeID;
    }
}
