package ast;

/**
 * Created by spiderpete on 26/10/2016.
 */
public class PointerType implements Type {
    public final Type pointerType;

    public PointerType(Type pointerType) {
        this.pointerType = pointerType;
    }

    public <T> T accept(ASTVisitor<T> v){
        return v.visitPointerType(this);
    }
}
