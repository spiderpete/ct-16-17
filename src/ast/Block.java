package ast;

import java.util.List;

public class Block extends Stmt {
    // to complete ...
    public final List<VarDecl> varDeclList;
    public final List<Stmt> stmtList;

    public Block(List<VarDecl> varDeclsList, List<Stmt> stmtList) {
        this.varDeclList = varDeclsList;
        this.stmtList = stmtList;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitBlock(this);
    }
}
