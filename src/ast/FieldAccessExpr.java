package ast;

/**
 * Created by spiderpete on 27/10/2016.
 */
public class FieldAccessExpr extends Expr {
    public final Expr expr;
    public final String str;

    public FieldAccessExpr(Expr expr, String str) {
        this.expr = expr;
        this.str = str;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitFieldAccessExpr(this);
    }
}
