package ast;

/**
 * Created by spiderpete on 27/10/2016.
 */
public class Return extends Stmt {
    public final Expr expr;

    public Return() {
        this.expr = null;
    }

    public Return(Expr expr) {
        this.expr = expr;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitReturn(this);
    }
}
