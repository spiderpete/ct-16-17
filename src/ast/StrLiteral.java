package ast;

/**
 * Created by spiderpete on 27/10/2016.
 */
public class StrLiteral extends Expr {
    public final String str;

    public StrLiteral(String str) {
        this.str = str;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitStrLiteral(this);
    }
}
