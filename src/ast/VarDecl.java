package ast;

public class VarDecl implements ASTNode {
    public final Type type;
    public final String varName;

    public StructType std;
    public boolean isGlobalVar;
    public int offset;

    public VarDecl(Type type, String varName) {
	    this.type = type;
	    this.varName = varName;
        this.isGlobalVar = false;
        this.offset = 1; // legal values are <= 0
    }

     public <T> T accept(ASTVisitor<T> v) {
         return v.visitVarDecl(this);
    }
}
