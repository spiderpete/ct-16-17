package ast;

import java.util.List;

/**
 * @author cdubach
 */
public class StructType implements Type {
    // to be completed
    public final String str;
    public final List<VarDecl> varDecls;
    //public StructType std; // to be filled in by the name analyser

    public StructType(String str) {
        this.str = str; this.varDecls = null;
    }
    public StructType(String str, List<VarDecl>varDecls) {
        this.str = str; this.varDecls = varDecls;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitStructType(this);
    }

}
